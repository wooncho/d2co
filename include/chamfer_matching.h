#pragma once

#include <boost/shared_ptr.hpp>
#include <vector>
#include <opencv2/opencv.hpp>

#include "cv_ext/cv_ext.h"
#include "template_matching.h"

/* TODO
 * 
 * -Compare performance with and without bilinear interpolation
 */
typedef boost::shared_ptr< std::vector<cv_ext::IntegralImage<float> > > IntegralImageVectorPtr;
typedef boost::shared_ptr< std::vector<cv::Mat > > ImageTensorPtr;

// for debugging
void computeSegmentImage( const cv::Mat &src_img, cv::Mat &seg_img );

void computeDistanceMap( const cv::Mat &src_img, cv::Mat &dist_map );
void computeDistanceMap( const cv::Mat &src_img, cv::Mat &dist_map, 
                         cv::Mat &closest_edgels_map);
void computeImageIntegrals( const cv::Mat &src_img, 
                            IntegralImageVectorPtr &int_imgs_ptr, 
                            int num_directions = 60 );

void computeDistanceMapTensor( const cv::Mat &src_img, 
                               ImageTensorPtr &dst_map_tensor_ptr, 
                               double lambda = 100.0, int num_directions = 60 );

void computeDistanceMapTensor( const cv::Mat &src_img, 
                               ImageTensorPtr &dst_map_tensor_ptr, 
                               ImageTensorPtr &edgels_map_tensor_ptr,
                               double lambda = 100.0, int num_directions = 60 );

void computeDistanceMapTensor( const cv::Mat &src_img, 
                               ImageTensorPtr &dst_map_tensor_ptr, 
                               ImageTensorPtr &x_dst_map_tensor_ptr, 
                               ImageTensorPtr &y_dst_map_tensor_ptr, 
                               ImageTensorPtr &edgels_map_tensor_ptr,
                               double lambda = 100.0, int num_directions = 60 );

class ChamferMatching : public TemplateMatching
{
public:
  
  ChamferMatching( const cv_ext::PinholeCameraModel &cam_model,
                   const cv::Mat &dist_map,
                   const IntegralImageVectorPtr &int_dist_maps_ptr = 
                   IntegralImageVectorPtr () );
  
  ChamferMatching( const ChamferMatching &other );
  
  virtual ~ChamferMatching(){};  
    
protected:
  
  virtual void updateOptimizer( int idx );
  virtual double optimize();
  virtual double avgDistance( int idx );
  
private:
  
  void init( const cv::Mat &dist_map, const IntegralImageVectorPtr &int_dist_maps_ptr );
  
  cv::Mat dist_map_;
  IntegralImageVectorPtr int_dist_maps_ptr_;
  
  /* Pimpl idiom */
  class Optimizer;
  boost::shared_ptr< Optimizer > optimizer_ptr_;
  
  int num_directions_;
  double eta_direction_;
};

class ICPChamferMatching : public TemplateMatching
{
public:
  
  ICPChamferMatching( const cv_ext::PinholeCameraModel &cam_model,
                      const cv::Mat &closest_edgels_map );
  
  ICPChamferMatching( const ICPChamferMatching &other );
  
  virtual ~ICPChamferMatching(){};  
    
protected:
  
  virtual void updateOptimizer( int idx );
  virtual double optimize();
  virtual double avgDistance( int idx );
  
private:
  
  void init( const cv::Mat &closest_edgels_map );
  
  cv::Mat closest_edgels_map_;
  std::vector<cv::Point3f> model_pts_;
  int num_icp_iterations_;
  
  /* Pimpl idiom */
  class Optimizer;
  boost::shared_ptr< Optimizer > optimizer_ptr_;
  int selected_idx_;
};

class DirectionalChamferMatching : public TemplateMatching
{
public:
  
  DirectionalChamferMatching( const cv_ext::PinholeCameraModel &cam_model,
                              const ImageTensorPtr &dist_map_tensor_ptr,
                              const IntegralImageVectorPtr &int_dist_map_tensor_ptr = 
                              IntegralImageVectorPtr ());
  
  DirectionalChamferMatching( const DirectionalChamferMatching &other );
  
  virtual ~DirectionalChamferMatching(){};  

protected:
  
  virtual void updateOptimizer( int idx );
  virtual double optimize();
  virtual double avgDistance( int idx );
  
private:
  
  void init( const ImageTensorPtr &dist_map_tensor_ptr,
             const IntegralImageVectorPtr &int_dist_map_tensor_ptr );

  
  ImageTensorPtr dist_map_tensor_ptr_;
  IntegralImageVectorPtr int_dist_map_tensor_ptr_;
  
  /* Pimpl idiom */
  class Optimizer;
  boost::shared_ptr< Optimizer > optimizer_ptr_;
  
  int num_directions_;
  double eta_direction_;
};

class BidirectionalChamferMatching  : public TemplateMatching
{
public:
  
  BidirectionalChamferMatching ( const cv_ext::PinholeCameraModel &cam_model,
                              const ImageTensorPtr &x_dist_map_tensor_ptr,
                              const ImageTensorPtr &y_dist_map_tensor_ptr,
                              const IntegralImageVectorPtr &int_dist_map_tensor_ptr = 
                              IntegralImageVectorPtr ());
  
  BidirectionalChamferMatching ( const BidirectionalChamferMatching  &other );
  
  virtual ~BidirectionalChamferMatching (){};  

protected:
  
  virtual void updateOptimizer( int idx );
  virtual double optimize();
  virtual double avgDistance( int idx );
  
private:
  
  void init( const ImageTensorPtr &x_dist_map_tensor_ptr,
             const ImageTensorPtr &y_dist_map_tensor_ptr,
             const IntegralImageVectorPtr &int_dist_map_tensor_ptr );

  
  ImageTensorPtr x_dist_map_tensor_ptr_, y_dist_map_tensor_ptr_;
  IntegralImageVectorPtr int_dist_map_tensor_ptr_;
  
  /* Pimpl idiom */
  class Optimizer;
  boost::shared_ptr< Optimizer > optimizer_ptr_;
  
  int num_directions_;
  double eta_direction_;
};

class MultiViewsDirectionalChamferMatching  : public TemplateMatching
{
public:
  
  MultiViewsDirectionalChamferMatching ( const cv_ext::PinholeCameraModel &cam_model,
                              const std::vector<ImageTensorPtr> &dist_map_tensor_ptr_vec,
                              const ImageTensorPtr &dist_map_tensor_ptr,
                              const IntegralImageVectorPtr &int_dist_map_tensor_ptr = 
                              IntegralImageVectorPtr ());
  
  MultiViewsDirectionalChamferMatching ( const MultiViewsDirectionalChamferMatching  &other );
  
  virtual ~MultiViewsDirectionalChamferMatching (){};  

protected:
  
  virtual void updateOptimizer( int idx );
  virtual double optimize();
  virtual double avgDistance( int idx );
  
private:
  
  void init( const std::vector<ImageTensorPtr> &dist_map_tensor_ptr_vec,
              const ImageTensorPtr &dist_map_tensor_ptr,
             const IntegralImageVectorPtr &int_dist_map_tensor_ptr );

  
  std::vector<ImageTensorPtr> dist_map_tensor_ptr_vec_;
  ImageTensorPtr dist_map_tensor_ptr_;
  IntegralImageVectorPtr int_dist_map_tensor_ptr_;
  
  /* Pimpl idiom */
  class Optimizer;
  boost::shared_ptr< Optimizer > optimizer_ptr_;
  
  int num_directions_;
  double eta_direction_;
};

class MultiViewsCalibrationDirectionalChamferMatching  : public TemplateMatching
{
public:
  
  MultiViewsCalibrationDirectionalChamferMatching ( const cv_ext::PinholeCameraModel &cam_model,
                              const std::vector<ImageTensorPtr> &dist_map_tensor_ptr_vec,
                              const ImageTensorPtr &dist_map_tensor_ptr,
                              const IntegralImageVectorPtr &int_dist_map_tensor_ptr = 
                              IntegralImageVectorPtr ());
  
  MultiViewsCalibrationDirectionalChamferMatching ( const MultiViewsCalibrationDirectionalChamferMatching  &other );
  
  virtual ~MultiViewsCalibrationDirectionalChamferMatching (){};  

protected:
  
  virtual void updateOptimizer( int idx );
  virtual double optimize();
  virtual double avgDistance( int idx );
  
private:
  
  void init( const std::vector<ImageTensorPtr> &dist_map_tensor_ptr_vec,
              const ImageTensorPtr &dist_map_tensor_ptr,
             const IntegralImageVectorPtr &int_dist_map_tensor_ptr );

  
  std::vector<ImageTensorPtr> dist_map_tensor_ptr_vec_;
  ImageTensorPtr dist_map_tensor_ptr_;
  IntegralImageVectorPtr int_dist_map_tensor_ptr_;
  
  /* Pimpl idiom */
  class Optimizer;
  boost::shared_ptr< Optimizer > optimizer_ptr_;
  
  int num_directions_;
  double eta_direction_;
};

class ICPDirectionalChamferMatching : public TemplateMatching
{
public:
  
  ICPDirectionalChamferMatching( const cv_ext::PinholeCameraModel &cam_model,
                                 const ImageTensorPtr &edgels_map_tensor_ptr );
  
  ICPDirectionalChamferMatching( const ICPDirectionalChamferMatching &other );
  
  virtual ~ICPDirectionalChamferMatching(){};  

protected:
  
  virtual void updateOptimizer( int idx );
  virtual double optimize();
  virtual double avgDistance( int idx );
  
private:
  
  void init( const ImageTensorPtr &edgels_map_tensor_ptr );

  
  ImageTensorPtr edgels_map_tensor_ptr_;
  std::vector<cv::Point3f> model_pts_;
  int num_icp_iterations_;
  
  /* Pimpl idiom */
  class Optimizer;
  boost::shared_ptr< Optimizer > optimizer_ptr_;
  
  int num_directions_;
  double eta_direction_;
  int selected_idx_;
};
