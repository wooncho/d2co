#pragma once

#include "raster_object_model.h"

/** @brief Shared pointer typedef */
typedef boost::shared_ptr< class RasterObjectModel2D > RasterObjectModel2DPtr;

class RasterObjectModel2D : public RasterObjectModel
{
public:
  
  RasterObjectModel2D();
  ~RasterObjectModel2D(){};
    
  bool setDxfFile( const std::string &dxf_filename );
  
  virtual bool allVisiblePoints() const{ return true; };
  virtual void computeRaster();
  virtual void updateRaster();
  
  virtual void getPoints( std::vector<cv::Point3f> &pts ) const;
  virtual void getPoints( std::vector<cv::Point3f> &pts, std::vector<cv::Point3f> &d_pts ) const;
  virtual void getSegments( std::vector<cv::Vec6f> &segments ) const;
  virtual void getSegments( std::vector<cv::Vec6f> &segments, std::vector<cv::Point3f> &d_segments ) const;
  
  virtual void getVisiblePoints( std::vector<cv::Point3f> &pts ) const;
  virtual void getVisiblePoints( std::vector<cv::Point3f> &pts, std::vector<cv::Point3f> &d_pts ) const;
  virtual void getVisibleSegments( std::vector<cv::Vec6f> &segments ) const;
  virtual void getVisibleSegments( std::vector<cv::Vec6f> &segments, std::vector<cv::Point3f> &d_segments ) const;

  virtual void getVisiblePoints( int idx, std::vector<cv::Point3f> &pts ) const;
  virtual void getVisiblePoints( int idx, std::vector<cv::Point3f> &pts, std::vector<cv::Point3f> &d_pts ) const;
  virtual void getVisibleSegments( int idx, std::vector<cv::Vec6f> &segments ) const;
  virtual void getVisibleSegments( int idx, std::vector<cv::Vec6f> &segments, std::vector<cv::Point3f> &d_segments ) const;
  
protected:

  virtual void storeModel();
  virtual void retreiveModel( int idx );
  virtual void savePrecomputedModels( cv::FileStorage &fs ) const;
  virtual void loadPrecomputedModels( cv::FileStorage &fs );
  
private:
  
  inline void addLine( cv::Point3f &p0, cv::Point3f &p1 );
  inline void addCircleArc( cv::Point3f &center, float radius, float start_ang, float end_ang );
  inline void addEllipseArc( cv::Point3f &center, cv::Point3f &major_axis_ep, 
                                    float minor_major_ratio, float start_ang, float end_ang );

  std::vector<cv::Point3f> pts_;
  std::vector<cv::Point3f> d_pts_;

  std::vector<cv::Vec6f> segs_;
  std::vector<cv::Point3f> d_segs_;
  
  /* Pimpl idiom */
  class CadModel; 
  boost::shared_ptr< CadModel > cad_ptr_;
};
