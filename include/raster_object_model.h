#pragma once    

#include <string>
#include <vector>
#include <opencv2/opencv.hpp>
#include <Eigen/Dense>
#include <boost/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>

#include "cv_ext/cv_ext.h"

/** @brief Shared pointer typedef */
typedef boost::shared_ptr< class RasterObjectModel > RasterObjectModelPtr;

class RasterObjectModel
{
public:
  
  RasterObjectModel();
  ~RasterObjectModel(){};

  enum UoM{ MICRON = 1000000,
            CENTIMILLIMETER = 100000, 
            DECIMILLIMETER = 10000, 
            MILLIMETER = 1000, 
            CENTIMETER = 100, 
            METER = 1 };
            
  
  UoM unitOfMeasure() const { return unit_meas_; };
  cv::Point3f origOffset()const { return orig_offset_; }
  double stepMeters()const { return step_; };
  double minSegmentsLen() { return min_seg_len_; }

  void savePrecomputedModelsViews( std::string base_name ) const;
  void loadPrecomputedModelsViews( std::string base_name );

  int numPrecomputedModelsViews() const {return precomputed_rq_view_.size(); };
  /**
   * @brief Provide the camera model object used to project the raster to an
   *        image plane
   */
  cv_ext::PinholeCameraModel cameraModel() const { return cam_model_; }
  
  void setUnitOfMeasure( UoM val );
  void setCentroidOrigOffset();
  void setBBCenterOrigOffset();
  void setOrigOffset( cv::Point3f offset );
  void setStepMeters( double s );
  void setMinSegmentsLen( double len );
  void setCamModel( const cv_ext::PinholeCameraModel &cam_model );
  
  virtual bool allVisiblePoints() const = 0;
  virtual void computeRaster() = 0;
  virtual void updateRaster() = 0;

  /**
   * @brief Set the transformation that transforms points from the model frame to the camera frame 
   * 
   * @param[in] r_quat Rotation quaternion
   * @param[in] t_vec Translation vector
   */
  void setModelView( const double r_quat[4], const double t_vec[3] );

  /**
   * @brief Set the transformation that transforms points from the model frame to the camera frame 
   * 
   * @param[in] r_quat Rotation quaternion
   * @param[in] t_vec Translation vector
   */
  void setModelView( const Eigen::Quaterniond &r_quat, const Eigen::Vector3d &t_vec );
  
  /**
   * @brief Set the transformation that transforms points from the model frame to the camera frame 
   * 
   * @param[in] r_vec 3 by 1 rotation vector, in exponential notation (angle-axis)
   * @param[in] t_vec Translation vector
   */
  void setModelView( const cv::Mat_<double> &r_vec, const cv::Mat_<double> &t_vec );


  void storeModelView();
  void setModelView( int idx );
  
  /**
   * @brief Provide the current transformation that transforms points from the model frame to the camera frame 
   * 
   * @param[out] r_quat Rotation quaternion
   * @param[out] t_vec Translation vector
   */
  void modelView( double r_quat[4], double t_vec[3] ) const;
  
  /**
   * @brief Provide the current transformation that transforms points from the model frame to the camera frame 
   * 
   * @param[out] r_quat Rotation quaternion
   * @param[out] t_vec Translation vector
   */
  void modelView( Eigen::Quaterniond &r_quat, Eigen::Vector3d &t_vec ) const;
  void modelView( int idx, Eigen::Quaterniond &r_quat, Eigen::Vector3d &t_vec ) const;


 /**
   * @brief Provide the current transformation that transforms points from the model frame to the camera frame 
   * 
   * @param[out] r_vec 3 by 1 rotation vector, in exponential notation (angle-axis)
   * @param[out] t_vec Translation vector
   */
  void modelView( cv::Mat_<double> &r_vec, cv::Mat_<double> &t_vec ) const;


  virtual void getPoints( std::vector<cv::Point3f> &pts ) const = 0;
  virtual void getPoints( std::vector<cv::Point3f> &pts, std::vector<cv::Point3f> &d_pts ) const = 0;
  virtual void getSegments( std::vector<cv::Vec6f> &segments ) const = 0;
  virtual void getSegments( std::vector<cv::Vec6f> &segments, std::vector<cv::Point3f> &d_segments ) const = 0;
  
  virtual void getVisiblePoints( std::vector<cv::Point3f> &pts ) const = 0;
  virtual void getVisiblePoints( std::vector<cv::Point3f> &pts, std::vector<cv::Point3f> &d_pts ) const = 0;
  virtual void getVisibleSegments( std::vector<cv::Vec6f> &segments ) const = 0;
  virtual void getVisibleSegments( std::vector<cv::Vec6f> &segments, std::vector<cv::Point3f> &d_segments ) const = 0;

  virtual void getVisiblePoints( int idx, std::vector<cv::Point3f> &pts ) const = 0;
  virtual void getVisiblePoints( int idx, std::vector<cv::Point3f> &pts, std::vector<cv::Point3f> &d_pts ) const = 0;
  virtual void getVisibleSegments( int idx, std::vector<cv::Vec6f> &segments ) const = 0;
  virtual void getVisibleSegments( int idx, std::vector<cv::Vec6f> &segments, std::vector<cv::Point3f> &d_segments ) const = 0;
  
  /**
   * @brief Project the raster points on the image plane, 
   *        given the current model view ( see setModelView() )
   * 
   * @param[out]proj_pts Output projected points
   * 
   * \note If a point is projected outside the image, or it is not possible to 
   * project the point or it is occluded, its coordinates are set to (-1, -1)
   */
  void projectRasterPoints( std::vector<cv::Point2f> &proj_pts ) const;

  void projectRasterPoints(  const double r_quat[4], const double t_vec[3],
                             std::vector<cv::Point2f> &proj_pts ) const;
  void projectRasterPoints(  const Eigen::Quaterniond &r_quat, const Eigen::Vector3d &t_vec,
                             std::vector<cv::Point2f> &proj_pts ) const;
  void projectRasterPoints(  const cv::Mat_<double> &r_vec, const cv::Mat_<double> &t_vec,
                             std::vector<cv::Point2f> &proj_pts ) const;
                             
  void projectRasterPoints( int idx, std::vector< cv::Point2f >& proj_pts ) const;
  void projectRasterPoints( int idx, const double r_quat[4], const double t_vec[3],
                            std::vector< cv::Point2f >& proj_pts ) const;
  void projectRasterPoints( int idx, const Eigen::Quaterniond &r_quat, const Eigen::Vector3d &t_vec,
                            std::vector< cv::Point2f >& proj_pts ) const;
  void projectRasterPoints( int idx, const cv::Mat_<double> &r_vec, const cv::Mat_<double> &t_vec,
                            std::vector< cv::Point2f >& proj_pts ) const;
  
  /**
   * @brief Project the raster points and theirs normal directions on the image plane, 
   *        given the current model view ( see setModelView() )
   * 
   * @param[out]proj_pts Output projected points
   * @param[out]normal_directions Output normal directions in the image plane
   * 
   * \note If a point is projected outside the image, or it is not possible to 
   * project the point or it is occluded, its coordinates are set to (-1, -1)
   * and the normal directions is set to std::numeric_limits< float >::max();
   */                      
  void projectRasterPoints( std::vector<cv::Point2f> &proj_pts,
                            std::vector<float> &normal_directions ) const;          
  void projectRasterPoints( const double r_quat[4], const double t_vec[3],
                            std::vector<cv::Point2f> &proj_pts,
                            std::vector<float> &normal_directions ) const;          
  void projectRasterPoints( const Eigen::Quaterniond &r_quat, const Eigen::Vector3d &t_vec,
                            std::vector<cv::Point2f> &proj_pts,
                            std::vector<float> &normal_directions ) const;          
  void projectRasterPoints( const cv::Mat_<double> &r_vec, const cv::Mat_<double> &t_vec,
                            std::vector<cv::Point2f> &proj_pts,
                            std::vector<float> &normal_directions ) const;          
                            
  void projectRasterPoints( int idx, std::vector<cv::Point2f> &proj_pts,
                            std::vector<float> &normal_directions ) const;          
  void projectRasterPoints( int idx, const double r_quat[4], const double t_vec[3],
                            std::vector<cv::Point2f> &proj_pts,
                            std::vector<float> &normal_directions ) const;          
  void projectRasterPoints( int idx, const Eigen::Quaterniond &r_quat, const Eigen::Vector3d &t_vec,
                            std::vector<cv::Point2f> &proj_pts,
                            std::vector<float> &normal_directions ) const;          
  void projectRasterPoints( int idx, const cv::Mat_<double> &r_vec, const cv::Mat_<double> &t_vec,
                            std::vector<cv::Point2f> &proj_pts,
                            std::vector<float> &normal_directions ) const;          
                            
  /**
   * @brief Project the raster segments on the image plane, 
   *        given the current model view ( see setModelView() )
   * 
   * @param[out]proj_segs Output projected segments
   * 
   * \note If a segment is projected outside the image, or it is not possible to 
   * project the segment or it is occluded, all its coordinates are set to -1
   */
  void projectRasterSegments( std::vector<cv::Vec4f> &proj_segs ) const;
 
  
  /**
   * @brief Project the raster segments and theirs normal directions  on the image plane, 
   *        given the current model view ( see setModelView() )
   * 
   * @param[out]proj_segs Output projected segments
   * @param[out]normal_directions Output normal directions in the image plane
   * 
   * \note If a segment is projected outside the image, or it is not possible to 
   * project the segment or it is occluded, all its coordinates are set to -1
   * and the normal directions is set to std::numeric_limits< float >::max();
   */                      
  void projectRasterSegments( std::vector<cv::Vec4f> &proj_segs,
                              std::vector<float> &normal_directions ) const;          


protected:
  
  virtual void storeModel() = 0;
  virtual void retreiveModel( int idx ) = 0;
  virtual void savePrecomputedModels( cv::FileStorage &fs ) const = 0;
  virtual void loadPrecomputedModels( cv::FileStorage &fs ) = 0;
  
//   boost::mutex mutex_;

  cv_ext::PinholeCameraModel cam_model_;
  Eigen::Quaterniond rq_view_; 
  Eigen::Vector3d t_view_;
  UoM unit_meas_;
  enum OrigOffsetType
  {
    USER_DEFINED_ORIG_OFFSET,
    CENTROID_ORIG_OFFSET,
    BOUNDING_BOX_CENTER_ORIG_OFFSET
  };
  
  OrigOffsetType centroid_orig_offset_;
  cv::Point3f orig_offset_;
  double step_;
  double epsilon_;
  double min_seg_len_;
  
  std::vector<Eigen::Quaterniond> precomputed_rq_view_;
  std::vector<Eigen::Vector3d> precomputed_t_view_;
  
};
