#pragma once

#include <boost/shared_ptr.hpp>
#include <vector>
#include <opencv2/opencv.hpp>

#include "cv_ext/cv_ext.h"
#include "raster_object_model.h"
#include "template_matching.h"

struct ScaledImage { cv::Mat img; double scale; };
typedef boost::shared_ptr< std::vector< ScaledImage > > ScaledImagesListPtr;

void computeGradientMagnitudePyr( const cv::Mat& src_img, ScaledImagesListPtr &g_mag_pyr_ptr, 
				  unsigned int pyr_levels, double smooth_std = 1.0 );

class DirectMatching : public TemplateMatching
{
public:
  
  DirectMatching( const cv_ext::PinholeCameraModel &cam_model,
                   ScaledImagesListPtr &imgs_list_ptr );
  
  DirectMatching( const DirectMatching &other );
  
  virtual ~DirectMatching(){};  
    
protected:
  
  virtual void updateOptimizer( int idx );
  virtual double optimize();
  virtual double avgDistance( int idx );
  
private:
  
  void init( const ScaledImagesListPtr &img_pyr_ptr );
  
  ScaledImagesListPtr imgs_list_ptr_;
  std::vector<cv_ext::PinholeCameraModel> scaled_cam_models_;
  
  /* Pimpl idiom */
  class Optimizer;
  std::vector< boost::shared_ptr< Optimizer > > optimizer_pyr_ptr_;
};
