#pragma once

extern "C"
{
#include "lsd.h"
}

#include "cv_ext/pinhole_camera_model.h"
#include "raster_object_model.h"
#include "raster_object_model3D.h"

void extractEdges( const cv::Mat &src_img, std::vector< cv::Vec4f > &segments );
void extractEdgesPyr( const cv::Mat &src_img, std::vector< cv::Vec4f > &segments );
void computeEdgesNormalsDirections( const std::vector< cv::Vec4f >& segments, 
                                    std::vector< float >& normals, 
                                    std::vector< cv::Point2f >& centers );

class TemplateMatching
{
public:
  
  TemplateMatching( const cv_ext::PinholeCameraModel &cam_model );
  
  virtual ~TemplateMatching(){};

  void setTemplateModel( const RasterObjectModelPtr &model_ptr );
  void setTemplateModelVec( const std::vector<RasterObjectModel3DPtr> &model_ptr_vec );
    
  void enableVerbouseMode( bool enabled ) { verbouse_mode_ = enabled; };
  void setMaxNumIterations (int n ) { max_num_iterations_ = n; };
  
  double performOptimization( double r_quat[4], double t_vec[3] );
  double performOptimization( Eigen::Quaterniond &r_quat, Eigen::Vector3d &t_vec );
  double performOptimization( cv::Mat_<double> &r_vec, cv::Mat_<double> &t_vec );
  double performOptimization( int idx, double r_quat[4], double t_vec[3] );
  double performOptimization( int idx, Eigen::Quaterniond& r_quat, Eigen::Vector3d& t_vec );
  double performOptimization( int idx, cv::Mat_<double> &r_vec, cv::Mat_<double> &t_vec );
  
  double performOptimization ( Eigen::Quaterniond& r_quat, 
                               Eigen::Vector3d& t_vec, 
                               const std::vector<Eigen::Affine3d> &views );
                               
  double performOptimization ( Eigen::Quaterniond& r_quat, 
                               Eigen::Vector3d& t_vec, 
                               const std::vector<Eigen::Affine3d> &camera_poses,
                               const Eigen::Affine3d &model_pose );
  
  double getAvgDistance( const double r_quat[4], const double t_vec[3] );
  double getAvgDistance( const Eigen::Quaterniond &r_quat, const Eigen::Vector3d &t_vec );
  double getAvgDistance( const cv::Mat_<double> &r_vec, const cv::Mat_<double> &t_vec );
  double getAvgDistance( int idx );
    
protected:

  virtual void updateOptimizer( int idx ) = 0;
  virtual double optimize() = 0;
  virtual double avgDistance( int idx ) = 0;
  
  void setPos( const double r_quat[4], const double t_vec[3] );
  void setPos( const Eigen::Quaterniond &r_quat, const Eigen::Vector3d &t_vec );
  void setPos( const cv::Mat_<double> &r_vec, const cv::Mat_<double> &t_vec );

  void getPos( double r_quat[4], double t_vec[3] ) const;
  void getPos( Eigen::Quaterniond &r_quat, Eigen::Vector3d &t_vec ) const;
  void getPos( cv::Mat_<double> &r_vec, cv::Mat_<double> &t_vec ) const;

  
  cv_ext::PinholeCameraModel cam_model_;
  Eigen::Matrix< double, 8, 1> transf_;
  RasterObjectModelPtr model_ptr_;
  std::vector<RasterObjectModel3DPtr> model_ptr_vec_;

  std::vector<Eigen::Affine3d> views_;
  std::vector<Eigen::Affine3d> camera_poses_;
  Eigen::Affine3d model_pose_;

  int max_num_iterations_;
  double verbouse_mode_;
  
private:

  TemplateMatching& operator=( const TemplateMatching &other );
  
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};
