#pragma once

#include "raster_object_model.h"

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

/** @brief Shared pointer typedef */
typedef boost::shared_ptr< class RasterObjectModel3D > RasterObjectModel3DPtr;

class RasterObjectModel3D : public RasterObjectModel
{
public:
  
  RasterObjectModel3D();
  ~RasterObjectModel3D();
    
  bool setStlFile( const std::string& stl_filename );
  
  virtual bool allVisiblePoints() const{ return false; };
  virtual void computeRaster();
  virtual void updateRaster();
 
  
  virtual void getPoints( std::vector<cv::Point3f> &pts ) const;
  virtual void getPoints( std::vector<cv::Point3f> &pts, std::vector<cv::Point3f> &d_pts ) const;
  virtual void getSegments( std::vector<cv::Vec6f> &segments ) const;
  virtual void getSegments( std::vector<cv::Vec6f> &segments, std::vector<cv::Point3f> &d_segments ) const;
  
  virtual void getVisiblePoints( std::vector<cv::Point3f> &pts ) const;
  virtual void getVisiblePoints( std::vector<cv::Point3f> &pts, std::vector<cv::Point3f> &d_pts ) const;
  virtual void getVisibleSegments( std::vector<cv::Vec6f> &segments ) const;
  virtual void getVisibleSegments( std::vector<cv::Vec6f> &segments, std::vector<cv::Point3f> &d_segments ) const;

  virtual void getVisiblePoints( int idx, std::vector<cv::Point3f> &pts ) const;
  virtual void getVisiblePoints( int idx, std::vector<cv::Point3f> &pts, std::vector<cv::Point3f> &d_pts ) const;
  virtual void getVisibleSegments( int idx, std::vector<cv::Vec6f> &segments ) const;
  virtual void getVisibleSegments( int idx, std::vector<cv::Vec6f> &segments, std::vector<cv::Point3f> &d_segments ) const;
  
  void getDepthBufferData(int idx,std::vector<float>& dept_buffer_data) const;
  void getDepthBufferData(int idx, int idx2, float& dept_buffer_data_value) const;
  
  void setRenderWindowSize ( cv::Size s ){ render_win_size_ = s; };
  cv::Size renderWindowSize() const {return render_win_size_; };
  
  void setRenderZNear( float dist ){ render_z_near_ = dist; }
  void setRenderZFar( float dist ){ render_z_far_ = dist; }
  void setRenderFOV( float fov ){ render_fov_ = fov; };
  
  float renderZNear() const { return render_z_near_; };
  float renderZFar() const { return render_z_far_; };
  float renderFOV() const { return render_fov_; };
  
  void storePrecomputedModelsFromOther(RasterObjectModel3DPtr& other);
  
  void clearModel()
  {
    precomputed_vis_pts_.clear();
    precomputed_vis_d_pts_.clear();
    precomputed_vis_segs_.clear();
    precomputed_vis_d_segs_.clear();
    precomputed_depth_buffer_data_.clear();
    precomputed_rt_persp_.clear();
    precomputed_rq_view_.clear();
    precomputed_t_view_.clear();
  }
  
  void enableDepthBufferStoring(bool enable){ depth_buffer_storing_enabled_=enable; };
  
  // project 3D point into the current gl perspective (returns the current depth_buffer_data_ index)
  int projectPointToGLPersp(const cv::Point3f& p, cv::Point& proj_p, float& depth);
  // project 3D point into the i-th precomputed gl perspective (returns the i-th precomputed depth_buffer_data_ index)
  int projectPointToGLPersp(const int idx, const cv::Point3f& p, cv::Point& proj_p, float& depth);

protected:

  virtual void clearModel2()
  {
    precomputed_vis_pts_.clear();
    precomputed_vis_d_pts_.clear();
    precomputed_vis_segs_.clear();
    precomputed_vis_d_segs_.clear();
    precomputed_depth_buffer_data_.clear();
    precomputed_rt_persp_.clear();
    precomputed_rq_view_.clear();
    precomputed_t_view_.clear();
  }
  virtual void storeModel();
  virtual void retreiveModel( int idx );
  virtual void savePrecomputedModels( cv::FileStorage &fs ) const;
  virtual void loadPrecomputedModels( cv::FileStorage &fs );

  
private:

  inline void addLine( cv::Point3f &p0, cv::Point3f &p1 );
  inline void addVisibleLine( cv::Point3f &p0, cv::Point3f &p1 );
  bool checkPointOcclusion( cv::Point3f& p );
    
  std::vector<cv::Point3f> pts_, vis_pts_;
  std::vector<cv::Point3f> d_pts_, vis_d_pts_;

  std::vector<cv::Vec6f> segs_, vis_segs_;
  std::vector<cv::Point3f> d_segs_, vis_d_segs_;
  
  glm::mat4 rt_persp_;

  std::vector< std::vector<cv::Point3f> > precomputed_vis_pts_;
  std::vector< std::vector<cv::Point3f> > precomputed_vis_d_pts_;
  
  std::vector< std::vector<cv::Vec6f> > precomputed_vis_segs_;
  std::vector< std::vector<cv::Point3f> > precomputed_vis_d_segs_;
  
  std::vector<glm::mat4> precomputed_rt_persp_;
  std::vector< std::vector<float> > precomputed_depth_buffer_data_;
  
  bool depth_buffer_storing_enabled_;
    
  /* Pimpl idiom */
  class MeshModel; 
  boost::shared_ptr< MeshModel > mesh_model_ptr_;
  
  cv::Size render_win_size_;
  float render_z_near_, render_z_far_, render_fov_;
  float depth_buffer_epsilon_, normal_epsilon_;

  std::vector<float> depth_buffer_data_;
  float depth_transf_a_, depth_transf_b_, depth_transf_c_;
};
