#include "cv_ext/types.h"
#include "cv_ext/debug_tools.h"

#include <stdexcept>

#include <pcl/visualization/cloud_viewer.h>

void cv_ext::showImage( const cv::Mat &img, const std::string &win_name, 
                        bool normalize, int sleep )
{
  if( img.channels() == 1 && normalize )
  {
    cv::Mat debug_img;
    cv::normalize(img, debug_img, 0, 255, cv::NORM_MINMAX, cv::DataType<uchar>::type);
    cv::imshow(win_name,debug_img);
  }
  else if( img.channels() == 3 && normalize )
  {
    cv::Mat debug_img;
    cv::normalize(img, debug_img, 0, 255, cv::NORM_MINMAX, cv::DataType<cv::Vec3b>::type);
    cv::imshow(win_name,debug_img);
  }
  else
    cv::imshow(win_name,img);

  if( sleep )
    cv::waitKey(sleep);
  else
    while ( cv::waitKey() != 27);
}


template <typename _TPoint3D> 
  void cv_ext::show3DPoints( const std::vector<_TPoint3D> &points, 
                             const std::string &win_name, bool show_axes, 
                             const std::vector< Eigen::Affine3f > &axes_transf )
{
  int points_size = points.size();
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud ( new pcl::PointCloud<pcl::PointXYZ> );

  // Fill in the cloud data
  cloud->width    = points_size;
  cloud->height   = 1;
  cloud->is_dense = true;
  cloud->points.reserve ( points_size );

  for ( size_t i = 0; i < points_size; ++i )
    cloud->points.push_back( pcl::PointXYZ( points[i].x, points[i].y, points[i].z) );

  boost::shared_ptr<pcl::visualization::PCLVisualizer> 
    viewer (new pcl::visualization::PCLVisualizer (win_name));
    
  viewer->setBackgroundColor (0,0,0);
  viewer->addPointCloud<pcl::PointXYZ> (cloud );
  if( show_axes )
  {
    if(axes_transf.empty())
      viewer->addCoordinateSystem (1.0);
    else
    {
      for(int i = 0; i < axes_transf.size(); i++)
        viewer->addCoordinateSystem (1.0, axes_transf[i]);
    }
  }
  viewer->initCameraParameters ();
  
  
  while (!viewer->wasStopped ())
  {
    viewer->spinOnce (100);
    boost::this_thread::sleep (boost::posix_time::microseconds (100000));
  }
  
  return;
}


#define CV_EXT_INSTANTIATE_show3DPoints(_T) \
template void cv_ext::show3DPoints( const std::vector< _T > &points, \
                                    const std::string &win_name, \
                                    bool show_axes, const std::vector< Eigen::Affine3f > &axes_transf );

CV_EXT_INSTANTIATE( show3DPoints, CV_EXT_3D_POINT_TYPES );