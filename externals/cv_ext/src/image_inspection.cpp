#include "cv_ext/image_inspection.h"
#include "cv_ext/base.h"
#include "cv_ext/types.h"

static void getIntegralImageLinePattern( int width, int height, double &direction,
                                         std::vector<cv::Point> &line_pattern, bool &x_major )
{
  direction = cv_ext::normalizeAngle(direction);
  if( direction >= M_PI/2 )
    direction -= M_PI;
  else if ( direction < -M_PI/2 )
    direction += M_PI;
  
  cv::Point p0(0, 0), p1;  
  if( direction >= -M_PI/4 && direction < M_PI/4 )
  {
    x_major = true;
    p1 = cv::Point(width - 1, (width-1)*tan(direction));
  }
  else
  {
    x_major = false;
    if( direction > 0 )
      p1 = cv::Point((height-1)*1.0/tan(direction), height - 1);
    else
      p1 = cv::Point((-height +1)*1.0/tan(direction), -height + 1);
  }

  cv_ext::getLinePoints( p0, p1, line_pattern );
}

void cv_ext :: getLinePoints( const cv::Point &p0, const cv::Point &p1, 
                              std::vector<cv::Point> &line_pts )
{
  int dx =  std::abs(p0.x-p1.x), sx = p0.x<p1.x ? 1 : -1;
  int dy = -std::abs(p0.y-p1.y), sy = p0.y<p1.y ? 1 : -1;
  int err = dx+dy, e2;
  int line_size = std::max<int>(dx,-dy) + 1;
  
  line_pts.clear();
  line_pts.reserve(line_size);
  
  cv::Point p = p0;
  for( int i = 0; i < line_size; i++)
  {
    line_pts.push_back(p);
    e2 = 2*err;
    if (e2 >= dy) { err += dy; p.x += sx; } 
    if (e2 <= dx) { err += dx; p.y += sy; }
  }
}

template < typename _Tin, typename _Tout> 
  void cv_ext::computeIntagralImage( const cv::Mat& src_img, cv::Mat& int_img, double direction )
{
  int w = src_img.cols, h = src_img.rows;
  int_img = cv::Mat ( cv::Size ( w, h ), cv::DataType<_Tout>::type );

  bool x_major;
  std::vector<cv::Point> line_pts;  
  getIntegralImageLinePattern( w, h, direction, line_pts, x_major );
 
  cv::Point &p0 = line_pts[0], &p1 = line_pts[line_pts.size() - 1];
  _Tout sum = 0;
  
  if( x_major )
  { 
    int offset = p1.y - p0.y, init_offset, end_offset;
    if( offset < 0)
    {
      init_offset = 0;
      end_offset = -offset;
    }
    else
    {
      init_offset = -offset;
      end_offset = 0;      
    }
    for(int iy = init_offset; iy < h + end_offset; iy++)
    {
      for( int i = 0; i < line_pts.size(); i++ )
      {
        cv::Point &p = line_pts[i];
        int &x = p.x, y = p.y + iy;
        if( x >= 0 && y >= 0 && x < w && y < h )
        {
          sum += _Tout(src_img.at<_Tin>(y, x));
          int_img.at<_Tout>(y, x) = sum;
        }
      }
    }
  }
  else
  {
    int offset_x = p0.x - p1.x, offset_y;
 
    if(direction > 0)
    {
      offset_y = 0;
      for(int ix = w - 1; ix >= offset_x; ix--)
      {
        for( int i = 0; i < line_pts.size(); i++ )
        {
          cv::Point &p = line_pts[i];
          int x = p.x + ix, y = p.y + offset_y;
          if( x >= 0 && y >= 0 && x < w && y < h )
          {
            sum += _Tout(src_img.at<_Tin>(y, x));
            int_img.at<_Tout>(y, x) = sum;
          }
        }
      }
    }
    else
    {
      offset_y = h - 1;
      for(int ix = offset_x; ix < w; ix++)
      {
        for( int i = 0; i < line_pts.size(); i++ )
        {
          cv::Point &p = line_pts[i];
          int x = p.x + ix, y = p.y + offset_y;
          if( x >= 0 && y >= 0 && x < w && y < h )
          {
            sum += _Tout(src_img.at<_Tin>(y, x));
            int_img.at<_Tout>(y, x) = sum;
          }
        }
      }
    }
  }
}

template <typename _Tin >
  cv_ext::IntegralImage<_Tin >::IntegralImage( const cv::Mat& src_img, double direction ) :
  direction_(direction)
{
  computeIntagralImage<_Tin, double>( src_img, int_img_, direction_ );
  std::vector<cv::Point> line_pattern;
  w_ = src_img.cols; h_ = src_img.rows;
  getIntegralImageLinePattern( w_, h_, direction_, line_pattern, x_major_ );
  
  lut_.resize(line_pattern.size());
  if( x_major_ )
  {
    for( size_t i = 0; i < line_pattern.size(); i++)
      lut_[i] = line_pattern[i].y;
  }
  else
  {
    if( direction_ > 0 )
    {
      for( size_t i = 0; i < line_pattern.size(); i++)
        lut_[i] = line_pattern[i].x;
    }
    else
    {
      for( size_t i = 0, j = line_pattern.size() - 1; i < line_pattern.size(); i++, j--)
        lut_[i] = line_pattern[j].x;      
    }
  }
}

template <typename _Tin> 
  cv_ext::IntegralImage2<_Tin>::IntegralImage2( const cv::Mat& src_img, double direction )
{
  direction_ = cv_ext::normalizeAngle(direction);
  if( direction_ >= M_PI/2 )
    direction_ -= M_PI;
  else if ( direction_ < -M_PI/2 )
    direction_ += M_PI;
  
  computeIntagral( src_img );
}

template <typename _Tin> 
  void cv_ext::IntegralImage2<_Tin>::computeIntagral ( const cv::Mat& src_img )
{
  int src_w = src_img.cols, src_h = src_img.rows;
  cos_d_ = cos(direction_);
  sin_d_ = sin(direction_);
  
  cv::Point bp[4], p_ul(std::numeric_limits< int >::max(), std::numeric_limits< int >::max()), 
            p_br(std::numeric_limits< int >::min(), std::numeric_limits< int >::min());

  bp[0] = cv::Point(0,0);
  bp[1] = cv::Point(round( (src_w - 1)*cos_d_ ),round( (src_w - 1)*sin_d_ ));
  bp[2] = cv::Point(round( -(src_h - 1)*sin_d_ ), round( (src_h - 1)*cos_d_ ) );
  bp[3] = cv::Point(round ( (src_w - 1)*cos_d_ - (src_h - 1)*sin_d_), 
                    round( (src_w - 1)*sin_d_ + (src_h - 1)*cos_d_ ) );
  
  for(int i = 0; i < 4; i++ )
  {
    if( bp[i].x < p_ul.x ) p_ul.x = bp[i].x;
    if( bp[i].y < p_ul.y ) p_ul.y = bp[i].y;
    if( bp[i].x > p_br.x ) p_br.x = bp[i].x;
    if( bp[i].y > p_br.y ) p_br.y = bp[i].y;
  }
  
  w_ = p_br.x - p_ul.x + 1, h_ = p_br.y - p_ul.y + 1;
  offset_ = p_ul;
  int_img_ = cv::Mat ( cv::Size ( w_, h_ ), cv::DataType<double>::type, cv::Scalar(-1) );
  
  for( int y = 0; y < h_; y++)
  {
    double *int_img_p = int_img_.ptr<double>(y); 
    for( int x = 0; x < w_; x++, int_img_p++)
    {
      cv::Point p(x, y);
      p += offset_;
      cv::Point src_pt(round ( p.x*cos_d_ + p.y*sin_d_), round( -p.x*sin_d_ + p.y*cos_d_ ) );
      if(src_pt.x >= 0 && src_pt.y >= 0 && src_pt.x < src_w && src_pt.y < src_h )
        *int_img_p = src_img.at<_Tin>(src_pt.y, src_pt.x);
    }
  }
  double sum = 0;
  for( int y = 0; y < h_; y++)
  {
    double *int_img_p = int_img_.ptr<double>(y); 
    for( int x = 0; x < w_; x++, int_img_p++)
    {
      if( *int_img_p != -1 )
      {
        sum += *int_img_p;
        *int_img_p = sum;
      }
    }
  }
}


#define CV_EXT_INSTANTIATE_computeIntagralImage(_T) \
template void cv_ext::computeIntagralImage< _T, uint32_t >( const cv::Mat& src_img, cv::Mat& int_img, double direction ); \
template void cv_ext::computeIntagralImage< _T, uint64_t >( const cv::Mat& src_img, cv::Mat& int_img, double direction ); \
template void cv_ext::computeIntagralImage< _T, float >( const cv::Mat& src_img, cv::Mat& int_img, double direction ); \
template void cv_ext::computeIntagralImage< _T, double >( const cv::Mat& src_img, cv::Mat& int_img, double direction );

#define CV_EXT_INSTANTIATE_IntegralImage(_T) \
template class cv_ext::IntegralImage< _T >;

#define CV_EXT_INSTANTIATE_IntegralImage2(_T) \
template class cv_ext::IntegralImage2< _T >;

CV_EXT_INSTANTIATE( computeIntagralImage, CV_EXT_PIXEL_DEPTH_TYPES );
CV_EXT_INSTANTIATE( IntegralImage, CV_EXT_PIXEL_DEPTH_TYPES );
CV_EXT_INSTANTIATE( IntegralImage2, CV_EXT_PIXEL_DEPTH_TYPES );