#include <stdexcept>

#include "cv_ext/drawing.h"
#include "cv_ext/image_inspection.h"
#include "cv_ext/types.h"

template < typename _TPoint2 > void cv_ext::drawPoints(cv::Mat &img, const std::vector< _TPoint2 > &pts,
                                                       cv::Scalar color, double scale )
{
  drawCircles( img, pts, 1, color, scale );
}


template < typename _TPoint2, typename _T  > 
  void cv_ext::drawNormalsDirections( cv::Mat &img, const std::vector< _TPoint2 > &pts, 
                                      const std::vector< _T > &normals_dirs, cv::Scalar color, 
                                      int normal_length, double scale )
{
  float m, dx,dy;
  cv::Point p1, p2;
  int pts_size = pts.size(), width = img.cols, height = img.rows;
  for(int i = 0; i< pts_size; i++)
  {
    const float &xc = pts[i].x, &yc = pts[i].y;
    
    dx = normal_length*cos(normals_dirs[i]);
    dy = normal_length*sin(normals_dirs[i]);
    
    p1.x = round(scale*xc-dx/2);
    p1.y = round(scale*yc-dy/2);
    p2.x = round(scale*xc+dx/2);
    p2.y = round(scale*yc+dy/2);

    if( p1.x >= 0 && p1.y >= 0 && p2.x >= 0 && p2.y >= 0 &&
        p1.x < width && p1.y < height && p2.x < width && p2.y < height )
      cv::line(img, p1, p2, color, 1, 8, 0);
  }
}

template <typename _TVec4> void cv_ext::drawSegments( cv::Mat &img, const std::vector< _TVec4 > &segments,
                                                      cv::Scalar color, int tickness, double scale )
{
  cv::Point2i pt0, pt1;
  int seg_size = segments.size();
  scale = 1./scale;
  for(int i = 0; i < seg_size; i++)
  {
    const _TVec4 &s = segments[i];
    pt0 = cv::Point(round(scale*s[0]),round(scale*s[1]));
    pt1 = cv::Point(round(scale*s[2]),round(scale*s[3]));

    cv::line(img, pt0, pt1, color, tickness);
  }
}

// template <typename _TVec4> void cv_ext::drawSegmentsWithNormalsDirections( cv::Mat &img, cv::Mat &norm_dir_img,
//                                                                            const std::vector< _TVec4 > &segments,
//                                                                            cv::Scalar color, double scale )
// {
//   cv::Point2i pt0, pt1;
//   int seg_size = segments.size();
//   scale = 1./scale;
//   std::vector<cv::Point> line_pts;
//   for(int i = 0; i < seg_size; i++)
//   {
//     const _TVec4 &s = segments[i];
//     pt0 = cv::Point(round(scale*s[0]),round(scale*s[1]));
//     pt1 = cv::Point(round(scale*s[2]),round(scale*s[3]));
// 
//     getLinePoints( pt0, pt1, line_pts );
//     cv::line(img, pt0, pt1, color, tickness);
//   }
// }

template < typename _TPoint2 > void cv_ext::drawCircles( cv::Mat &img, const std::vector< _TPoint2 > &pts,
                                                         int radius, cv::Scalar color, double scale )
{
  int pts_size = pts.size(), width = img.cols, height = img.rows;
  for(int i = 0; i< pts_size; i++)
  {
    const float xc = scale*pts[i].x, yc = scale*pts[i].y;
    
    if( xc >= 0 && yc >= 0 && xc <= width - 1 && yc <= height - 1)
      cv::circle( img, cv::Point2f(round(xc),round(yc)),radius,cv::Scalar(color),-1,8 );
  }
}

cv::Mat cv_ext::drawDenseOptFlow( const cv::Mat &flow, const cv::Mat &img, int step, 
                                  cv::Scalar color, const cv::Mat &mask )
{
  if( flow.depth() != cv::DataType<float>::type || !flow.rows || !flow.cols || flow.channels() != 2 ||
      img.rows != flow.rows || img.cols != flow.cols || img.depth() != cv::DataType<uchar>::type ||
      (img.channels() != 1 && img.channels() != 3) ||
      (!mask.empty() && ( mask.depth() != cv::DataType<uchar>::type || mask.channels() != 1 ||
         mask.rows != flow.rows || mask.cols != flow.cols ) ) )
    throw std::invalid_argument("Unsopported images");
 
  cv::Mat flow_img(flow.rows, flow.cols, cv::DataType< cv::Vec< uchar, 3 > >::type );
  if( img.channels() == 1 )
    cv::cvtColor(img, flow_img, cv::COLOR_GRAY2BGR );
  else
    img.copyTo(flow_img);
  
  if( !mask.empty() )
  {
    for(int y = 0; y < flow_img.rows; y += step)
    {
      const uchar *mask_p = mask.ptr<uchar>(y);
      const cv::Point2f *flow_p = flow.ptr<cv::Point2f>(y);
      
      for(int x = 0; x < flow_img.cols; x += step, mask_p += step, flow_p += step ) 
      {
        if( *mask_p )
        {
          const cv::Point2f &fxy = *flow_p;
          cv::line(flow_img, cv::Point(x,y), 
          cv::Point( cvRound(x + fxy.x), cvRound(y +fxy.y) ), color);
        }
      }
    }
  }
  else
  {
    for(int y = 0; y < flow_img.rows; y += step)
    {
      const cv::Point2f *flow_p = flow.ptr<cv::Point2f>(y);
      
      for(int x = 0; x < flow_img.cols; x += step, flow_p += step ) 
      {
        const cv::Point2f &fxy = *flow_p;
        cv::line(flow_img, cv::Point(x,y), 
                  cv::Point( cvRound(x + fxy.x), cvRound(y +fxy.y) ), color);
      }
    }
  }
  
  return flow_img;
}



#define CV_EXT_INSTANTIATE_drawPoints(_T) \
template void cv_ext::drawPoints(cv::Mat &img, const std::vector< _T > &pts, \
                                 cv::Scalar color, double scale );

#define CV_EXT_INSTANTIATE_drawNormalsDirections(_T) \
template void cv_ext::drawNormalsDirections( cv::Mat &img, const std::vector< _T > &pts, \
                                             const std::vector< float > &normals_dirs, cv::Scalar color, \
                                             int normal_length, double scale ); \
template void cv_ext::drawNormalsDirections( cv::Mat &img, const std::vector< _T > &pts, \
                                             const std::vector< double > &normals_dirs, cv::Scalar color, \
                                             int normal_length, double scale );
                                         
#define CV_EXT_INSTANTIATE_drawSegments(_T) \
template void cv_ext::drawSegments( cv::Mat &img, const std::vector< _T > &segments, \
                                    cv::Scalar color, int tickness, double scale );
                        
#define CV_EXT_INSTANTIATE_drawCircles(_T) \
template void cv_ext::drawCircles( cv::Mat &img, const std::vector< _T > &pts, \
                                   int radius, cv::Scalar color, double scale );
                                   
CV_EXT_INSTANTIATE( drawPoints, CV_EXT_2D_POINT_TYPES );
CV_EXT_INSTANTIATE( drawNormalsDirections, CV_EXT_2D_POINT_TYPES );
CV_EXT_INSTANTIATE( drawSegments, CV_EXT_4D_VECTOR_TYPES );
CV_EXT_INSTANTIATE( drawCircles, CV_EXT_2D_POINT_TYPES );