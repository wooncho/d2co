#include <stdexcept>

#include "cv_ext/image_pyramid.h"

using namespace cv_ext;

ImagePyramid::ImagePyramid() :
  pyr_num_levels_(0),
  pyr_scale_factor_(0.0),
  gaussian_pyr_(true) {}

void ImagePyramid::buildFromImage ( const cv::Mat& img, int pyr_levels, int pyr_img_type, 
                                    bool deep_copy, double pyr_scale_factor, bool gaussian_pyr, 
                                    InterpolationType interp_type )
{
  
  if ( pyr_levels < 1 || pyr_scale_factor < 1.0 || pyr_scale_factor > 2.0 )
    throw std::invalid_argument ( "Number of pyramid levels must be >= 1,\
                                   pyramid scale factor must be in the range [1,2]" );
  
  pyr_num_levels_ = pyr_levels; 
  pyr_scale_factor_ = pyr_scale_factor; 
  gaussian_pyr_ = gaussian_pyr;
  interp_type_ = interp_type;
  
  pyr_imgs_.clear();
  scales_.clear();
  
  pyr_imgs_.resize(pyr_levels);
  scales_.resize(pyr_levels);
  
  scales_[0] = 1.0;
  for ( int i = 1; i < pyr_num_levels_; i++ )
    scales_[i] = scales_[i-1]*pyr_scale_factor_;
  
  if( img.type() == pyr_img_type )
  {
    if( deep_copy )
      pyr_imgs_[0] = img.clone();
    else
      pyr_imgs_[0] = cv::Mat(img);
  }
  else
    img.convertTo( pyr_imgs_[0], pyr_img_type );
}

double ImagePyramid::getScale ( int scale_index ) const
{
  if( scale_index < 0 || scale_index >= pyr_num_levels_ )
    throw std::invalid_argument("Scale index out of range");
  
  return scales_[scale_index];
}

cv::Mat& ImagePyramid::operator[]( int scale_index )
{
  if( scale_index < 0 || scale_index >= pyr_num_levels_ )
    throw std::invalid_argument("Scale index out of range");
  
  if( pyr_imgs_[scale_index].empty() )
    computeScaleLevel( scale_index );
  return pyr_imgs_[scale_index]; 
}

const cv::Mat& ImagePyramid::at( int scale_index )
{
  return operator[](scale_index);
}

const cv::Mat& ImagePyramid::getImageAt ( int scale_index )
{
  return operator[](scale_index);
}
  
void ImagePyramid::computeScaleLevel ( int scale_index )
{
  // Scale down to obtain the required pyramid level
  if( pyr_scale_factor_ == 2.0 && gaussian_pyr_ && 
      interp_type_ == cv_ext::INTERP_NEAREST_NEIGHBOR)
  {
    // Use pyrDown() for scale factor 2
    for ( int i = 1; i <= scale_index; i++ )
    {
      if( pyr_imgs_[i].empty() )
        cv::pyrDown(pyr_imgs_[i-1], pyr_imgs_[i]);
    }
  }
  else
  {
    // ... otherwise, use resize()
    double scale = 1.0/pyr_scale_factor_;
    
    int interp_type;
    switch( interp_type_ )
    {
      case cv_ext::INTERP_NEAREST_NEIGHBOR:
        interp_type = cv::INTER_NEAREST;
        break;
      case cv_ext::INTERP_BILINEAR:
        interp_type = cv::INTER_LINEAR;
        break;
      case cv_ext::INTERP_BICUBIC:
        interp_type = cv::INTER_CUBIC;
        break;
      default:
        interp_type = cv::INTER_NEAREST;
        break;
    }
    
    for ( int i = 1; i <= scale_index; i++ )
    {
      if( pyr_imgs_[i].empty() )
      {
        cv::Mat src_img;
        if( gaussian_pyr_ )
          // TODO Check std_dev here!!
          cv::GaussianBlur( pyr_imgs_[i-1], src_img, cv::Size(5,5), pyr_scale_factor_/2 );
        else
          src_img = pyr_imgs_[i-1];
        
        cv::resize(src_img, pyr_imgs_[i], cv::Size(0,0), scale, scale, interp_type );
      }
    }
  }
}
