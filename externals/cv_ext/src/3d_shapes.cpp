#include <cmath>
#include <iostream>
#include <stdexcept>
#include <boost/concept_check.hpp>
#include <opencv2/opencv.hpp>

#include <Eigen/Geometry> 


#include "cv_ext/base.h"
#include "cv_ext/types.h"
#include "cv_ext/3d_shapes.h"

static inline Eigen::Matrix3d computeXRot( double theta_x )
{
  Eigen::Matrix3d rot;
  rot << 1.0,                   0.0,                   0.0,
         0.0,                   cos ( theta_x ),       -sin ( theta_x ),
         0.0,                   sin ( theta_x ),      cos ( theta_x );
  return rot;
}

static inline Eigen::Matrix3d computeYRot( double theta_y )
{
  Eigen::Matrix3d rot;
  rot << cos ( theta_y ),       0.0,                   sin ( theta_y ),
         0.0,                   1.0,                   0.0,
         -sin ( theta_y ),       0.0,                   cos ( theta_y );
  return rot;
}

static inline Eigen::Matrix3d computeZRot( double theta_z )
{
  Eigen::Matrix3d rot;
  rot << cos ( theta_z ),       -sin ( theta_z ),       0.0,
         sin ( theta_z ),      cos ( theta_z ),       0.0,
         0.0,                   0.0,                   1.0;
  return rot;
}

template < typename _TPoint3D > 
  static void sampleRotations( _TPoint3D &vec, std::vector< Eigen::Quaterniond > &quat_rotations, 
                               int num_rotations, cv_ext::CoordinateAxis aligned_axis )
{
  if( num_rotations < 1 )
    throw std::invalid_argument("num_rotations must be >= 1");
    
  double theta_0, theta_1 = 0, theta_1_step = 2*M_PI/num_rotations;
  Eigen::Vector3d n_vec( double(vec.x), double(vec.y), double(vec.z) ), axis_vec, r_vec;
  n_vec /= n_vec.norm();
  Eigen::Matrix3d init_rot, final_rot, tot_rot;
  
  switch( aligned_axis )
  {
    case cv_ext::COORDINATE_X_AXIS:
      axis_vec = Eigen::Vector3d( 1, 0, 0 );
      break;
      
    case cv_ext::COORDINATE_Y_AXIS:
      axis_vec = Eigen::Vector3d( 0, 1, 0 );
      break;
      
    default:
    case cv_ext::COORDINATE_Z_AXIS:
      axis_vec = Eigen::Vector3d( 0, 0, 1 );
      break;
  }

  if( (n_vec - axis_vec).norm() < std::numeric_limits< double>::epsilon() )
    init_rot = Eigen::Matrix3d::Identity();
  else if( (n_vec + axis_vec).norm() < std::numeric_limits< double>::epsilon() )
  {
    switch( aligned_axis )
    {
      case cv_ext::COORDINATE_X_AXIS:
        init_rot << -1,  0,  0, 
                     0, -1,  0, 
                     0,  0,  1;
        break;
        
      case cv_ext::COORDINATE_Y_AXIS:
        init_rot <<  1,  0,  0, 
                     0, -1,  0, 
                     0,  0, -1;
        break;
        
      default:
      case cv_ext::COORDINATE_Z_AXIS:
        init_rot << -1,  0,  0, 
                     0,  1,  0, 
                     0,  0, -1;
        break;
    }
  }
  else
  {
    r_vec = axis_vec.cross(n_vec);
    
    theta_0 = std::asin(r_vec.norm());
    r_vec /= r_vec.norm();
    Eigen::Matrix3d rot_0, rot_1;
    rot_0 = Eigen::AngleAxisd(theta_0, r_vec);
    rot_1 = Eigen::AngleAxisd(M_PI - theta_0, r_vec);
    
    if( (rot_0*axis_vec - n_vec).norm() < (rot_1*axis_vec - n_vec).norm() )
      init_rot = rot_0;
    else
      init_rot = rot_1;
  }
  
  // Rotate around the selected axes
  for( int i = 0; i < num_rotations; i++, theta_1+= theta_1_step )
  {
    final_rot =  Eigen::AngleAxisd(theta_1, n_vec);
    tot_rot = final_rot*init_rot;
    quat_rotations.push_back( Eigen::Quaterniond(tot_rot) );
  }
}

template < typename _TPoint3D > 
  void cv_ext::sampleRotationsAroundVector( _TPoint3D &vec, std::vector< Eigen::Quaterniond > &quat_rotations, 
                                            int num_rotations, CoordinateAxis aligned_axis )
{
  quat_rotations.clear();
  sampleRotations( vec, quat_rotations, num_rotations, aligned_axis );
}

template < typename _TPoint3D > 
  void cv_ext::sampleRotationsAroundVectors( std::vector< _TPoint3D> &vecs, std::vector< Eigen::Quaterniond > &quat_rotations, 
                                             int num_rotations, CoordinateAxis aligned_axis )
{
  quat_rotations.clear();
  for( int i = 0; i < vecs.size(); i++ )
    sampleRotations( vecs[i], quat_rotations, num_rotations, aligned_axis );
}

// Icosphere implementaion inspired by: 
// http://blog.andreaskahler.com/2009/06/creating-icosphere-mesh-in-code.html

static const int ICOSPHERE_NUM_VERTEXS = 12;
static const int ICOSPHERE_NUM_FACES = 20;

struct IcosphereTriangle
{ 
  IcosphereTriangle(){};
  IcosphereTriangle( int v0_idx, int v1_idx, int v2_idx) :
    v0_idx(v0_idx), v1_idx(v1_idx), v2_idx(v2_idx){};
  int v0_idx, v1_idx, v2_idx;   
};

template < typename _TPoint3D > 
  static inline IcosphereTriangle computeInscribedTriangle( const IcosphereTriangle& triangle, 
                                                            std::vector< _TPoint3D > &sphere_points,
                                                            cv::Mat &idx_mask )
{
  IcosphereTriangle inscribed;
  _TPoint3D new_pt;
  
  // Checks if the point has already been inserted
  if( idx_mask.at<int32_t>( triangle.v0_idx, triangle.v1_idx ) < 0 && 
      idx_mask.at<int32_t>( triangle.v1_idx, triangle.v0_idx ) < 0 )
  {
    // New point: compute the point ...
    new_pt.x = 0.5* (sphere_points[triangle.v0_idx].x + sphere_points[triangle.v1_idx].x );
    new_pt.y = 0.5* (sphere_points[triangle.v0_idx].y + sphere_points[triangle.v1_idx].y );
    new_pt.z = 0.5* (sphere_points[triangle.v0_idx].z + sphere_points[triangle.v1_idx].z );
    new_pt = cv_ext::normalize3DPoint(new_pt);
    // ... add the index to the triangle...
    inscribed.v0_idx = sphere_points.size();
    // ... add the point to the point vector ...
    sphere_points.push_back( new_pt );
    // ... and update the mask
    idx_mask.at<int32_t>( triangle.v0_idx, triangle.v1_idx ) = inscribed.v0_idx;
    idx_mask.at<int32_t>( triangle.v1_idx, triangle.v0_idx ) = inscribed.v0_idx;
  }
  else
  {
    // Otherwise, just retrive and store the index
    inscribed.v0_idx = idx_mask.at<int32_t>( triangle.v0_idx, triangle.v1_idx );
  }
  
  // Checks if the point has already been inserted
  if( idx_mask.at<int32_t>( triangle.v1_idx, triangle.v2_idx ) < 0 && 
      idx_mask.at<int32_t>( triangle.v2_idx, triangle.v1_idx ) < 0 )
  {
    // New point: compute the point ...
    new_pt.x = 0.5*( sphere_points[triangle.v1_idx].x + sphere_points[triangle.v2_idx].x );
    new_pt.y = 0.5*( sphere_points[triangle.v1_idx].y + sphere_points[triangle.v2_idx].y );
    new_pt.z = 0.5*( sphere_points[triangle.v1_idx].z + sphere_points[triangle.v2_idx].z );
    new_pt = cv_ext::normalize3DPoint(new_pt);
    // ... add the index to the triangle...
    inscribed.v1_idx = sphere_points.size();
    // ... add the point to the point vector ...
    sphere_points.push_back( new_pt );
    // ... and update the mask
    idx_mask.at<int32_t>( triangle.v1_idx, triangle.v2_idx ) = inscribed.v1_idx;
    idx_mask.at<int32_t>( triangle.v2_idx, triangle.v1_idx ) = inscribed.v1_idx;
  }
  else
  {
    // Otherwise, just retrive and store the index
    inscribed.v1_idx = idx_mask.at<int32_t>( triangle.v1_idx, triangle.v2_idx );
  }
  
  // Checks if the point has already been inserted
  if( idx_mask.at<int32_t>( triangle.v2_idx, triangle.v0_idx ) < 0 && 
      idx_mask.at<int32_t>( triangle.v0_idx, triangle.v2_idx ) < 0 )
  {
    // New point: compute the point ...
    new_pt.x = 0.5*( sphere_points[triangle.v2_idx].x + sphere_points[triangle.v0_idx].x );
    new_pt.y = 0.5*( sphere_points[triangle.v2_idx].y + sphere_points[triangle.v0_idx].y );
    new_pt.z = 0.5*( sphere_points[triangle.v2_idx].z + sphere_points[triangle.v0_idx].z );
    new_pt = cv_ext::normalize3DPoint(new_pt);
    // ... add the index to the triangle...
    inscribed.v2_idx = sphere_points.size();
    // ... add the point to the point vector ...
    sphere_points.push_back( new_pt );
    // ... and update the mask
    idx_mask.at<int32_t>( triangle.v2_idx, triangle.v0_idx ) = inscribed.v2_idx;
    idx_mask.at<int32_t>( triangle.v0_idx, triangle.v2_idx ) = inscribed.v2_idx;
  }
  else
  {
    // Otherwise, just retrive and store the index
    inscribed.v2_idx = idx_mask.at<int32_t>( triangle.v2_idx, triangle.v0_idx );
  }
  
  return inscribed;
}

template < typename _TPoint3D > 
  static void icosphereRefinement( const std::vector< IcosphereTriangle > &src_triangles, 
                                   std::vector< IcosphereTriangle > &dst_triangles, 
                                   std::vector< _TPoint3D > &sphere_points )
{
  int src_triangles_size = src_triangles.size(),
      dst_triangles_size = 4*src_triangles_size,
      pts_size = sphere_points.size();
  
  sphere_points.reserve( sphere_points.size() + 3*src_triangles_size/2 );
  
  dst_triangles.clear();
  dst_triangles.reserve ( dst_triangles_size );
  
  cv::Mat idx_mask(pts_size, pts_size, cv::DataType< int32_t >::type, cv::Scalar(-1) );
  
  for ( int i=0; i < src_triangles_size; i++ )
  {
    const IcosphereTriangle &src_triangle = src_triangles[i];
    IcosphereTriangle inscribed = computeInscribedTriangle< _TPoint3D > ( src_triangle, sphere_points, idx_mask );
    
    // Split the old triangle in 4 smaller triangles, starting from the inscribed one
    dst_triangles.push_back( inscribed );
    dst_triangles.push_back( IcosphereTriangle( inscribed.v0_idx, inscribed.v1_idx, src_triangle.v1_idx) );
    dst_triangles.push_back( IcosphereTriangle( src_triangle.v2_idx, inscribed.v1_idx, inscribed.v2_idx ) );
    dst_triangles.push_back( IcosphereTriangle( src_triangle.v0_idx, inscribed.v0_idx, inscribed.v2_idx ) );
  }
}


template < typename _TPoint3D > 
  void cv_ext::createIcosphere( std::vector< _TPoint3D > &sphere_points, 
                                int num_refinements, double radius )
{
  if( num_refinements <= 0 || radius <= 0 )
    throw std::invalid_argument("num_refinements and radius parameters must be positive");
  
  double tau = ( 1.0 + std::sqrt ( 5.0 ) ) /2.0;

  // Inizialization vertexs of icosahedron
  sphere_points.resize ( ICOSPHERE_NUM_VERTEXS );

  sphere_points[0].x = -1.0;  sphere_points[0].y = tau;  sphere_points[0].z = 0.0;
  sphere_points[1].x = 1.0;   sphere_points[1].y = tau;  sphere_points[1].z = 0.0;
  sphere_points[2].x = -1.0;  sphere_points[2].y = -tau; sphere_points[2].z = 0.0;
  sphere_points[3].x = 1.0;   sphere_points[3].y = -tau; sphere_points[3].z = 0.0;

  sphere_points[4].x = 0.0;   sphere_points[4].y = -1.0; sphere_points[4].z = tau;
  sphere_points[5].x = 0.0;   sphere_points[5].y = 1.0;  sphere_points[5].z = tau;
  sphere_points[6].x = 0.0;   sphere_points[6].y = -1.0; sphere_points[6].z = -tau;
  sphere_points[7].x = 0.0;   sphere_points[7].y = 1.0;  sphere_points[7].z = -tau;

  sphere_points[8].x = tau;   sphere_points[8].y = 0.0;  sphere_points[8].z = -1.0;
  sphere_points[9].x = tau;   sphere_points[9].y = 0.0;  sphere_points[9].z = 1.0;
  sphere_points[10].x = -tau; sphere_points[10].y = 0.0; sphere_points[10].z = -1.0;
  sphere_points[11].x = -tau; sphere_points[11].y = 0.0; sphere_points[11].z = 1.0;

  for ( int i=0; i < ICOSPHERE_NUM_VERTEXS; i++ )
    sphere_points[i] = cv_ext::normalize3DPoint(sphere_points[i]);
  
  std::vector< IcosphereTriangle > triangles_buf[2];
  std::vector< IcosphereTriangle > &triangles = triangles_buf[0];
  
  triangles.resize ( ICOSPHERE_NUM_FACES );

  // 5 faces around vertex 0
  triangles[0] = IcosphereTriangle( 0, 11, 5 );
  triangles[1] = IcosphereTriangle( 0, 5, 1 );
  triangles[2] = IcosphereTriangle( 0, 1, 7 );
  triangles[3] = IcosphereTriangle( 0, 7, 10 );
  triangles[4] = IcosphereTriangle( 0, 10, 11 );

  // 5 adjacent faces
  triangles[5] = IcosphereTriangle( 1, 5, 9 );
  triangles[6] = IcosphereTriangle( 5, 11, 4 );
  triangles[7] = IcosphereTriangle( 11, 10, 2 );
  triangles[8] = IcosphereTriangle( 10, 7, 6 );
  triangles[9] = IcosphereTriangle( 7, 1, 8 );

  //5 faces around point 3
  triangles[10] = IcosphereTriangle( 3, 9, 4 );
  triangles[11] = IcosphereTriangle( 3, 4, 2 );
  triangles[12] = IcosphereTriangle( 3, 2, 6 );
  triangles[13] = IcosphereTriangle( 3, 6, 8 );
  triangles[14] = IcosphereTriangle( 3, 8, 9 );

  //5 adjacent faces
  triangles[15] = IcosphereTriangle( 4, 9, 5 );
  triangles[16] = IcosphereTriangle( 2, 4, 11 );
  triangles[17] = IcosphereTriangle( 6, 2, 10 );
  triangles[18] = IcosphereTriangle( 8, 6, 7 );
  triangles[19] = IcosphereTriangle( 9, 8, 1 );

  for ( int ref_i=1; ref_i <= num_refinements; ref_i++ )
  {
    std::vector< IcosphereTriangle > &src_triangles = triangles_buf[!(ref_i%2)],
                                     &dst_triangles = triangles_buf[ref_i%2];
   
    icosphereRefinement< _TPoint3D >( src_triangles, dst_triangles, sphere_points );
  }
  
  if( radius != 1.0)
  {
    for( int i = 0; i < sphere_points.size(); i++ )
    {
      sphere_points[i].x *= radius;
      sphere_points[i].y *= radius;
      sphere_points[i].z *= radius;
    }
  }
}

template < typename _TPoint3D > 
  void cv_ext::createIcospherePolarCap( std::vector<_TPoint3D> &cap_points, int num_refinements, 
                                        double latitude_angle, double radius, bool only_north_cap )
{
  std::vector<_TPoint3D> sphere_points;
  createIcosphere( sphere_points, num_refinements, radius );
  if( latitude_angle <= 0 )
  {
    cap_points = sphere_points;
    return;
  }
  else if( latitude_angle >= M_PI/2 )
    latitude_angle = M_PI/2;
  
  int pts_size = sphere_points.size();

  cap_points.clear();
  cap_points.reserve( pts_size );
  
  for( int i = 0; i < pts_size; i++)
  {
    double dist_z = std::sqrt(sphere_points[i].x*sphere_points[i].x + sphere_points[i].y*sphere_points[i].y);
    if( atan(sphere_points[i].z/dist_z) >= latitude_angle )
      cap_points.push_back(sphere_points[i]);
    if( !only_north_cap && atan(sphere_points[i].z/dist_z) <= -latitude_angle )
      cap_points.push_back(sphere_points[i]);
  }
}

#define CV_EXT_INSTANTIATE_sampleRotationsAroundVector(_T) \
template void cv_ext::sampleRotationsAroundVector( _T &vec, std::vector< Eigen::Quaterniond > &quat_rotations, \
                                                   int num_rotations, cv_ext::CoordinateAxis aligned_axis );
#define CV_EXT_INSTANTIATE_sampleRotationsAroundVectors(_T) \
template void cv_ext::sampleRotationsAroundVectors( std::vector< _T> &vecs, std::vector< Eigen::Quaterniond > &quat_rotations, \
                                                    int num_rotations, cv_ext::CoordinateAxis aligned_axis );
#define CV_EXT_INSTANTIATE_createIcosphere(_T) \
template void cv_ext::createIcosphere( std::vector<_T> &sphere_points, int refinements, double radius );
#define CV_EXT_INSTANTIATE_createIcospherePolarCap(_T) \
template void cv_ext::createIcospherePolarCap( std::vector<_T> &cap_points, int num_refinements, \
                                               double latitude_angle, double radius, bool only_north_cap );

CV_EXT_INSTANTIATE( sampleRotationsAroundVector, CV_EXT_3D_POINT_TYPES );
CV_EXT_INSTANTIATE( sampleRotationsAroundVectors, CV_EXT_3D_POINT_TYPES );
CV_EXT_INSTANTIATE( createIcosphere, CV_EXT_3D_POINT_TYPES );
CV_EXT_INSTANTIATE( createIcospherePolarCap, CV_EXT_3D_POINT_TYPES );