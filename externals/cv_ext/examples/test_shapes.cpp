#include <iostream>
#include "cv_ext/3d_shapes.h"
#include "cv_ext/debug_tools.h"

using namespace cv_ext;
using namespace Eigen;

int main(int argc, char** argv)
{  
  int refinements = 4;
  
  std::vector <cv::Point3f> points_icosphere, points_caps;

  createIcosphere( points_icosphere, refinements );
  std::cout<<"Num sphere points : "<<points_icosphere.size()<<std::endl;
  show3DPoints( points_icosphere );
  
  refinements = 2;
  
  createIcospherePolarCap( points_caps, refinements, M_PI/4 );
  std::cout<<"Num cap points : "<<points_caps.size()<<std::endl;
  show3DPoints( points_caps, "points_caps", true );
  
  
  std::vector< Quaterniond > quat_rotations;
  sampleRotationsAroundVectors( points_caps, quat_rotations, 1, COORDINATE_Z_AXIS );
  std::vector< Affine3f > axes_transf;
  for( int i = 0; i < quat_rotations.size(); i++ )
  {
    Affine3f transf;
    transf.fromPositionOrientationScale( Vector3f::Zero(), quat_rotations[i].cast<float>(), Vector3f::Ones());
    axes_transf.push_back(transf);
  }
  show3DPoints( points_caps, "rotations", true, axes_transf );
      
  createIcospherePolarCap( points_caps, refinements, M_PI/2, 1, true );
  std::cout<<"Num cap points : "<<points_caps.size()<<std::endl;
  
  sampleRotationsAroundVectors( points_caps, quat_rotations, 6, COORDINATE_Z_AXIS );
  axes_transf.clear();
  for( int i = 0; i < quat_rotations.size(); i++ )
  {
    Affine3f transf;
    transf.fromPositionOrientationScale( Vector3f::Zero(), quat_rotations[i].cast<float>(), Vector3f::Ones());
    axes_transf.push_back(transf);
  }
  show3DPoints( points_caps, "rotations", true, axes_transf );
  return 0;
}