#include "cv_ext/pinhole_scene_projector.h"
#include "cv_ext/simple_timer.h"

int main(int argc, char** argv)
{
  typedef double point_3Dtype;
  typedef float point_2Dtype;

  int num_samples = 1000;  
  if( argc > 1)
    num_samples = atoi(argv[1]);

  std::cout<<"Using "<<num_samples<<" sample points, precision 3D: "<<sizeof(point_3Dtype)
           <<" bits, precision 2D: "<<sizeof(point_2Dtype)<<" bits"<<std::endl;
  
  cv::Mat r_vec(cv::Mat_<double>(3,1, 0.0)),
          t_vec(cv::Mat_<double>(3,1, 0.0));
  t_vec.at<double>(2) = 1.0;
  
  double fx = 517.30, fy = 516.50, 
        cx = 318.60, cy = 255.30;
        
  cv::Mat camera_matrix = (cv::Mat_<double>(3,3) << fx, 0, cx, 0, fy, cy, 0, 0, 1);
  cv::Mat dist_coeff = (cv::Mat_<double>(8,1) <<   -4.18023e-1, 5.07152440636e-1, 9.7214487e-4, 2.44e-5, -5.7843597214487474e-1, 
                        -2e-5, 4e-6, 1e-7);
  
  //dist_coeff = cv::Mat::zeros(8,1,CV_64F);
  std::vector< cv::Point3_<point_3Dtype> > sample_pts;
  std::vector< cv::Point_<point_2Dtype> > proj_pts_sp_single, 
                                          proj_pts_sp_parallel,
                                          proj_pts_sp_scaled,
                                          norm_pts_sp_single,
                                          norm_pts_sp_parallel;
                                          
  // OpenCV does not support different types
  std::vector< cv::Point_<point_3Dtype> >proj_pts_cv, norm_pts_cv;
 
  sample_pts.reserve(num_samples);
  double r1 , r2 , r3;
  for(int i = 0; i < num_samples; i++)
  {
    r1 = -0.5 + static_cast <double> (rand()) / static_cast <double> (RAND_MAX);
    r2 = -0.5 + static_cast <double> (rand()) / static_cast <double> (RAND_MAX);
    r3 = -0.5 + static_cast <double> (rand()) / static_cast <double> (RAND_MAX);        
    sample_pts.push_back(cv::Point3_<point_3Dtype>(r1, r2, 1.0 + r3));
  }
  
  cv_ext::BasicTimer timer;
  cv_ext::PinholeCameraModel cam_model( camera_matrix, 640, 480, 1.0, dist_coeff ),
                             scaled_cam_model( camera_matrix, 640, 480, 2.0, dist_coeff );
  cv_ext::PinholeSceneProjector s_proj( cam_model );
  
  cv_ext::PinholeSceneProjector scaled_s_proj( scaled_cam_model );
  
  // PinholeSceneProjector parallel
  s_proj.enableParallelism( true );
  s_proj.setTransformation(r_vec, t_vec );
  timer.reset();
  s_proj.projectPoints( sample_pts, proj_pts_sp_parallel );
  std::cout<<"PinholeSceneProjector projection parallel: elapsedTime :"<<timer.elapsedTimeUs()<<" usec\n";

  // PinholeSceneProjector single thread
  s_proj.enableParallelism( false );
  
  timer.reset();
  s_proj.projectPoints( sample_pts, proj_pts_sp_single );
  std::cout<<"PinholeSceneProjector projection single: elapsedTime :"<<timer.elapsedTimeUs()<<" usec\n";
  
  // Open cv
  timer.reset();
  cv::projectPoints(sample_pts, r_vec, t_vec, camera_matrix,  dist_coeff, proj_pts_cv );
  std::cout<<"OpenCV projection: elapsedTime :"<<timer.elapsedTimeUs()<<" usec\n";
  
  bool test_passed = true;
  for(int i = 0; i < num_samples; i++)
  {
    cv::Point_<point_2Dtype> pt_cv = proj_pts_cv[i];
    cv::Point_<point_2Dtype> diff1(proj_pts_sp_parallel[i] - pt_cv);
    cv::Point_<point_2Dtype> diff2(proj_pts_sp_single[i] - pt_cv);
    
    double dist1 = sqrt(diff1.ddot(diff1)), dist2 = sqrt(diff2.ddot(diff2));
    if( dist1 > 1e-7 || dist2 > 1e-7)
    {
      std::cout<<"Warning!!! PinholeSceneProjector vs OpenCV projected points difference :"<<std::endl
               <<"parallel : "<<std::endl<<proj_pts_sp_parallel[i] - pt_cv<<std::endl
               <<"single thread : "<<std::endl<<proj_pts_sp_single[i] - pt_cv<<std::endl;
    
      test_passed = false;
    }
  }
  
  scaled_s_proj.setTransformation(r_vec, t_vec );
  scaled_s_proj.projectPoints( sample_pts, proj_pts_sp_scaled );
  for(int i = 0; i < num_samples; i++)
  {
    cv::Point_<point_2Dtype> pt_scaled = proj_pts_sp_single[i];
    pt_scaled.x /= 2.0; pt_scaled.y /= 2.0;
    cv::Point_<point_2Dtype> diff(pt_scaled - proj_pts_sp_scaled[i]);
    double dist = sqrt(diff.ddot(diff));
    if( dist > 1e-7)
    {
      std::cout<<"Warning!!! PinholeSceneProjector scale factor does not work properly :"<<std::endl
               <<"defference : "<<std::endl<<pt_scaled - proj_pts_sp_scaled[i]<<std::endl;
    
      test_passed = false;
    }
  }

  s_proj.enableParallelism( true );
  timer.reset();
  s_proj.normalizePoints( proj_pts_sp_parallel, norm_pts_sp_parallel );
  std::cout<<"PinholeSceneProjector normalization parallel: elapsedTime :"<<timer.elapsedTimeUs()<<" usec\n";
  
  s_proj.enableParallelism( false );
  timer.reset();
  s_proj.normalizePoints( proj_pts_sp_single, norm_pts_sp_single );
  std::cout<<"PinholeSceneProjector normalization single: elapsedTime :"<<timer.elapsedTimeUs()<<" usec\n";
  
  timer.reset();
  cv::undistortPoints(proj_pts_cv, norm_pts_cv, camera_matrix, dist_coeff );
  std::cout<<"OpenCV normalization: elapsedTime :"<<timer.elapsedTimeUs()<<" usec\n";

  
  for(int i = 0; i < num_samples; i++)
  {
    cv::Point_<point_2Dtype> pt_cv = norm_pts_cv[i];
    cv::Point_<point_2Dtype> diff1(norm_pts_sp_parallel[i] - pt_cv);
    cv::Point_<point_2Dtype> diff2(norm_pts_sp_single[i] - pt_cv);
    
    double dist1 = sqrt(diff1.ddot(diff1)), dist2 = sqrt(diff2.ddot(diff2));
    if( dist1 > 1e-5 || dist2 > 1e-5)
    {
      std::cout<<"Warning!!! PinholeSceneProjector vs OpenCV normalized points difference :"<<std::endl
               <<"parallel : "<<std::endl<<norm_pts_sp_parallel[i] - pt_cv<<std::endl
               <<"single thread : "<<std::endl<<norm_pts_sp_single[i] - pt_cv<<std::endl;
    
      test_passed = false;
    }
  }
  
  if( !test_passed )
    std::cout<<"Warning!!! Test NOT passed!"<<std::endl;
  else
    std::cout<<"OK: Test passed"<<std::endl;
  
  return 0;
}