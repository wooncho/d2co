#include "cv_ext/image_inspection.h"
#include "cv_ext/timer.h"
#include "cv_ext/debug_tools.h"

int main()
{
  int w = 1280, h = 1024;
  cv::Mat test_img = cv::Mat ( cv::Size ( w, h ),
                               cv::DataType<uchar>::type, cv::Scalar(1) );
  
  int iter = 0;
  int n_pts = 10000000;
  std::vector<cv::Point> i_pts, e_pts;
  std::vector<int> lens;
  i_pts.resize(n_pts);
  e_pts.resize(n_pts);
  lens.resize(n_pts);
  cv_ext::BasicTimer timer;
  std::vector<cv_ext::IntegralImage<uchar> > int_imgs;
  int_imgs.reserve(60);
  
  timer.reset();
  
  for( double direction = -M_PI/2; direction <= M_PI/2; direction += M_PI/60.0)
  {
    int_imgs.push_back( cv_ext::IntegralImage<uchar>( test_img, direction ) );
  }
  
  std::cout<<"Elapsed time to compute "<<int_imgs.size()<<" ["<<w<<"X"<<h<<"] integral images : "<<timer.elapsedTimeMs()<<std::endl;

  for( int i = 0; i < int_imgs.size(); i++ )
  {    
    double direction = int_imgs[i].direction();
    
    for( int j = 0; j < n_pts; j++ )
    {
      int radius = rand()%100;
      cv::Point p0( rand()%test_img.cols, rand()%test_img.rows), 
                p1(radius*cos(direction), radius*sin(direction));
      int len = abs(p1.x) > abs(p1.y) ? abs(p1.x) + 1: abs(p1.y) + 1; 
      p1 += p0;
      
      i_pts[j] = p0;
      e_pts[j] = p1;
      lens[j] = len;
    }    

    timer.reset();
    for( int j = 0; j < n_pts; j++ )
    {
      int len;
      double sum = int_imgs[i].getLineSum(i_pts[j], e_pts[j], len);
      if(sum != std::numeric_limits< double >::max())
      {
        if(sum != lens[j] && len != lens[j] )
          std::cout<<"Warning!!! IntegralImage::getLineSum() return "<<sum
                   <<" and length "<<len
                   <<" with a line of actual sum "<<lens[j]
                   <<" (direction :"<<direction<<" )"<<std::endl;
      }
    }
    std::cout<<"Elapsed time for "<<n_pts<<" segments : "<<timer.elapsedTimeMs()<<std::endl;
  }
   
  return 0;
}