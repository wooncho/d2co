#pragma once

#include <vector>
#include <opencv2/opencv.hpp>

#include "cv_ext/types.h"

namespace cv_ext
{
class ImagePyramid
{
public:
  
  ImagePyramid();
  ~ImagePyramid() {};

  void buildFromImage( const cv::Mat &img, int pyr_levels, int pyr_img_type, bool deep_copy = true,
                       double pyr_scale_factor = 2.0, bool gaussian_pyr = true, 
                       InterpolationType interp_type = cv_ext::INTERP_NEAREST_NEIGHBOR );
  
  int numLevels() { return pyr_num_levels_; };
  double scaleFactor() { return pyr_scale_factor_; };

  cv::Mat& operator[]( int scale_index );  
  const cv::Mat& at( int scale_index );
  const cv::Mat& getImageAt( int scale_index );

  double getScale( int scale_index ) const;
  
private:
  
  ImagePyramid ( const ImagePyramid& other );
  ImagePyramid& operator= ( const ImagePyramid& other );
  
  void computeScaleLevel( int scale_index );
  
  int pyr_num_levels_;
  double pyr_scale_factor_;
  
  std::vector<cv::Mat> pyr_imgs_;
  std::vector<double> scales_; 
  bool gaussian_pyr_;
  InterpolationType interp_type_;
};

}
