#pragma once

#include <omp.h>
#include <stdexcept>
#include <boost/concept_check.hpp>
#include <boost/shared_ptr.hpp>
#include <Eigen/Geometry>

#include "cv_ext/conversions.h"
#include "cv_ext/pinhole_camera_model.h"

namespace cv_ext
{

/** @brief Shared pointer typedef */
typedef boost::shared_ptr< class PinholeSceneProjector > PinholeSceneProjectorPtr;

/**
 * @brief Utility class used for perspective projection (using the pinhole model)
 *        It can deals with occlusions. 
 */
class PinholeSceneProjector
{
public:
  
  /**
   * @brief Constructor: just stores the parameters
   * 
   * @param[in] cam_model The camera model object
   */
  PinholeSceneProjector( const PinholeCameraModel &cam_model ) :
    cam_model_(cam_model),
    parallelism_enabled_(false) 
  {
    r_quat_[0] = 1.0; r_quat_[1] = r_quat_[2] = r_quat_[3] = 1.0;
    t_vec_[0] = t_vec_[1] = t_vec_[2];
  };

  /**
   * @brief Provide the camera model object used for projection
   */
  PinholeCameraModel cameraModel() const { return cam_model_; };
  
  /**
   * @brief Enable/disable parallelism for the projected algorithm
   * 
   * @param[in] enable If true, projection algorithm is run in a parallel region,
   *                   otherwise it is run as a single thread
   */
  void enableParallelism( bool enable ){ parallelism_enabled_ = enable; };
  
  /**
   * @brief Return true is the projection algorithm is run in a parallel region
   */
  bool isParallelismEnabled() const { return parallelism_enabled_; };

  /**
   * @brief Set current rigid body transformation (quaternion rotation + translation)
   * 
   * @param[in] r_quat Quaternion that represents the rotation. It is not assumed that r_quat has unit norm, but
    *                  it is assumed that the norm is non-zero.
   * @param[in] t_vec Translation vector
   */
  void setTransformation(const double r_quat[4], const double t_vec[3] )
  {
    r_quat_[0] = r_quat[0]; r_quat_[1] = r_quat[1]; r_quat_[2] = r_quat[2]; r_quat_[3] = r_quat[3];
    t_vec_[0] = t_vec[0]; t_vec_[1] = t_vec[1]; t_vec_[2] = t_vec[2];
  };
  
  /**
   * @brief Set current rigid body transformation (quaternion rotation + translation)
   * 
   * @param[in] r_quat Quaternion that represents the rotation. It is not assumed that r_quat has unit norm, but
    *                  it is assumed that the norm is non-zero.
   * @param[in] t_vec Translation vector
   */
  void setTransformation(const Eigen::Quaterniond &r_quat, const Eigen::Vector3d t_vec )
  {
    cv_ext::eigenQuat2Quat(r_quat, r_quat_);
    t_vec_[0] = t_vec(0); t_vec_[1] = t_vec(1); t_vec_[2] = t_vec(2);
  };
  
  /**
   * @brief Set current rigid body transformation (angle-axis or quaternion rotation + translation)
   * 
   * @param[in] r_vec 3 by 1 rotation vector, in exponential notation (angle-axis),
   *                  or 4 x 1 quaternion that represents the rotation. 
   *                  It is not assumed that r_quat has unit norm, but
    *                 it is assumed that the norm is non-zero.
   * @param[in] t_vec 3 by 1 translation vector
   * 
   */
  void setTransformation(const cv::Mat &r_vec, const cv::Mat &t_vec)
  {
    if( r_vec.channels() != 1 || ( r_vec.rows != 3 && r_vec.rows != 4 )|| r_vec.cols != 1 || 
        t_vec.channels() != 1 || t_vec.rows != 3 || t_vec.cols != 1 )
      throw std::invalid_argument("SceneProjector::setTransformation() : \
                                   r_vec must have size 3 X 1 or 4 x 1, t_vec must have size 3 X 1");

    if( t_vec.rows == 3 )
    {
      cv::Mat_<double> angle_axis = cv::Mat_<double>(r_vec);
      ceres::AngleAxisToQuaternion((const double*)angle_axis.data, r_quat_);
      cv::Mat translation(3, 1, cv::DataType<double>::type, t_vec_);
      t_vec.convertTo(translation, translation.type());
    }
    else if ( t_vec.rows == 4 )
    {
      cv::Mat rotation_quat(4, 1, cv::DataType<double>::type, r_quat_);
      r_vec.convertTo(rotation_quat, rotation_quat.type());
      cv::Mat translation(3, 1, cv::DataType<double>::type, t_vec_);
      t_vec.convertTo(translation, translation.type());
    }
  };

 
  /**
   * @brief Projects a vector of points given the 
   *        transformation previously provided with setTransformation().
   *        The template types _TPoint3D and _TPoint2D represent the 3D scene points and 
   *        2D projected points types (e.g., cv::Point3d ). They should have x and y (and 
   *        also z for _TPoint3D) fields, and basic constructors like _TPoint3D(T _x, T _y, T _z)
   * 
   * @param[in] scene_pts Input 3D scene points
   * @param[out] img_pts Output 2D image points
   * 
   * \note If a point is projected outside the image, or it is not possible to 
   * project the point, its coordinates are set to (-1, -1)
   */
  template < typename _TPoint3D, typename _TPoint2D > 
    void projectPoints( const std::vector< _TPoint3D > &scene_pts, 
                        std::vector< _TPoint2D > &img_pts ) const ;

  /**
   * @brief Projects a vector of 3D segments (represented by theirs start and end points)
   *        given the transformation previously provided with setTransformation().
   * 
   * @param[in] scene_segs Input 3D scene segments
   * @param[out] img_segs Output 2D image segments
   * 
   * The template types _TSeg3D and _TSeg2D represent the 3D and 2D segments,
   * (e.g., cv::Vec6f ), with 6 and 4 elements, respectively,
   * accessible with the [x] operator, e.g.:
   * 
   * \code
   * _TSeg3D seg;
   * 
   * seg[0] = x0; seg[1] = y0; seg[2] = z0;
   * seg[3] = x1; seg[4] = y2; seg[5] = z3;
   * \endcode
   * 
   * \note If a segment is projected outside the image, or it is not possible to 
   * project the point, all its coordinates are set to -1
   */
  template < typename _TSeg3D, typename _TSeg2D > 
    void projectSegments( const std::vector< _TSeg3D > &scene_segs, 
                          std::vector< _TSeg2D > &img_segs ) const ;
                        
  /**
   * @brief Projects a vector of points given the 
   *        transformation previously provided with setTransformation().
   *        This function deals with occlusion, providing in output an occlusion mask.
   *        The template types _TPoint3D and _TPoint2D represent the 3D scene points and 
   *        2D projected points types (e.g., cv::Point3d ). They should have x and y (and 
   *        also z for _TPoint3D) fields, and basic constructors like _TPoint3D(T _x, T _y, T _z)
   * 
   * @param[in] scene_pts Input 3D scene points
   * @param[out] img_pts Output 2D image points
   * @param[out] occ_mask Output (binary) image mask with highlighted the current positions of 
   *                      the occlusions
   * 
   * \note If a point is projected outside the image, or it is not possible to 
   * project the point or it is occluded, its coordinates are set to (-1, -1)
   */
  template < typename _TPoint3D, typename _TPoint2D > 
    void projectPoints( const std::vector< _TPoint3D > &scene_pts, 
                        std::vector< _TPoint2D > &img_pts, 
                        cv::Mat &occ_mask ) const ;

  
  /**
   * @brief Computes the ideal point coordinates in the the normalized (canonical) camera from 
   *        a vector of points observed from the real camera.
   *        The template type _TPoint2D represents the image coordinates type 
   *        (e.g., cv::Point2d ). It should have x and y fields, and a basic 
   *        constructor like _TPoint2D(T _x, T _y).
   * 
   * @param[in] src_pt Input 2D real image points
   * @param[out] dest_pt Output 2D ideal points
   */
  template < typename _TPoint2D > 
    void normalizePoints( const std::vector< _TPoint2D > &dist_pts, 
                          std::vector< _TPoint2D > &norm_pts ) const;
   

  /**
   * @brief Unprojects a vector of image points along with their depths
   *        into a vector of 3D scene points, assuming the point is the camera reference frame.
   *        The template types _TPoint3D and _TPoint2D represent the 3D scene points and 
   *        2D projected points types (e.g., cv::Point3d ). They should have x and y (and 
   *        also z for _TPoint3D) fields, and basic constructors like _TPoint3D(T _x, T _y, T _z)
   *        Typename _T represents the depth type (e.g., float, double, ...)
   * 
   * @param[in] img_pt Input 2D image points
   * @param[in] depth Input z coordinates (depths) of the points in the camera reference frame
   * @param[out] scene_pt Output 3D scene points
   */
  template < typename _TPoint2D, typename _TPoint3D, typename _T > 
    void unprojectPoints( const std::vector< _TPoint2D > &img_pts,
                          const std::vector< _T > &depths,
                          std::vector< _TPoint3D > &scene_pts ) const;
                        
private:
  
  PinholeCameraModel cam_model_;
  double r_quat_[4]; 
  double t_vec_[3];
  bool parallelism_enabled_;
};
  
/* Implementation */

template < typename _TPoint3D, typename _TPoint2D > void PinholeSceneProjector:: 
  projectPoints( const std::vector< _TPoint3D > &scene_pts, 
                 std::vector< _TPoint2D > &img_pts ) const 
{   
  int pts_size = scene_pts.size();
  img_pts.clear();
  img_pts.reserve(pts_size);
  
  if( cam_model_.hasDistCoeff() )
  {    
    #pragma omp parallel for if( parallelism_enabled_ )
    for( int i = 0; i < pts_size; i++)
    {
      const double scene_pt[3] = { scene_pts[i].x, scene_pts[i].y, scene_pts[i].z };
      double proj_pt[2];
      cam_model_.quatRTProject( r_quat_, t_vec_, scene_pt, proj_pt );
      img_pts.push_back(_TPoint2D( proj_pt[0], proj_pt[1]));
    }
  }
  else
  {
    #pragma omp parallel for if( parallelism_enabled_ )
    for( int i = 0; i < pts_size; i++)
    {
      const double scene_pt[3] = { scene_pts[i].x, scene_pts[i].y, scene_pts[i].z };
      double proj_pt[2];
      cam_model_.quatRTProjectWithoutDistortion(r_quat_, t_vec_, scene_pt, proj_pt );
      img_pts.push_back(_TPoint2D( proj_pt[0], proj_pt[1]) );
    }
  }
}

template < typename _TPoint3D, typename _TPoint2D > void PinholeSceneProjector::
    projectPoints( const std::vector< _TPoint3D > &scene_pts, std::vector< _TPoint2D > &img_pts, 
                   cv::Mat &occ_mask ) const 
{
  int pts_size = scene_pts.size();
  img_pts.clear();
  img_pts.reserve(pts_size);
  
  std::vector<double> scene_pts_z;
  scene_pts_z.reserve(pts_size);
    
  if( cam_model_.hasDistCoeff() )
  {    
    #pragma omp parallel for if( parallelism_enabled_ )
    for( int i = 0; i < pts_size; i++)
    {
      const double scene_pt[3] = { scene_pts[i].x, scene_pts[i].y, scene_pts[i].z };
      double proj_pt[2], depth;
      cam_model_.quatRTProject( r_quat_, t_vec_, scene_pt, proj_pt, depth );
      img_pts.push_back( _TPoint2D( proj_pt[0], proj_pt[1]) );
      scene_pts_z.push_back( depth );
    }
  }
  else
  {
    #pragma omp parallel for if( parallelism_enabled_ )
    for( int i = 0; i < pts_size; i++)
    {
      const double scene_pt[3] = { scene_pts[i].x, scene_pts[i].y, scene_pts[i].z };
      double proj_pt[2], depth;
      cam_model_.quatRTProjectWithoutDistortion(r_quat_, t_vec_, scene_pt, proj_pt, depth );
      img_pts.push_back( _TPoint2D( proj_pt[0], proj_pt[1]) );
      scene_pts_z.push_back( depth );
    }
  }
  

  cv::Mat index_mask = cv::Mat_<int>(cam_model_.imgHeight(), cam_model_.imgWidth(), -1);
  occ_mask = cv::Mat_<uchar>(cam_model_.imgHeight(), cam_model_.imgWidth(), uchar(0));
  for( int i = 0; i < pts_size; i++)
  {
    if( img_pts[i].x >= 0 )
    {
      int x = round(img_pts[i].x), y = round(img_pts[i].y);
      int selected_idx = index_mask.at<int>(y,x);
      if( selected_idx < 0 )
        index_mask.at<int>(y,x) = i;
      else
      {
        // Occlusion
        if( scene_pts_z[i] < scene_pts_z[selected_idx] )
        {
          img_pts[selected_idx] = _TPoint2D( -1, -1 );
          index_mask.at<int>(y,x) = i;
        }
        else
          img_pts[i] = _TPoint2D( -1, -1 );       
        occ_mask.at<uchar>(y,x) = 255;
      }
    }
  }
}

template < typename _TPoint2D > void PinholeSceneProjector:: 
  normalizePoints( const std::vector< _TPoint2D > &dist_pts, std::vector< _TPoint2D > &norm_pts ) const
{   
  int pts_size = dist_pts.size();
  norm_pts.resize(pts_size);
    
  if( cam_model_.hasDistCoeff() )
  {    
    #pragma omp parallel for if( parallelism_enabled_ )
    for( int i = 0; i < pts_size; i++)
    {
      if( dist_pts[i].x >= 0 )
      {
        const double dist_pt[2] = { dist_pts[i].x, dist_pts[i].y };
        double norm_pt[2];
        cam_model_.normalize( dist_pt, norm_pt );
        norm_pts[i] = _TPoint2D( norm_pt[0], norm_pt[1]);
      }
      else
        norm_pts[i] = _TPoint2D( -1, -1 );
    }
  }
  else
  {
    #pragma omp parallel for if( parallelism_enabled_ )
    for( int i = 0; i < pts_size; i++)
    {
      if( dist_pts[i].x >= 0 )
      {
        const double dist_pt[2] = { dist_pts[i].x, dist_pts[i].y };
        double norm_pt[2];
        cam_model_.normalizeWithoutDistortion( dist_pt, norm_pt );
        norm_pts[i] = _TPoint2D( norm_pt[0], norm_pt[1]);
      }
      else
        norm_pts[i] = _TPoint2D( -1, -1 );
    }
  }
}

template < typename _TSeg3D, typename _TSeg2D > 
    void PinholeSceneProjector::projectSegments( const std::vector< _TSeg3D > &scene_segs, 
                                                 std::vector< _TSeg2D > &img_segs ) const
{
  int segs_size = scene_segs.size();
  img_segs.clear();
  img_segs.reserve(segs_size);
  
  if( cam_model_.hasDistCoeff() )
  {    
    #pragma omp parallel for if( parallelism_enabled_ )
    for( int i = 0; i < segs_size; i++)
    {
      const _TSeg3D &scene_seg = scene_segs[i];
      const double scene_pt0[3] = { scene_seg[0], scene_seg[1], scene_seg[2] }, 
                   scene_pt1[3] = { scene_seg[3], scene_seg[4], scene_seg[5] };
      double proj_pt0[2], proj_pt1[2];
      cam_model_.quatRTProject( r_quat_, t_vec_, scene_pt0, proj_pt0 );
      cam_model_.quatRTProject( r_quat_, t_vec_, scene_pt1, proj_pt1 );
      if( proj_pt0[0] == -1 ||  proj_pt1[0] == -1 )
        img_segs.push_back( _TSeg2D( -1, -1, -1, -1 ));
      else
        img_segs.push_back( _TSeg2D( proj_pt0[0], proj_pt0[1], proj_pt1[0], proj_pt1[1] ));
    }
  }
  else
  {
    #pragma omp parallel for if( parallelism_enabled_ )
    for( int i = 0; i < segs_size; i++)
    {
      const _TSeg3D &scene_seg = scene_segs[i];
      const double scene_pt0[3] = { scene_seg[0], scene_seg[1], scene_seg[2] }, 
                   scene_pt1[3] = { scene_seg[3], scene_seg[4], scene_seg[5] };
      double proj_pt0[2], proj_pt1[2];
      cam_model_.quatRTProjectWithoutDistortion( r_quat_, t_vec_, scene_pt0, proj_pt0 );
      cam_model_.quatRTProjectWithoutDistortion( r_quat_, t_vec_, scene_pt1, proj_pt1 );
      img_segs.push_back( _TSeg2D( proj_pt0[0], proj_pt0[1], proj_pt1[0], proj_pt1[1] ));
    }
  }
}

template < typename _TPoint2D, typename _TPoint3D, typename _T > void PinholeSceneProjector::
  unprojectPoints( const std::vector< _TPoint2D > &img_pts,
                   const std::vector< _T > &depths,
                   std::vector< _TPoint3D > &scene_pts ) const
{
  int pts_size = img_pts.size();
  scene_pts.resize(pts_size);
    
  if( cam_model_.hasDistCoeff() )
  {    
    #pragma omp parallel for if( parallelism_enabled_ )
    for( int i = 0; i < pts_size; i++)
    {
      const double img_pt[2] = { img_pts[i].x, img_pts[i].y };
      double scene_pt[3];
      cam_model_.unproject( img_pt, double(depths[i]), scene_pt );
      scene_pts[i] = _TPoint3D( scene_pt[0], scene_pt[1], scene_pt[2]);

    }
  }
  else
  {
    #pragma omp parallel for if( parallelism_enabled_ )
    for( int i = 0; i < pts_size; i++)
    {
      const double img_pt[2] = { img_pts[i].x, img_pts[i].y };
      double scene_pt[3];
      cam_model_.unprojectWithoutDistortion( img_pt, double(depths[i]), scene_pt );
      scene_pts[i] = _TPoint3D( scene_pt[0], scene_pt[1], scene_pt[2]);
    }
  }  
}

};