#pragma once

#include <stdint.h>

#include <boost/preprocessor/cat.hpp>
#include <boost/preprocessor/seq/for_each.hpp>

#include <pcl/point_types.h>
#include <opencv2/opencv.hpp>

#define CV_EXT_REAL_TYPES \
  (double)                \
  (float)

#define CV_EXT_UINT_PIXEL_DEPTH_TYPES \
  (uint8_t)                           \
  (uint16_t)                          \
  (uint32_t)

#define CV_EXT_PIXEL_DEPTH_TYPES \
  CV_EXT_UINT_PIXEL_DEPTH_TYPES  \
  CV_EXT_REAL_TYPES
  
#define CV_EXT_2D_REAL_POINT_TYPES \
  (cv::Point2f)                    \
  (cv::Point2d)

#define CV_EXT_3D_REAL_POINT_TYPES \
(cv::Point3f)                    \
(cv::Point3d)                    \
PCL_XYZ_POINT_TYPES

#define CV_EXT_2D_INT_POINT_TYPES \
  (cv::Point2i)

#define CV_EXT_3D_INT_POINT_TYPES \
(cv::Point3i)

#define CV_EXT_2D_POINT_TYPES \
  CV_EXT_2D_REAL_POINT_TYPES  \
  CV_EXT_2D_INT_POINT_TYPES
  
#define CV_EXT_3D_POINT_TYPES \
  CV_EXT_3D_REAL_POINT_TYPES  \
  CV_EXT_3D_INT_POINT_TYPES

#define CV_EXT_4D_VECTOR_TYPES \
  (cv::Vec4i)                  \
  (cv::Vec4f)                  \
  (cv::Vec4d)                  \
  (Eigen::Vector4i)            \
  (Eigen::Vector4f)            \
  (Eigen::Vector4d)


#define CV_EXT_INSTANTIATE_MACRO(r, NAME, TYPE) \
  BOOST_PP_CAT(CV_EXT_INSTANTIATE_, NAME)(TYPE)

/** @brief Provide explicit instantiations of some templates for the types given in the 
 *         sequence TYPES_SEQ, to be called at the end of the function/class definitions 
 *         (e.g., at the end of the .cpp file)
 * 
 * When writing:
 * 
 * CV_EXT_INSTANTIATE( ClassName, CV_EXT_FLOATING_POINT_TYPES )
 * 
 * the macro will expand:
 * 
 *  CV_EXT_INSTANTIATE_ClassName( float )
 *  CV_EXT_INSTANTIATE_ClassName( double )
 * 
 * so CV_EXT_INSTANTIATE_ClassName should be defined somewhere, before CV_EXT_INSTANTIATE(...), 
 * e.g. :
 * 
 * CV_EXT_INSTANTIATE_ClassName( TYPE ) \
 *   template class ClassName< TYPE >;
 * 
 *  (the semicolon at the end of the macro is mandatory, due to BOOST_PP_SEQ_FOR_EACH(...) )
 */
#define CV_EXT_INSTANTIATE(NAME, TYPES_SEQ )                        \
  BOOST_PP_SEQ_FOR_EACH(CV_EXT_INSTANTIATE_MACRO, NAME, TYPES_SEQ )


namespace cv_ext
{
  
enum CoordinateAxis { COORDINATE_X_AXIS, COORDINATE_Y_AXIS, COORDINATE_Z_AXIS };
enum InterpolationType { INTERP_NEAREST_NEIGHBOR, INTERP_BILINEAR, INTERP_BICUBIC };
enum WeightFunctionType { TRIANGLE_WEIGHT, GAUSSIAN_WEIGHT };

}
