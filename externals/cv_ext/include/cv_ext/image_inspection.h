#pragma once

#include <opencv2/opencv.hpp>
#include <vector>
#include <cstdlib>
#include <algorithm>
#include <boost/concept_check.hpp>
#include <cv_ext/interpolations.h>

// TODO Complete doxygen docs, remove IntegralImage2 ??
namespace cv_ext
{
/**
 * @brief Provides a vector of the points that belong to a line segment 
 *        connecting two points. The line is computed using the
 *        bresenham algorithm, with tickness = 1
 * 
 * @param[in] p0 Line segment start point
 * @param[in] p1 Line segment end point
 * @param[out] line_pts Output line points
 */  
void getLinePoints( const cv::Point &p0, const cv::Point &p1, std::vector<cv::Point> &line_pts );

/** 
 * @brief Calculates the integral of a single channel image, along the direction specified in input.
 *        Typenames _Tin and _Tout represent the input and output image pixel depths, respectively 
 *        (e.g., _Tin can be uchar, float, double, ..., see CV_EXT_PIXEL_DEPTH_TYPES in cv_ext/types.h, 
 *               _Tout can be uint32_t, uint64_t, float or double )
 * 
 * @param[in] src_img Input, source image
 * @param[out] int_img Output, integral image
 * @param[in] direction Direction used to compute the inegral image (it should be [-M_PI/2, M_PI/2), but
 *                      a normalization is performed internally)
 * 
 * \note The integral image is computed following a line computed using the
 * bresenham algorithm, see getLinePoints() function
 */
template < typename _Tin, typename _Tout> 
  void computeIntagralImage( const cv::Mat& src_img, cv::Mat& int_img, double direction = 0 );

  
template <typename _Tin > class IntegralImage
{
public:
  
  IntegralImage( const cv::Mat& src_img, double direction = 0 );
  ~IntegralImage(){};
  
  const cv::Mat &img(){ return int_img_; };
  double direction() { return direction_; };
  
  template < typename _TVec4 > inline double getLineSum( const _TVec4 &line_segment ) const;
  template < typename _TPoint2D > inline double getLineSum( const _TPoint2D &p0, 
                                                            const _TPoint2D &p1 ) const;
  template < typename _T > inline double getLineSum( const _T &p0_x, const _T &p0_y,
                                                     const _T &p1_x, const _T &p1_y ) const;
  
  template < typename _TVec4 > inline double getLineSum( const _TVec4 &line_segment, 
                                                         int &len  ) const;
  template < typename _TPoint2D > inline double getLineSum( const _TPoint2D &p0, 
                                                            const _TPoint2D &p1,
                                                            int &len ) const;
  template < typename _T > inline double getLineSum( const _T &p0_x, const _T &p0_y,
                                                     const _T &p1_x, const _T &p1_y,
                                                     int &len ) const;
private:
  cv::Mat int_img_;
  int w_, h_;
  double direction_;
  bool x_major_;
  std::vector<int> lut_;
};

// Some implementations
template < typename _Tin > template < typename _TVec4 > inline 
  double IntegralImage<_Tin>::getLineSum( const _TVec4 &line_segment ) const
{
  int len;
  return getLineSum( line_segment[0], line_segment[1], line_segment[2], line_segment[3], len );
}

template < typename _Tin > template < typename _TPoint2D > inline 
  double IntegralImage<_Tin>::getLineSum( const _TPoint2D &p0, const _TPoint2D &p1 ) const
{
  int len;
  return getLineSum( p0.x, p0.y, p1.x, p1.y, len );
}

template < typename _Tin > template < typename _T > inline 
  double IntegralImage<_Tin>::getLineSum( const _T &p0_x, const _T &p0_y,
                                          const _T &p1_x, const _T &p1_y ) const
{
  int len;
  return getLineSum( p0_x, p0_y, p1_x, p1_y, len );
}

template < typename _Tin > template < typename _TVec4 > inline 
  double IntegralImage<_Tin>::getLineSum( const _TVec4 &line_segment, int &len ) const
{
  return getLineSum( line_segment[0], line_segment[1], line_segment[2], line_segment[3], len );
}

template < typename _Tin > template < typename _TPoint2D > inline 
  double IntegralImage<_Tin>::getLineSum( const _TPoint2D &p0, const _TPoint2D &p1, int &len ) const
{
  return getLineSum( p0.x, p0.y, p1.x, p1.y, len );
}

template < typename _Tin > template < typename _T > inline 
  double IntegralImage<_Tin>::getLineSum( const _T &p0_x, const _T &p0_y,
                                          const _T &p1_x, const _T &p1_y,
                                          int &len ) const
{
  _T len_x = p1_x > p0_x ? p1_x - p0_x : p0_x - p1_x,
     len_y = p1_y > p0_y ? p1_y - p0_y : p0_y - p1_y;;
  
  len = round( len_x>len_y ? len_x : len_y ) + 1;
  int h_len = len>>1, odd;
  if(len%2) odd = 1; else odd = 0;

  cv::Point c = cv::Point((p0_x + p1_x)/2, (p0_y + p1_y)/2);
  
  if( x_major_ )
  {
    cv::Point p0, p1;
    p0.x = c.x - h_len  - odd;  p1.x = c.x + h_len;

    if( p0.x >= 0 && p1.x >= 0 && p0.x < w_ && p1.x < w_ )
    {
      int dy0 = lut_[p0.x] - lut_[c.x] , dy1 = lut_[p1.x] - lut_[c.x];

      p0.y = c.y + dy0;
      p1.y = c.y + dy1;

      if( p0.y >= 0 && p1.y >= 0 && p0.y < h_ && p1.y < h_ )
      {
        return int_img_.at<double> ( p1.y, p1.x ) - int_img_.at<double> ( p0.y, p0.x);         
      }
    }
  }
  else
  {
    cv::Point p0, p1;
    if( direction_ > 0 )
    {
      p0.y = c.y - h_len  - odd;  p1.y = c.y + h_len;
    }
    else
    {
      p0.y = c.y + h_len  + odd;  p1.y = c.y - h_len;
    }

    if( p0.y >= 0 && p1.y >= 0 && p0.y < h_ && p1.y < h_ )
    {
      int dx0 = lut_[p0.y] - lut_[c.y], dx1 = lut_[p1.y] - lut_[c.y];
     
      p0.x = c.x + dx0;
      p1.x = c.x + dx1;

      if( p0.x >= 0 && p1.x >= 0 && p0.x < w_ && p1.x < w_ )
      {
        return int_img_.at<double> ( p1.y, p1.x ) - int_img_.at<double> ( p0.y, p0.x);
      }
    }
  }
  return std::numeric_limits< double >::max();
}

template <typename _Tin> class IntegralImage2
{
public:
  
  IntegralImage2( const cv::Mat& src_img, double direction = 0 );
  
  const cv::Mat &img(){ return int_img_; };
  double direction() { return direction_; };
  
  template < typename _TVec4 > inline double getLineSum( const _TVec4 &line_segment ) const
  {
    int len;
    return getLineSum( line_segment[0], line_segment[1], line_segment[2], line_segment[3], len );
  };
  template < typename _TPoint2D > inline double getLineSum( const _TPoint2D &p0, 
                                                            const _TPoint2D &p1 ) const
  {
    int len;
    return getLineSum( p0.x, p0.y, p1.x, p1.y, len );
  };

  template < typename _T > inline double getLineSum( const _T &p0_x, const _T &p0_y,
                                                     const _T &p1_x, const _T &p1_y ) const
  {
    int len;
    return getLineSum( p0_x, p0_y, p1_x, p1_y, len );    
  }
  template < typename _TVec4 > inline double getLineSum( const _TVec4 &line_segment, int len ) const
  {
    return getLineSum( line_segment[0], line_segment[1], line_segment[2], line_segment[3], len );
  };
  template < typename _TPoint2D > inline double getLineSum( const _TPoint2D &p0, 
                                                            const _TPoint2D &p1, int len ) const
  {
    return getLineSum( p0.x, p0.y, p1.x, p1.y, len );
  };
  
  template < typename _T > inline double getLineSum( const _T &p0_x, const _T &p0_y,
                                                     const _T &p1_x, const _T &p1_y, 
                                                     int len ) const
  {
    _T len_x = p1_x > p0_x ? p1_x - p0_x : p0_x - p1_x,
       len_y = p1_y > p0_y ? p1_y - p0_y : p0_y - p1_y;
    
    len = round( len_x>len_y ? len_x : len_y ) + 1;
    int h_len = len/2, odd;
    
    cv::Point_<_T> c = cv::Point_<_T>((p0_x + p1_x)/2, (p0_y + p1_y)/2);
    if(len%2) odd = 1; else odd = 0;
    
    cv::Point_<_T> int_c( round( c.x*cos_d_ - c.y*sin_d_), 
                          round( c.x*sin_d_ + c.y*cos_d_ ) );

    int_c -= offset_;
    
    cv::Point_<_T> p0, p1;
    p0.x = int_c.x - _T(h_len) - _T(odd);  p1.x = int_c.x + _T(h_len);
    p0.y = p1.y = int_c.y;
    
    if( p0.x >= 0 && p1.x >= 0 && p0.x < w_ && p1.x < w_ && p0.y >= 0 && p0.y < h_ )
    {
      double val0 = int_img_.at<double> ( p0.y, p0.x),
             val1 = int_img_.at<double> ( p1.y, p1.x );
             
      if( val0 != -1 && val1 != -1 )
        return val1 - val0;
    }
    return std::numeric_limits< double >::max();
  };
  
private:
  
  void computeIntagral( const cv::Mat& src_img );
  
  cv::Mat int_img_;
  double direction_;
  int w_, h_;
  cv::Point offset_;
  double cos_d_, sin_d_;
};

}
