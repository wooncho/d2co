#pragma once

#include <vector>
#include <Eigen/Geometry>
#include "cv_ext/types.h"

namespace cv_ext
{
/**
  * @brief Align the selected aligned_axis with the input vector vec and provide a set of 
  *        num_rotations quaternions that represents rotations around this axis.
  * 
  * @param[in] vec Input 3D point, i.e. the (direction) vector
  * @param[out] quat_rotations The set of quaternions representig the sampled rotations
  * @param[in] num_rotations Number of rotations around the input vector
  * @param[in] aligned_axis The coordindate axis to be aligned with the input vector
  * 
  * This function first compute the rotation to align the selected axis (aligned_axis) with the 
  * input vector vec, then sample around the selected axis a number of num_rotations rotations.
  */
template < typename _TPoint3D > 
  void sampleRotationsAroundVector( _TPoint3D &vec, std::vector< Eigen::Quaterniond > &quat_rotations, 
                                    int num_rotations, CoordinateAxis aligned_axis = COORDINATE_Z_AXIS );
  

/**
  * @brief Align the selected aligned_axis with the input vectors vecs and provide a set of 
  *        num_rotations quaternions that represents rotations around each aligned axis.
  * 
  * @param[in] vecs Input 3D points, i.e. the (direction) vectors
  * @param[out] quat_rotations The set of quaternions representig the sampled rotations
  * @param[in] num_rotations Number of rotations around each input vector
  * @param[in] aligned_axis The coordindate axis to be aligned with the input vectors
  * 
  * This function first compute the rotation to align the selected axis (aligned_axis) with each of the 
  * input vectors vecs, then sample around the selected axis a number of num_rotations rotations.
  */
template < typename _TPoint3D > 
  void sampleRotationsAroundVectors( std::vector< _TPoint3D> &vecs, std::vector< Eigen::Quaterniond > &quat_rotations, 
                                     int num_rotations, CoordinateAxis aligned_axis = COORDINATE_Z_AXIS );
  
/**
  * @brief Construct a unit spherical point cloud with points regularly distributed in the 
  *        whole surface. The template type _TPoint3D represents a general 3D point
  *        (compliant types defined in CV_EXT_3D_POINT_TYPES, see cv_ext/types.h)
  * 
  * @param[out] points Output 3D cloud
  * @param[in] num_refinements Number of interations used for trangles splitting
  * @param[in] radius Sphere radius
  * 
  * The sphere ("icosphere") is constructed starting from a an icosahedron 
  * (i.e., a polyhedron with 20 triangular faces, 30 edges and 12 vertices, see:
  *  http://en.wikipedia.org/wiki/Icosahedron ) recursively splitting each triangle 
  * into 4 smaller triangles.
  */
template < typename _TPoint3D > 
  void createIcosphere( std::vector<_TPoint3D> &sphere_points, int num_refinements, double radius = 1.0 );
  
/**
  * @brief Construct the unit spherical polar caps point cloud with 
  *        points regularly distributed in the whole surface. 
  *        The template type _TPoint3D represents a general 3D point
  *        (compliant types defined in CV_EXT_3D_POINT_TYPES, see cv_ext/types.h)
  * 
  * @param[out] points Output 3D cloud
  * @param[in] num_refinements Number of interations used for trangles splitting
  * @param[in] latitude_angle Cutting absolute latitude angle, in radians.
  * @param[in] radius Sphere radius
  * @param[in] only_north_cap If true, create only the upper pole of the sphere
  * 
  * \note If latitude_angle is 0, createIcospherePolarCap() provide a complete icosphere. If 
  * latitude_angle is pi/2, createIcospherePolarCap() provide just two points in the poles (or only
  * one if only_north_cap is set to true)
  * 
  * See createIcosphere() for further details about the icosphere
  */
template < typename _TPoint3D > 
  void createIcospherePolarCap( std::vector<_TPoint3D> &cap_points, int num_refinements, 
                                double latitude_angle, double radius = 1.0, bool only_north_cap = false );
}