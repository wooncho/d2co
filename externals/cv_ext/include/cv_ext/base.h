#pragma once

#include <opencv2/opencv.hpp>

namespace cv_ext
{
/**
 * @brief Normalize a general input angle between -M_PI and M_PI
 *
 * @param ang The input angle
 * @return The normalized angle, [ -M_PI, M_PI )
 */
template < typename _T > inline _T normalizeAngle ( _T ang )
{
  if ( ang >= _T ( -M_PI ) && ang < _T ( M_PI ) )
    return ang;

  _T multiplier = floor ( ang / ( _T ( 2.0*M_PI ) ) );
  ang = ang - _T ( 2*M_PI ) *multiplier;
  if ( ang >= _T ( M_PI ) )
    ang = ang - _T ( 2.0*M_PI );

  if ( ang < _T ( -M_PI ) )
    ang = ang + _T ( 2.0*M_PI );

  return ang;
}

/**
 * @brief Provide the L2-norm of a 2D point
 *
 * @param pt The input point
 * @return The L2-norm
 * 
 * The template type _TPoint2D represents a 2D point (e.g., cv::Point2d ). 
 * It should have x and y fields, and basic constructors like 
 * _TPoint2D(T _x, T _y) where Typename _T represents the 
 * depth type (e.g., float, double, ...)
 */
template<typename _TPoint2D> static inline
double norm2D(const _TPoint2D& pt)
{
  return std::sqrt( double(pt.x)*pt.x + double(pt.y)*pt.y );
}

/**
 * @brief Provide the L2-norm of a 3D point
 *
 * @param pt The input point
 * @return The L2-norm
 * 
 * The template type _TPoint3D represents a 3D point (e.g., cv::Point3d ). 
 * It should have x, y and z fields, and basic constructors like 
 * _TPoint3D(T _x, T _y, T _z) where Typename _T represents the 
 * depth type (e.g., float, double, ...)
 */
template<typename _TPoint3D> static inline
double norm3D(const _TPoint3D& pt)
{
  return std::sqrt( double(pt.x)*pt.x + double(pt.y)*pt.y + double(pt.z)*pt.z );
}

/**
 * @brief Normalize a input 2D point to be in the unit sphere
 *
 * @param[in] src_pt The input point
 * 
 * @return The normalized point
 */
template<typename _TPoint2D> static inline
  _TPoint2D normalize2DPoint(const _TPoint2D& src_pt )
{
  _TPoint2D dst_pt;
  double eta = 1.0/cv_ext::norm2D(src_pt);
  dst_pt.x = eta*src_pt.x;
  dst_pt.y = eta*src_pt.y;
  return dst_pt;
}

/**
 * @brief Normalize a input 3D point to be in the unit sphere
 *
 * @param[in] src_pt The input point
 * 
 * @return The normalized point
 */
template<typename _TPoint3D> static inline
  _TPoint3D normalize3DPoint(const _TPoint3D& src_pt )
{
  _TPoint3D dst_pt;
  double eta = 1.0/cv_ext::norm3D(src_pt);
  dst_pt.x = eta*src_pt.x;
  dst_pt.y = eta*src_pt.y;
  dst_pt.z = eta*src_pt.z;
  return dst_pt;
}

}
