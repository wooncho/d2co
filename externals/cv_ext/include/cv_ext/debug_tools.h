#pragma once

#include <opencv2/opencv.hpp>
#include <Eigen/Geometry>

namespace cv_ext
{
/**
* @brief Simple image viewer
* 
* @param[in] img The input image
* @param[in] win_name Window names
* @param[in] normalize If true, normalize each channel of the image between 0 and 255 (i.e., min pix value -> 0, 
*                  max pixel value -> 255)
* @param[in] sleep If sleep == 0, block the application and wait for escape key press. If sleep > 0, wait for
*                  block the application for sleep milliseconds
*/
void showImage ( const cv::Mat &img, const std::string &win_name = "", bool normalize = true, int sleep = 0 );

/**
* @brief Simple point cloud viewer. The template type _TPoint3D represents a 3D point
*        (compliant types defined in CV_EXT_3D_POINT_TYPES, see cv_ext/types.h)
*
* @param[in] points Input 3D points
* @param[in] win_name Window names
* @param[in] show_axes Show x,y,z axes
* @param[in] axes_transf Axes transformation matrixes vector
* 
* \note If show_axes is true and axes_transf is empty, show a single coordinate system in the origin.
*       If show_axes is true and axes_transf is not empty, show a coordinate system for each transformation 
*/
template < typename _TPoint3D >
  void show3DPoints ( const std::vector<_TPoint3D> &points, 
                      const std::string &win_name = "", bool show_axes = false, 
                      const std::vector< Eigen::Affine3f > &axes_transf = std::vector< Eigen::Affine3f >() );
}
