/* TODO
 *
 * - Reduce the resolution
 */

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

#include <boost/concept_check.hpp>
#include <boost/thread/locks.hpp>

#include <OpenMesh/Core/IO/MeshIO.hh>
#include "raster_object_model3D.h"

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#define GLFW_INCLUDE_GLU
#include <GLFW/glfw3.h>

#include <glm/gtc/quaternion.hpp> 
#include <glm/gtx/quaternion.hpp>

using namespace glm;
using namespace std;

#define SIMPLE_OBJECT_VERTEX_SHADER_CODE(VERSION) \
"\
#version "+VERSION+"\
\n in vec3 vertex_pos; \
uniform mat4 rt_persp;\
void main()\
{\
  gl_Position =  rt_persp * vec4(vertex_pos,1);\
}\
"

#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
typedef OpenMesh::TriMesh_ArrayKernelT<>  Mesh;

static cv::Mat debug_img, color_debug_img;

void checkFramebufferStatus()
{
  GLenum status = glCheckFramebufferStatus ( GL_FRAMEBUFFER );
  switch ( status )
  {
  case GL_FRAMEBUFFER_COMPLETE:
    cout<<"GL_FRAMEBUFFER_COMPLETE"<<std::endl;
    break;
  case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
    cout<<"GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER"<<std::endl;
    break;
  case GL_FRAMEBUFFER_UNSUPPORTED:
    cout<<"GL_FRAMEBUFFER_UNSUPPORTED"<<std::endl;
    break;
  case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
    cout<<"GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT "<<std::endl;
    break;
  default:
    break;
  }
}

class RasterObjectModel3D::MeshModel
{
public:
  MeshModel() {};
  ~MeshModel() {};
  Mesh mesh;
  GLFWwindow* window;
  GLuint fbo, rbo;
  std::vector<GLfloat> vertex_buffer_data;
  GLuint vertex_buffer;
  GLuint shader_program_id;
  glm::mat4 rt_persp;
};


RasterObjectModel3D::RasterObjectModel3D() : 
  RasterObjectModel(),
  render_win_size_(cv::Size(1024, 768)),
//  render_win_size_(cv::Size(800, 600)),
  render_z_near_(0.01f),
  render_z_far_(5.0f),
  render_fov_(M_PI/3),
  depth_buffer_epsilon_(0.001f), //0.001f
  normal_epsilon_(0.4f)
{
  mesh_model_ptr_ = boost::shared_ptr< MeshModel > ( new MeshModel () );

  // Initialise GLFW
  if ( !glfwInit() )
  {
    std::cerr<<"Failed to initialize GLFW"<<std::endl;
    return;
  }

  glfwWindowHint ( GLFW_SAMPLES, 4 );
  glfwWindowHint ( GLFW_CONTEXT_VERSION_MAJOR, 2 );
  glfwWindowHint ( GLFW_CONTEXT_VERSION_MINOR, 1 );
  glfwWindowHint ( GLFW_VISIBLE, 0 );

  // Create an hidden window, default size
  mesh_model_ptr_->window = glfwCreateWindow ( render_win_size_.width, render_win_size_.height, "Render", NULL, NULL );
  if ( mesh_model_ptr_->window == NULL )
  {
    std::cerr<<"Failed to open GLFW window"<<std::endl;
    glfwTerminate();
    return;
  }

  glfwMakeContextCurrent ( mesh_model_ptr_->window );
  
  // Initialize GLEW
  if ( glewInit() != GLEW_OK )
  {
    std::cerr<<"Failed to initialize GLEW"<<std::endl;
    return;
  }
  
  depth_buffer_storing_enabled_=false;
  depth_buffer_data_.resize(render_win_size_.width*render_win_size_.height);
  
  glGenFramebuffers ( 1, &mesh_model_ptr_->fbo );
  glGenRenderbuffers ( 1, &mesh_model_ptr_->rbo );
  glBindRenderbuffer ( GL_RENDERBUFFER, mesh_model_ptr_->rbo );
  glBindFramebuffer(GL_FRAMEBUFFER, mesh_model_ptr_->fbo );
  glRenderbufferStorage ( GL_RENDERBUFFER, GL_DEPTH_COMPONENT32F, render_win_size_.width, render_win_size_.height );
  glFramebufferRenderbuffer ( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER,  mesh_model_ptr_->rbo );

  checkFramebufferStatus();

  // Enable depth test
  glEnable ( GL_DEPTH_TEST );
  // Accept fragment if it closer to the camera than the former one
  glDepthFunc ( GL_LESS );

  GLuint vertex_shader_id = glCreateShader ( GL_VERTEX_SHADER );
  std::string shading_version((char*)glGetString ( GL_SHADING_LANGUAGE_VERSION ));
  std::cout<<"Shader_version: "<<shading_version<<std::endl;
  
  std::string sh_ver="";
  for(int i=0; i<shading_version.size(); i++)
    if(std::isdigit(shading_version[i])) sh_ver.push_back(shading_version[i]);

  std::string vertex_shader_code ( SIMPLE_OBJECT_VERTEX_SHADER_CODE (sh_ver) );
  GLint result = GL_FALSE;

  // Compile Vertex Shader
  char const * vertex_source_ptr = vertex_shader_code.c_str();
  glShaderSource ( vertex_shader_id, 1, &vertex_source_ptr , NULL );
  glCompileShader ( vertex_shader_id );

//   // Check Vertex Shader
  int info_log_length;
  glGetShaderiv ( vertex_shader_id, GL_COMPILE_STATUS, &result );
  glGetShaderiv ( vertex_shader_id, GL_INFO_LOG_LENGTH, &info_log_length );
  std::vector<char> vertex_shader_error_message ( info_log_length );
  glGetShaderInfoLog ( vertex_shader_id, info_log_length, NULL, &vertex_shader_error_message[0] );
   std::cout<<"ShaderInfoLog: "<<&vertex_shader_error_message[0]<<std::endl;
   cout<<"===Vertex shader code==="<<endl<<vertex_shader_code<<endl;

  // Link the program
  mesh_model_ptr_->shader_program_id = glCreateProgram();

  glAttachShader ( mesh_model_ptr_->shader_program_id, vertex_shader_id );
  glLinkProgram ( mesh_model_ptr_->shader_program_id );
  glDeleteShader ( vertex_shader_id );
}

RasterObjectModel3D::~RasterObjectModel3D()
{
  glDeleteBuffers ( 1, & ( mesh_model_ptr_->vertex_buffer ) );
  glDeleteProgram ( mesh_model_ptr_->shader_program_id );
  glDeleteFramebuffers ( 1, &mesh_model_ptr_->fbo );
  glDeleteRenderbuffers( 1, &mesh_model_ptr_->rbo );
  glfwTerminate();
}

bool RasterObjectModel3D::setStlFile ( const string& stl_filename )
{
  mesh_model_ptr_->mesh.clear();
  if ( !OpenMesh::IO::read_mesh ( mesh_model_ptr_->mesh, stl_filename ) )
  {
    std::cerr<<"Error opening file: "<<stl_filename<<std::endl;
    return false;
  }

  return true;
}

void RasterObjectModel3D::computeRaster()
{
//   render_win_size_=cam_model_.imgSize();
//   render_win_size_.width*=cam_model_.sizeScaleFactor();
//   render_win_size_.height*=cam_model_.sizeScaleFactor();
//   float fov_x=atan2((double)cam_model_.imgWidth()*cam_model_.sizeScaleFactor()/2,cam_model_.fx()*cam_model_.sizeScaleFactor());
//   render_fov_=fov_x*2;
  
  int w, h;
  glfwGetWindowSize ( mesh_model_ptr_->window, &w, &h );
  
  if( w != render_win_size_.width || h != render_win_size_.height )
  {
    glfwSetWindowSize( mesh_model_ptr_->window, render_win_size_.width, render_win_size_.height );
    depth_buffer_data_.resize(render_win_size_.width*render_win_size_.height);
  }
    
  mesh_model_ptr_->vertex_buffer_data.clear();
  Mesh &mesh = mesh_model_ptr_->mesh;
    
  pts_.clear();
  d_pts_.clear();
  segs_.clear();
  d_segs_.clear();
   
  float multiplier = 1.0f/unit_meas_;
  
  // (linearly) iterate over all vertices
  for (Mesh::VertexIter v_it = mesh.vertices_sbegin(); v_it!=mesh.vertices_end(); ++v_it)
  {
    OpenMesh::DefaultTraits::Point &p = mesh.point ( *v_it );
    p *= multiplier;
  }
  
  if( centroid_orig_offset_ == CENTROID_ORIG_OFFSET)
  {
    OpenMesh::DefaultTraits::Point mean;
    int n_vtx = 0;
    for (Mesh::VertexIter v_it = mesh.vertices_sbegin(); v_it!=mesh.vertices_end(); ++v_it)
    {
      OpenMesh::DefaultTraits::Point &p = mesh.point ( *v_it );
      mean += p;
      n_vtx++;
    }
    mean[0] /= float(n_vtx);
    mean[1] /= float(n_vtx);
    mean[2] /= float(n_vtx);
    
    orig_offset_ = -cv::Point3f(mean[0], mean[1], mean[2]);
  }
  else if( centroid_orig_offset_ == BOUNDING_BOX_CENTER_ORIG_OFFSET )
  {
    OpenMesh::DefaultTraits::Point min, max;
    min[0] = std::numeric_limits<float>::max();
    min[1] = std::numeric_limits<float>::max();
    min[2] = std::numeric_limits<float>::max();
    
    max[0] = -std::numeric_limits<float>::max();
    max[1] = -std::numeric_limits<float>::max();
    max[2] = -std::numeric_limits<float>::max();
    
    for (Mesh::VertexIter v_it = mesh.vertices_sbegin(); v_it!=mesh.vertices_end(); ++v_it)
    {
      OpenMesh::DefaultTraits::Point &p = mesh.point ( *v_it );
      if(p[0] > max[0])  max[0] = p[0]; else if(p[0] < min[0]) min[0] = p[0];
      if(p[1] > max[1])  max[1] = p[1]; else if(p[1] < min[1]) min[1] = p[1];
      if(p[2] > max[2])  max[2] = p[2]; else if(p[2] < min[2]) min[2] = p[2];
    }
    
    orig_offset_ = -cv::Point3f( min[0] + (max[0] - min[0])/2.0f, 
                                 min[1] + (max[1] - min[1])/2.0f, 
                                 min[2] + (max[2] - min[2])/2.0f);
  }

  OpenMesh::DefaultTraits::Point orig_offset(orig_offset_.x, orig_offset_.y, orig_offset_.z );
  for (Mesh::VertexIter v_it = mesh.vertices_sbegin(); v_it!=mesh.vertices_end(); ++v_it)
  {
    OpenMesh::DefaultTraits::Point &p = mesh.point ( *v_it );
    p += orig_offset; 
  }

  // Iterate over all faces
  for ( Mesh::FaceIter f_it = mesh.faces_begin(); f_it != mesh.faces_end(); ++f_it )
  {
    Mesh::FaceVertexIter fv_it = mesh.fv_iter ( *f_it );
    cv::Point3f p0, p1, p2;
    
    OpenMesh::DefaultTraits::Point &v0 = mesh.point ( *fv_it++ );

    mesh_model_ptr_->vertex_buffer_data.push_back ( v0[0] );
    mesh_model_ptr_->vertex_buffer_data.push_back ( v0[1] );
    mesh_model_ptr_->vertex_buffer_data.push_back ( v0[2] );
    
    p0.x = v0[0];
    p0.y = v0[1];
    p0.z = v0[2];

    OpenMesh::DefaultTraits::Point &v1 = mesh.point ( *fv_it++ );
    
    mesh_model_ptr_->vertex_buffer_data.push_back ( v1[0] );
    mesh_model_ptr_->vertex_buffer_data.push_back ( v1[1] );
    mesh_model_ptr_->vertex_buffer_data.push_back ( v1[2] );
    
    p1.x = v1[0];
    p1.y = v1[1];
    p1.z = v1[2];
    
    OpenMesh::DefaultTraits::Point &v2 = mesh.point ( *fv_it );
    
    mesh_model_ptr_->vertex_buffer_data.push_back ( v2[0] );
    mesh_model_ptr_->vertex_buffer_data.push_back ( v2[1] );
    mesh_model_ptr_->vertex_buffer_data.push_back ( v2[2] );
    
    p2.x = v2[0];
    p2.y = v2[1];
    p2.z = v2[2];
    
    
    addLine(p0, p1);
    addLine(p1, p2);
    addLine(p2, p0);
  }

  // std::cout<<pts_.size()<<std::endl;
  
  glDeleteBuffers ( 1, & ( mesh_model_ptr_->vertex_buffer ) );
  glGenBuffers ( 1, & ( mesh_model_ptr_->vertex_buffer ) );
  glBindBuffer ( GL_ARRAY_BUFFER, mesh_model_ptr_->vertex_buffer );
  glBufferData ( GL_ARRAY_BUFFER, sizeof ( GL_FLOAT ) *mesh_model_ptr_->vertex_buffer_data.size(),
                 mesh_model_ptr_->vertex_buffer_data.data(), GL_STATIC_DRAW );
  
}


void RasterObjectModel3D::updateRaster()
{
  glfwMakeContextCurrent ( mesh_model_ptr_->window );
 // Get a handle for our buffers
  GLuint vertex_pos_id = glGetAttribLocation ( mesh_model_ptr_->shader_program_id, "vertex_pos" );

  GLuint mat_id = glGetUniformLocation ( mesh_model_ptr_->shader_program_id, "rt_persp" );
  // Projection matrix : 45° Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
  glm::mat4 persp = glm::perspective ( render_fov_, 4.0f / 3.0f, render_z_near_, render_z_far_ );
  
  Eigen::Matrix3d rot_mat = rq_view_.toRotationMatrix().transpose();
  Eigen::Vector3d t_vec = -rot_mat*t_view_;
  

//   Eigen::Matrix3d rot_mat = rq_view_.toRotationMatrix();
//   Eigen::Vector3d t_vec = t_view_;

//   Eigen::Vector3d look_at_direction = rot_mat*Eigen::Vector3d(0,0,1);
//   Eigen::Vector3d up_direction = rot_mat*Eigen::Vector3d(0,-1,0);
// 
//   glm::mat4 view = glm::lookAt ( glm::vec3 ( t_vec(0), t_vec(1), t_vec(2) ), 
//                                  glm::vec3 ( look_at_direction(0),look_at_direction(1),look_at_direction(2) ), 
//                                  glm::vec3 ( up_direction(0),up_direction(1),up_direction(2) ) );
  

  glm::mat4 view = glm::lookAt ( glm::vec3 ( 0, 0, 0 ), 
                                 glm::vec3 ( 0, 0, .1 ), 
                                 glm::vec3 ( 0, 1, 0 ) );

  glm::mat4 model_view=glm::translate(glm::mat4(1.0f), glm::vec3((float)t_view_(0), (float)t_view_(1), (float)t_view_(2)));
  glm::quat q_view; 
  q_view=glm::quat((float)rq_view_.w(), (float)rq_view_.x(), (float)rq_view_.y(), (float)rq_view_.z());
  model_view=model_view*glm::mat4_cast(q_view);
  
  mesh_model_ptr_->rt_persp = persp * view*model_view; 
  rt_persp_=mesh_model_ptr_->rt_persp;

  glClear ( GL_DEPTH_BUFFER_BIT );

  glUseProgram ( mesh_model_ptr_->shader_program_id );

  glUniformMatrix4fv ( mat_id, 1, GL_FALSE, &(mesh_model_ptr_->rt_persp[0][0]) );
  
  

  glEnableVertexAttribArray ( vertex_pos_id );
  glBindBuffer ( GL_ARRAY_BUFFER, mesh_model_ptr_->vertex_buffer );
  glVertexAttribPointer ( vertex_pos_id, 3, GL_FLOAT, GL_FALSE, 0, ( void* )0 );

  // Draw the triangles
  glDrawArrays ( GL_TRIANGLES, 0, mesh_model_ptr_->vertex_buffer_data.size() );
  
  glDisableVertexAttribArray ( vertex_pos_id );

  glfwSwapBuffers ( mesh_model_ptr_->window );

   
   glReadPixels ( 0, 0, render_win_size_.width, render_win_size_.height, GL_DEPTH_COMPONENT, GL_FLOAT, ( GLvoid * ) depth_buffer_data_.data() );
   
//   cv::Mat flipped_depth_img ( render_win_size_.height, render_win_size_.width, cv::DataType<float>::type, ( float * ) depth_buffer_data_.data() );
//   cv::Mat depth_im ( render_win_size_.height, render_win_size_.width, cv::DataType<float>::type );
//   for ( int i_r = render_win_size_.width - 1, o_r = 0; i_r >= 0; i_r--, o_r++ )
//     flipped_depth_img.col ( i_r ).copyTo ( depth_im.col ( o_r ) );
// 
//  cv::normalize ( depth_im, debug_img, 0, 255, cv::NORM_MINMAX, cv::DataType<uchar>::type );
//  cv::cvtColor(debug_img, color_debug_img, cv::COLOR_GRAY2BGR);
//   cv::imshow("depth_buffer_", color_debug_img);
//     cv::waitKey(1);
   
  Mesh &mesh = mesh_model_ptr_->mesh;
    
  vis_pts_.clear();
  vis_d_pts_.clear();
  vis_segs_.clear();
  vis_d_segs_.clear();
  
  depth_transf_a_ = render_z_near_*render_z_far_;
  depth_transf_b_ = render_z_far_ + render_z_near_;
  depth_transf_c_ = render_z_far_ - render_z_near_;
        
  // Iterate over all faces
   
  Eigen::Matrix4d RT(Eigen::Matrix4d::Identity()); RT.block<3,3>(0,0)=rq_view_.toRotationMatrix(); RT.col(3).head(3)=t_view_;  

  for ( Mesh::FaceIter f_it = mesh.faces_begin(); f_it != mesh.faces_end(); ++f_it  )
  {
    Mesh::FaceHalfedgeIter fh_it = mesh.fh_iter ( *f_it );
    Mesh::Normal face_normal = mesh.calc_face_normal(*f_it);
    
    Eigen::Vector3d rotated_face_normal_eigen((double)face_normal[0],(double)face_normal[1],(double)face_normal[2]);
    rotated_face_normal_eigen=RT.block<3,3>(0,0)*rotated_face_normal_eigen;
    //cv::Vec3f rotated_face_normal((float)rotated_face_normal_eigen(0),(float)rotated_face_normal_eigen(1),(float)rotated_face_normal_eigen(2));

    for(; fh_it.is_valid(); ++fh_it)
    {
      Mesh::HalfedgeHandle opposite_heh = mesh.opposite_halfedge_handle(*fh_it);
      Mesh::Normal near_face_normal = mesh.calc_face_normal(mesh.face_handle(opposite_heh));      
      OpenMesh::Vec3f normals_diff = near_face_normal - face_normal;
      float norm_dist = float(std::sqrt(normals_diff[0]*normals_diff[0] + normals_diff[1]*normals_diff[1] + normals_diff[2]*normals_diff[2]));
      const OpenMesh::VertexHandle from_ph = mesh.from_vertex_handle(*fh_it),
                                  to_ph = mesh.to_vertex_handle(*fh_it);
                                  
      const OpenMesh::DefaultTraits::Point &from_p = mesh.point(from_ph),
                                          &to_p = mesh.point(to_ph);
      
      cv::Point3f p0(from_p[0], from_p[1], from_p[2]), 
                  p1(to_p[0], to_p[1], to_p[2]);
      
                  
      if( norm_dist > normal_epsilon_ )
      {
        addVisibleLine( p0, p1 );
      }
      else
      {
        Eigen::Vector3f p0_e(from_p[0], from_p[1], from_p[2]), 
                  p1_e(to_p[0], to_p[1], to_p[2]);
        Eigen::Vector4d pfrom((double)p0_e(0), (double)p0_e(1), (double)p0_e(2),1);
        Eigen::Vector4d pto((double)p1_e(0), (double)p1_e(1), (double)p1_e(2),1);
        pfrom=RT*pfrom; pto=RT*pto;
        pfrom=pfrom/pfrom(3); pto=pto/pto(3);
        Eigen::Vector3d centr((pfrom+pto).head(3)/2);
        double dot=centr.dot(rotated_face_normal_eigen);
        
        Eigen::Vector3d rotated_near_face_normal_eigen((double)near_face_normal[0],(double)near_face_normal[1],(double)near_face_normal[2]);
        rotated_near_face_normal_eigen=RT.block<3,3>(0,0)*rotated_near_face_normal_eigen;
        //cv::Vec3f rotated_near_face_normal((float)rotated_near_face_normal_eigen(0),(float)rotated_near_face_normal_eigen(1),(float)rotated_near_face_normal_eigen(2));
        double dot_near=centr.dot(rotated_near_face_normal_eigen);
        if((dot*dot_near)<0 )
        {        
          addVisibleLine( p0, p1 );
        }
      }
    }
  }
}

// void RasterObjectModel3D::debugMoveCamera ( float x, float y, float z )
// {
//   // Get a handle for our buffers
//   GLuint vertex_pos_id = glGetAttribLocation ( mesh_model_ptr_->shader_program_id, "vertex_pos" );
// 
//   GLuint mat_id = glGetUniformLocation ( mesh_model_ptr_->shader_program_id, "rt_persp" );
//   // Projection matrix : 45° Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
//   glm::mat4 persp = glm::perspective ( render_fov_, 4.0f / 3.0f, render_z_near_, render_z_far_ );
//   
//   // Camera matrix
//   glm::mat4 view = glm::lookAt (
// //                            glm::vec3 ( 40*4,40*3,-40*3 ), // Camera is at (4,3,-3), in World Space
//                            glm::vec3 ( x, y, z ),
//                            glm::vec3 ( 0,0,0 ), // and looks at the origin
//                            glm::vec3 ( 0,1,0 ) // Head is up (set to 0,-1,0 to look upside-down)
//                          );
// 
//   glm::mat4 rt_persp = persp * view; 
// 
//   glClear ( GL_DEPTH_BUFFER_BIT );
// 
//   glUseProgram ( mesh_model_ptr_->shader_program_id );
// 
//   glUniformMatrix4fv ( mat_id, 1, GL_FALSE, &rt_persp[0][0] );
// 
//   glEnableVertexAttribArray ( vertex_pos_id );
//   glBindBuffer ( GL_ARRAY_BUFFER, mesh_model_ptr_->vertex_buffer );
//   glVertexAttribPointer ( vertex_pos_id, 3, GL_FLOAT, GL_FALSE, 0, ( void* )0 );
// 
//   // Draw the triangles
//   glDrawArrays ( GL_TRIANGLES, 0, mesh_model_ptr_->vertex_buffer_data.size() );
// 
//   glDisableVertexAttribArray ( vertex_pos_id );
// 
//   glfwSwapBuffers ( mesh_model_ptr_->window );
// 
//   float *data = new float[render_win_size_.width*render_win_size_.height];
//   glReadPixels ( 0, 0, render_win_size_.width, render_win_size_.height, GL_DEPTH_COMPONENT, GL_FLOAT, ( GLvoid * ) data );
//   cv::Mat tmp_depth_img ( render_win_size_.height, render_win_size_.width, cv::DataType<float>::type, data ),
//           depth_img ( render_win_size_.height, render_win_size_.width, cv::DataType<float>::type );
//   for ( int i_r = render_win_size_.height - 1, o_r = 0; i_r >= 0; i_r--, o_r++ )
//     tmp_depth_img.row ( i_r ).copyTo ( depth_img.row ( o_r ) );
//   cv::Mat debug_img, rgb_debug_img;
//   cv::normalize ( depth_img, debug_img, 0, 255, cv::NORM_MINMAX, cv::DataType<uchar>::type );
//   cv::cvtColor(debug_img, rgb_debug_img, cv::COLOR_GRAY2BGR);
//   for( int i = 0; i < triangles_.size(); i++ )
//   {
//     glm::vec4 p0(triangles_[i].p0.x, triangles_[i].p0.y, triangles_[i].p0.z, 1.0f);
//     glm::vec4 p1(triangles_[i].p1.x, triangles_[i].p1.y, triangles_[i].p1.z, 1.0f);
//     glm::vec4 p2(triangles_[i].p2.x, triangles_[i].p2.y, triangles_[i].p2.z, 1.0f);
//     
//     glm::vec4 proj_p0 = rt_persp*p0, 
//               proj_p1 = rt_persp*p1, 
//               proj_p2 = rt_persp*p2;
//    
// 
//     // std::cout<<proj_p0[0]<<" "<<proj_p0[1]<<" "<<proj_p0[2]<<" "<<proj_p0[3]<<" "<<std::endl;
//     
//     float d0 = proj_p0[2];
//     float d1 = proj_p1[2];
//     float d2 = proj_p2[2];
//     
//     proj_p0 /= proj_p0[3];
//     proj_p1 /= proj_p1[3];
//     proj_p2 /= proj_p2[3];
//     
//     proj_p0 = proj_p0 * 0.5f + 0.5f;
//     proj_p1 = proj_p1 * 0.5f + 0.5f;
//     proj_p2 = proj_p2 * 0.5f + 0.5f;
//     
//     
//     cv::Point img_p0(round(proj_p0[0] * render_win_size_.width), render_win_size_.height - round(proj_p0[1] * render_win_size_.height));
//     cv::Point img_p1(round(proj_p1[0] * render_win_size_.width), render_win_size_.height - round(proj_p1[1] * render_win_size_.height));
//     cv::Point img_p2(round(proj_p2[0] * render_win_size_.width), render_win_size_.height - round(proj_p2[1] * render_win_size_.height));
//     
// 
//     if( img_p0.x >= 0&& img_p0.y >= 0 && img_p0.x < render_win_size_.width && img_p0.y < render_win_size_.height && 
//         img_p1.x >= 0&& img_p1.y >= 0 && img_p1.x < render_win_size_.width && img_p1.y < render_win_size_.height )
//     {
//      
//       float z_n0 = 2.0 * depth_img.at<float>(img_p0.y, img_p0.x) - 1.0;
//       float z_e0 = (2.0 * render_z_near_*render_z_far_) / (render_z_far_ + render_z_near_ - z_n0 * (render_z_far_ - render_z_near_));
//       
//       float z_n1 = 2.0 * depth_img.at<float>(img_p1.y, img_p1.x) - 1.0;
//       float z_e1 = (2.0 * render_z_near_*render_z_far_) / (render_z_far_ + render_z_near_ - z_n1 * (render_z_far_ - render_z_near_));
//       
//       if( d0 <= z_e0 + depth_buffer_epsilon_ && d1 <= z_e1 + depth_buffer_epsilon_ )
//         cv::line(rgb_debug_img, img_p0, img_p1, CV_RGB(255, 0, 0));
//     }
//     
//     if( img_p1.x >= 0&& img_p1.y >= 0 && img_p1.x < render_win_size_.width && img_p1.y < render_win_size_.height && 
//         img_p2.x >= 0&& img_p2.y >= 0 && img_p2.x < render_win_size_.width && img_p2.y < render_win_size_.height )
//     {
//      
//       float z_n1 = 2.0 * depth_img.at<float>(img_p1.y, img_p1.x) - 1.0;
//       float z_e1 = (2.0 * render_z_near_*render_z_far_) / (render_z_far_ + render_z_near_ - z_n1 * (render_z_far_ - render_z_near_));
//       
//       float z_n2 = 2.0 * depth_img.at<float>(img_p2.y, img_p2.x) - 1.0;
//       float z_e2 = (2.0 * render_z_near_*render_z_far_) / (render_z_far_ + render_z_near_ - z_n2 * (render_z_far_ - render_z_near_));
//        
//       if( d1 <= z_e1 + depth_buffer_epsilon_ && d2 <= z_e2 + depth_buffer_epsilon_ )
//         cv::line(rgb_debug_img, img_p1, img_p2, CV_RGB(255, 0, 0));
//     }
//     
//     if( img_p2.x >= 0&& img_p2.y >= 0 && img_p2.x < render_win_size_.width && img_p2.y < render_win_size_.height && 
//         img_p0.x >= 0&& img_p0.y >= 0 && img_p0.x < render_win_size_.width && img_p0.y < render_win_size_.height )
//     {
//      
//       float z_n2 = 2.0 * depth_img.at<float>(img_p2.y, img_p2.x) - 1.0;
//       float z_e2 = (2.0 * render_z_near_*render_z_far_) / (render_z_far_ + render_z_near_ - z_n2 * (render_z_far_ - render_z_near_));
//       
//       float z_n0 = 2.0 * depth_img.at<float>(img_p0.y, img_p0.x) - 1.0;
//       float z_e0 = (2.0 * render_z_near_*render_z_far_) / (render_z_far_ + render_z_near_ - z_n0 * (render_z_far_ - render_z_near_));
//       
//       if( d2 <= z_e2 + depth_buffer_epsilon_ && d0 <= z_e0 + depth_buffer_epsilon_ )
//         cv::line(rgb_debug_img, img_p2, img_p0, CV_RGB(255, 0, 0));      ;
//     }
//     
//     
// // // //     if(d1 < 1.0f && d2 < 1.0f )
// //     {
// //       cv::line(rgb_debug_img, img_p1, img_p2, CV_RGB(255, 255, 255));
// //       std::cout<<d1<<" "<<d2<<std::endl;
// //     }
// // // //     if(d2 < 1.0f && d0 < 1.0f )
// //     {
// //       cv::line(rgb_debug_img, img_p2, img_p0, CV_RGB(255, 255, 255));
// //       std::cout<<d2<<" "<<d0<<std::endl;
// //     }
//     
// /*
//     
//     std::cout<<proj_p0[0]<<" "<<proj_p0[1]<<" "<<proj_p0[2]<<" "<<proj_p0[3]<<std::endl;
//     std::cout<<proj_p1[0]<<" "<<proj_p1[1]<<" "<<proj_p1[2]<<" "<<proj_p1[3]<<std::endl;
//     std::cout<<proj_p2[0]<<" "<<proj_p2[1]<<" "<<proj_p2[2]<<" "<<proj_p2[3]<<std::endl;*/
// 
//   }
//   // TODO 
//   cv::imshow ( "depth",rgb_debug_img );
//   while ( cv::waitKey() != 27 );
//   delete [] data;
// }

void RasterObjectModel3D::getPoints ( vector< cv::Point3f >& pts ) const
{
  pts = pts_;
}

void RasterObjectModel3D::getPoints ( vector< cv::Point3f >& pts, 
                                      vector< cv::Point3f >& d_pts ) const
{
  pts = pts_;
  d_pts = d_pts_;
}

void RasterObjectModel3D::getSegments ( vector< cv::Vec6f >& segments ) const
{
  segments = segs_;
}

void RasterObjectModel3D::getSegments ( vector< cv::Vec6f >& segments, 
                                        vector< cv::Point3f >& d_segments ) const
{
  segments = segs_;
  d_segments = d_segs_;
}

void RasterObjectModel3D::getVisiblePoints ( vector< cv::Point3f >& pts ) const
{
  pts = vis_pts_;
}

void RasterObjectModel3D::getVisiblePoints ( vector< cv::Point3f >& pts, 
                                             vector< cv::Point3f >& d_pts ) const
{
  pts = vis_pts_;
  d_pts = vis_d_pts_;
}

void RasterObjectModel3D::getVisibleSegments ( vector< cv::Vec6f >& segments ) const
{
  segments = vis_segs_;
}

void RasterObjectModel3D::getVisibleSegments ( vector< cv::Vec6f >& segments, 
                                               vector< cv::Point3f>& d_segments ) const
{
  segments = vis_segs_;
  d_segments = vis_d_segs_;
}

void RasterObjectModel3D::getVisiblePoints ( int idx, vector< cv::Point3f >& pts ) const
{
  pts = precomputed_vis_pts_[idx];
}

void RasterObjectModel3D::getVisiblePoints ( int idx, vector< cv::Point3f >& pts, 
                                             vector< cv::Point3f >& d_pts ) const
{
  pts = precomputed_vis_pts_[idx];
  d_pts = precomputed_vis_d_pts_[idx];
}

void RasterObjectModel3D::getVisibleSegments ( int idx, vector< cv::Vec6f >& segments ) const
{
  segments = precomputed_vis_segs_[idx];
}

void RasterObjectModel3D::getVisibleSegments ( int idx, vector< cv::Vec6f >& segments, 
                                               vector< cv::Point3f >& d_segments ) const
{
  segments = precomputed_vis_segs_[idx];
  d_segments = precomputed_vis_d_segs_[idx];
}

void RasterObjectModel3D::getDepthBufferData(int idx, vector<float>& dept_buffer_data) const
{
  dept_buffer_data=precomputed_depth_buffer_data_[idx];
}

void RasterObjectModel3D::getDepthBufferData(int idx, int idx2, float& dept_buffer_data_value) const
{
  dept_buffer_data_value=precomputed_depth_buffer_data_[idx][idx2];
}

void RasterObjectModel3D::addLine ( cv::Point3f& p0, cv::Point3f& p1 )
{
  cv::Point3f dir = p1 - p0;
  float len = std::sqrt(dir.dot(dir));
  if(!len)
    return;
  
  int n_steps = len/step_;
  cv::Point3f dp(p0 + epsilon_*dir); 
  if( len >= min_seg_len_ )
  {
    segs_.push_back( cv::Vec6f(p0.x, p0.y, p0.z, p1.x, p1.y, p1.z) );
    d_segs_.push_back(dp);
  }
  
  if( !n_steps )
  {
    // Push at least the first point
    pts_.push_back(p0);
    d_pts_.push_back(dp);
  }
  else
  {
    float dist_step = 1.0/n_steps;
      
    for(int i = 0; i <= n_steps; i++)
    {
      pts_.push_back(p0 + (i*dist_step)*dir);
      d_pts_.push_back(p0 + (i*dist_step + epsilon_)*dir);
    }
  }
}

void RasterObjectModel3D::addVisibleLine ( cv::Point3f& p0, cv::Point3f& p1 )
{
  cv::Point3f dir = p1 - p0;
  float len = std::sqrt(dir.dot(dir));
  if(!len)
    return;
  
  int init_i = vis_pts_.size();
        
  int n_steps = len/step_;
  if( !n_steps )
  {
    // Try to push at least the first point
    if(checkPointOcclusion(p0))
    {
      vis_pts_.push_back(p0);
      vis_d_pts_.push_back(p0 + epsilon_*dir);
    }
  }
  else
  {
    float dist_step = 1.0/n_steps;
    for(int i = 0; i <= n_steps; i++)
    {
      cv::Point3f p = p0 + (i*dist_step)*dir;
      if(checkPointOcclusion(p))
      {
        vis_pts_.push_back(p);
        vis_d_pts_.push_back(p+ epsilon_*dir);
      }
    }
  }    
  
  if( vis_pts_.size() > init_i )
  {
    cv::Point3f s_seg = vis_pts_.at(init_i), e_seg = vis_pts_.back();
    cv::Point3f diff = s_seg - e_seg; 
    len = std::sqrt(diff.dot(diff));
    if( len >= min_seg_len_ )
    {
      vis_segs_.push_back( cv::Vec6f(s_seg.x, s_seg.y, s_seg.z, e_seg.x, e_seg.y, e_seg.z) );
      vis_d_segs_.push_back(s_seg + epsilon_*dir);
    }
  }
}


// bool RasterObjectModel3D::checkPointOcclusion( cv::Point3f& p )
// {
//   cv::Point proj_p;
//   glm::vec4 hp(p.x, p.y, p.z, 1.0f);
//   glm::vec4 proj_hp = mesh_model_ptr_->rt_persp*hp;
//   float depth = proj_hp[3];
//   proj_hp /= proj_hp[3];
//   proj_hp = proj_hp * 0.5f + 0.5f;
//   proj_p.x = round(proj_hp[0] * render_win_size_.width); 
//   proj_p.y = render_win_size_.height - round(proj_hp[1] * render_win_size_.height);
//   if( proj_p.x < 1 || proj_p.y < 1 || proj_p.x > render_win_size_.width + 2 || proj_p.y > render_win_size_.height + 2 )
//     return false;
//   
//   float closest_depth0 = (2.0 * depth_transf_a_) / (depth_transf_b_ - (2.0 * depth_buffer_.at<float>(proj_p.y, proj_p.x) - 1.0) * (depth_transf_c_));
//   float closest_depth1 = (2.0 * depth_transf_a_) / (depth_transf_b_ - (2.0 * depth_buffer_.at<float>(proj_p.y + 1, proj_p.x) - 1.0) * (depth_transf_c_));
//   float closest_depth2 = (2.0 * depth_transf_a_) / (depth_transf_b_ - (2.0 * depth_buffer_.at<float>(proj_p.y - 1, proj_p.x) - 1.0) * (depth_transf_c_));
//   float closest_depth3 = (2.0 * depth_transf_a_) / (depth_transf_b_ - (2.0 * depth_buffer_.at<float>(proj_p.y, proj_p.x + 1) - 1.0) * (depth_transf_c_));
//   float closest_depth4 = (2.0 * depth_transf_a_) / (depth_transf_b_ - (2.0 * depth_buffer_.at<float>(proj_p.y, proj_p.x - 1) - 1.0) * (depth_transf_c_));
// 
//   if( depth <= closest_depth0 + depth_buffer_epsilon_ ||
//       depth <= closest_depth1 + depth_buffer_epsilon_ ||
//       depth <= closest_depth2 + depth_buffer_epsilon_ ||
//       depth <= closest_depth3 + depth_buffer_epsilon_ ||
//       depth <= closest_depth4 + depth_buffer_epsilon_ )
//   {
//     // color_debug_img.at<cv::Vec3b>(proj_p.y, proj_p.x) = cv::Vec3b(0,0,255);
//     return true;
//   }
//   else
//     return false;
// }

int RasterObjectModel3D::projectPointToGLPersp(const int idx, const cv::Point3f& p, cv::Point& proj_p, float& depth)
{
  glm::vec4 hp(p.x, p.y, p.z, 1.0f);
  glm::vec4 proj_hp = precomputed_rt_persp_[idx]*hp;
  depth = proj_hp[3];
  proj_hp /= proj_hp[3];
  proj_hp = proj_hp * 0.5f + 0.5f;
//   proj_p.x = round(proj_hp[0] * render_win_size_.width); 
  proj_p.x = render_win_size_.width - round(proj_hp[0] * render_win_size_.width); 
//   proj_p.y = render_win_size_.height - round(proj_hp[1] * render_win_size_.height);
  proj_p.y = round(proj_hp[1] * render_win_size_.height);
  if( proj_p.x < 1 || proj_p.y < 1 || proj_p.x > render_win_size_.width + 2 || proj_p.y > render_win_size_.height + 2 )
  {
    proj_p.x=-1; proj_p.y=-1; depth=-1;
    return -1;
  }
//   return (render_win_size_.height-1-proj_p.y)*render_win_size_.width +proj_p.x;
  return proj_p.y*render_win_size_.width + (render_win_size_.width-1-proj_p.x);
}

int RasterObjectModel3D::projectPointToGLPersp(const cv::Point3f& p, cv::Point& proj_p, float& depth)
{
  glm::vec4 hp(p.x, p.y, p.z, 1.0f);
  glm::vec4 proj_hp = rt_persp_*hp;
  depth = proj_hp[3];
  proj_hp /= proj_hp[3];
  proj_hp = proj_hp * 0.5f + 0.5f;
//   proj_p.x = round(proj_hp[0] * render_win_size_.width); 
  proj_p.x = render_win_size_.width - round(proj_hp[0] * render_win_size_.width); 
//   proj_p.y = render_win_size_.height - round(proj_hp[1] * render_win_size_.height);
  proj_p.y = round(proj_hp[1] * render_win_size_.height);

  if( proj_p.x < 1 || proj_p.y < 1 || proj_p.x > render_win_size_.width + 2 || proj_p.y > render_win_size_.height + 2 )
  {
    proj_p.x=-1; proj_p.y=-1; depth=-1;
    return -1;
  }
//   return (render_win_size_.height-1-proj_p.y)*render_win_size_.width +proj_p.x;
  return proj_p.y*render_win_size_.width + (render_win_size_.width-1-proj_p.x);
}

bool RasterObjectModel3D::checkPointOcclusion( cv::Point3f& p )
{
  cv::Point proj_p;
  float depth;
  int idx0=projectPointToGLPersp(p, proj_p, depth);
  if(idx0==-1)
    return false;

//   int idx1=(render_win_size_.height-1-proj_p.y+1)*render_win_size_.width +proj_p.x;
//   int idx2=(render_win_size_.height-1-proj_p.y-1)*render_win_size_.width +proj_p.x;
//   int idx3=(render_win_size_.height-1-proj_p.y)*render_win_size_.width +proj_p.x+1;
//   int idx4=(render_win_size_.height-1-proj_p.y)*render_win_size_.width +proj_p.x-1;
  int idx1=(proj_p.y+1)*render_win_size_.width + (render_win_size_.width-1-proj_p.x);
  int idx2=(proj_p.y-1)*render_win_size_.width + (render_win_size_.width-1-proj_p.x);
  int idx3=proj_p.y*render_win_size_.width + (render_win_size_.width-1-proj_p.x+1);
  int idx4=proj_p.y*render_win_size_.width + (render_win_size_.width-1-proj_p.x-1);
  float closest_depth0 = (2.0 * depth_transf_a_) / (depth_transf_b_ - (2.0 * depth_buffer_data_[idx0] - 1.0) * (depth_transf_c_));
  float closest_depth1 = (2.0 * depth_transf_a_) / (depth_transf_b_ - (2.0 * depth_buffer_data_[idx1] - 1.0) * (depth_transf_c_));
  float closest_depth2 = (2.0 * depth_transf_a_) / (depth_transf_b_ - (2.0 * depth_buffer_data_[idx2] - 1.0) * (depth_transf_c_));
  float closest_depth3 = (2.0 * depth_transf_a_) / (depth_transf_b_ - (2.0 * depth_buffer_data_[idx3] - 1.0) * (depth_transf_c_));
  float closest_depth4 = (2.0 * depth_transf_a_) / (depth_transf_b_ - (2.0 * depth_buffer_data_[idx4] - 1.0) * (depth_transf_c_));

  if( depth <= closest_depth0 + depth_buffer_epsilon_ ||
      depth <= closest_depth1 + depth_buffer_epsilon_ ||
      depth <= closest_depth2 + depth_buffer_epsilon_ ||
      depth <= closest_depth3 + depth_buffer_epsilon_ ||
      depth <= closest_depth4 + depth_buffer_epsilon_ )
  {
    // color_debug_img.at<cv::Vec3b>(proj_p.y, proj_p.x) = cv::Vec3b(0,0,255);
    return true;
  }
  else
    return false;
}

void RasterObjectModel3D::retreiveModel( int idx )
{
  vis_pts_ = precomputed_vis_pts_[idx];
  vis_d_pts_ = precomputed_vis_d_pts_[idx];
  vis_segs_ = precomputed_vis_segs_[idx];
  vis_d_segs_ = precomputed_vis_d_segs_[idx];
  
  if(precomputed_rt_persp_.size()>idx && precomputed_depth_buffer_data_.size()>idx)
  {
    rt_persp_=precomputed_rt_persp_[idx];
    depth_buffer_data_=precomputed_depth_buffer_data_[idx];
  }
}

void RasterObjectModel3D::storeModel()
{
  precomputed_vis_pts_.push_back(vis_pts_);
  precomputed_vis_d_pts_.push_back(vis_d_pts_);
  precomputed_vis_segs_.push_back(vis_segs_);
  precomputed_vis_d_segs_.push_back(vis_d_segs_);

  if(depth_buffer_storing_enabled_)
  {
    precomputed_rt_persp_.push_back(rt_persp_);
    precomputed_depth_buffer_data_.push_back(depth_buffer_data_);
  }
}

void RasterObjectModel3D::storePrecomputedModelsFromOther(RasterObjectModel3DPtr& other)
{
  precomputed_vis_pts_=other->precomputed_vis_pts_;
  precomputed_vis_d_pts_=other->precomputed_vis_d_pts_;
  precomputed_vis_segs_=other->precomputed_vis_segs_;
  precomputed_vis_d_segs_=other->precomputed_vis_d_segs_;
  precomputed_rq_view_=other->precomputed_rq_view_;
  precomputed_t_view_=other->precomputed_t_view_;
}

void RasterObjectModel3D::loadPrecomputedModels ( cv::FileStorage& fs )
{
  precomputed_vis_pts_.clear();
  precomputed_vis_d_pts_.clear();
  precomputed_vis_segs_.clear();
  precomputed_vis_d_segs_.clear();
  
  int n_precomputed_views = precomputed_rq_view_.size();
  
  for( int i = 0; i < n_precomputed_views; i++ )
  {
    precomputed_vis_pts_.push_back(std::vector<cv::Point3f>());
    precomputed_vis_d_pts_.push_back(std::vector<cv::Point3f>());
    
    cv::Mat model_views_pts;
    std::stringstream sname;
    sname<<"model_views_pts_"<<i;
    fs[sname.str().data()] >> model_views_pts;
   
    for( int j = 0; j < model_views_pts.rows; j++ )
    {
      cv::Point3f pt, d_pt;
      pt.x = model_views_pts.at<float>(j,0);
      pt.y = model_views_pts.at<float>(j,1);
      pt.z = model_views_pts.at<float>(j,2);
      
      d_pt.x = model_views_pts.at<float>(j,3);
      d_pt.y = model_views_pts.at<float>(j,4);
      d_pt.z = model_views_pts.at<float>(j,5);
      
      precomputed_vis_pts_[i].push_back(pt);
      precomputed_vis_d_pts_[i].push_back(d_pt);
    }
    
    precomputed_vis_segs_.push_back(std::vector<cv::Vec6f>());
    precomputed_vis_d_segs_.push_back(std::vector<cv::Point3f>());
    
    sname.clear();
    sname<<"model_views_segs_"<<i;
    cv::Mat model_views_segs;
    fs[sname.str().data()] >> model_views_segs;
    
    for( int j = 0; j < model_views_segs.rows; j++ )
    {
      cv::Vec6f seg;
      cv::Point3f d_seg;
      seg[0] = model_views_segs.at<float>(j,0);
      seg[1] = model_views_segs.at<float>(j,1);
      seg[2] = model_views_segs.at<float>(j,2);
      seg[3] = model_views_segs.at<float>(j,3);
      seg[4] = model_views_segs.at<float>(j,4);
      seg[5] = model_views_segs.at<float>(j,5);
      
      d_seg.x = model_views_pts.at<float>(j,6);
      d_seg.y = model_views_pts.at<float>(j,7);
      d_seg.z = model_views_pts.at<float>(j,8);
      
      precomputed_vis_segs_[i].push_back(seg);
      precomputed_vis_d_segs_[i].push_back(d_seg);
    }
  } 
}

void RasterObjectModel3D::savePrecomputedModels ( cv::FileStorage& fs ) const
{
  for( int i = 0; i < precomputed_vis_pts_.size(); i++ )
  {
    const std::vector<cv::Point3f> &pts = precomputed_vis_pts_[i],
                                   &d_pts = precomputed_vis_d_pts_[i];
    cv::Mat model_views_pts(precomputed_vis_pts_[i].size(), 6, cv::DataType<float>::type );
    for( int j = 0; j < precomputed_vis_pts_[i].size(); j++)
    {
      model_views_pts.at<float>(j,0) = pts[j].x;
      model_views_pts.at<float>(j,1) = pts[j].y;
      model_views_pts.at<float>(j,2) = pts[j].z;
      
      model_views_pts.at<float>(j,3) = d_pts[j].x;
      model_views_pts.at<float>(j,4) = d_pts[j].y;
      model_views_pts.at<float>(j,5) = d_pts[j].z;
    }
    
    std::stringstream sname;
    sname<<"model_views_pts_"<<i;
    fs << sname.str().data() << model_views_pts;
    
    const std::vector<cv::Vec6f> &segs = precomputed_vis_segs_[i];
    const std::vector<cv::Point3f> &d_segs = precomputed_vis_d_segs_[i];
    cv::Mat model_views_segs(precomputed_vis_segs_[i].size(), 9, cv::DataType<float>::type );
    for( int j = 0; j < precomputed_vis_segs_[i].size(); j++)
    {
      model_views_segs.at<float>(j,0) = segs[j][0];
      model_views_segs.at<float>(j,1) = segs[j][1];
      model_views_segs.at<float>(j,2) = segs[j][2];
      model_views_segs.at<float>(j,3) = segs[j][3];
      model_views_segs.at<float>(j,4) = segs[j][4];
      model_views_segs.at<float>(j,5) = segs[j][5];
      
      model_views_segs.at<float>(j,6) = d_segs[j].x;
      model_views_segs.at<float>(j,7) = d_segs[j].y;
      model_views_segs.at<float>(j,8) = d_segs[j].z;
    }
    sname.clear();
    sname<<"model_views_segs_"<<i;
    fs << sname.str().data() << model_views_segs;
  }
}
