#include "chamfer_matching.h"

#include <omp.h>
#include <algorithm>

#include <ceres/ceres.h>
#include "cv_ext/cv_ext.h"

void computeSegmentImage( const cv::Mat& src_img, cv::Mat& seg_img )
{
  std::vector< cv::Vec4f > segments;
  extractEdgesPyr( src_img, segments );
  seg_img=cv::Mat(src_img.size(), cv::DataType<uchar>::type, cv::Scalar(255));
  cv_ext::drawSegments( seg_img, segments, cv::Scalar(0), 1);
}

void computeDistanceMap( const cv::Mat& src_img, cv::Mat& dist_map )
{
  const int dist_type = CV_DIST_L2, mask_size = 5;
  
  std::vector< cv::Vec4f > segments;
  extractEdgesPyr( src_img, segments );
  cv::Mat seg_img(src_img.size(), cv::DataType<uchar>::type, cv::Scalar(255));
  cv_ext::drawSegments( seg_img, segments, cv::Scalar(0), 1);
  
  cv::distanceTransform ( seg_img, dist_map, dist_type, mask_size );

}

void computeDistanceMap( const cv::Mat& src_img, cv::Mat& dist_map, 
                         cv::Mat &closest_edgels_map )
{
  const int dist_type = CV_DIST_L2, mask_size = 5;
  
  std::vector< cv::Vec4f > segments;
  extractEdgesPyr( src_img, segments );
  cv::Mat seg_img(src_img.size(), cv::DataType<uchar>::type, cv::Scalar(255));
  cv_ext::drawSegments( seg_img, segments, cv::Scalar(0), 1);
  
  cv::Mat labels_img(src_img.size(), cv::DataType<u_int32_t>::type );
  closest_edgels_map = cv::Mat(src_img.size(), cv::DataType<cv::Point2f>::type );
  
  cv::distanceTransform(seg_img, dist_map, labels_img, dist_type, mask_size, cv::DIST_LABEL_PIXEL );

  int labels_size = seg_img.total() - cv::countNonZero(seg_img) + 1;
  std::vector< cv::Point2f> edgels(labels_size);
  
  for( int y = 0; y < labels_img.rows; y++)
  {
    const uchar *seg_row_p = seg_img.ptr<uchar>(y);
    const u_int32_t *label_row_p = labels_img.ptr<u_int32_t>(y);
    for( int x = 0; x < labels_img.cols; x++, label_row_p++, seg_row_p++ )
    {
      if( !(*seg_row_p) )
        edgels[*label_row_p] = cv::Point2f(x,y);
    }
  }
  
  for( int y = 0; y < labels_img.rows; y++)
  {
    cv::Point2f *points_row_p = closest_edgels_map.ptr<cv::Point2f>(y);
    const u_int32_t *label_row_p = labels_img.ptr<u_int32_t>(y);
    for( int x = 0; x < labels_img.cols; x++, label_row_p++, points_row_p++ )
      *points_row_p = edgels[*label_row_p];
  }

//     for( int y = 0; y < labels_img.rows; y++)
//     {
//       const cv::Point2f *points_row_p = closest_edgels_map.ptr<cv::Point2f>(y);
//       for( int x = 0; x < labels_img.cols; x++, points_row_p++ )
//       {
//         cv::Mat dbg_img = seg_img.clone();
//         const cv::Point2f &closest_edgels = *points_row_p;
//         cv::line(dbg_img, cv::Point(x,y), cv::Point(closest_edgels.x, closest_edgels.y), 
//                  cv::Scalar(128,128,128));
//         cv_ext::showImage(dbg_img, "seg_img");
//       }
//     }

  //cv_ext::showImage(seg_img, "seg_img");      
}

void computeImageIntegrals ( const cv::Mat& src_img, 
                             IntegralImageVectorPtr& int_imgs_ptr, 
                             int num_directions )
{
  int_imgs_ptr = IntegralImageVectorPtr( new std::vector<cv_ext::IntegralImage<float> >() );
  std::vector<cv_ext::IntegralImage<float> > &int_imgs = *int_imgs_ptr;
  int_imgs.reserve(num_directions);
  double ang_step = M_PI/double(num_directions);
  for( double ang = -M_PI/2; ang < M_PI/2; ang += ang_step )
    int_imgs.push_back( cv_ext::IntegralImage<float>( src_img, ang ) );
}

void computeDistanceMapTensor ( const cv::Mat& src_img, 
                                ImageTensorPtr& dst_map_tensor_ptr, 
                                double lambda, int num_directions )
{
  int w = src_img.cols, h = src_img.rows;
  const int dist_type = CV_DIST_L2, mask_size = 5;
    
  std::vector< cv::Vec4f > segments;
  std::vector< float > normals;
  std::vector<cv::Point2f> centers;
  extractEdgesPyr( src_img, segments );
  //extractEdges( src_img, segments );
  ///// debug
  //cv::Mat seg_img=cv::Mat(src_img.size(), cv::DataType<uchar>::type, cv::Scalar(255));
  //cv_ext::drawSegments( seg_img, segments, cv::Scalar(0), 1);
  //cv_ext::showImage(seg_img, "tmp"/*, true, 10  */);
  
  computeEdgesNormalsDirections( segments, normals, centers );
  
  dst_map_tensor_ptr = ImageTensorPtr( new std::vector<cv::Mat>() );
  std::vector<cv::Mat> &dst_map_tensor = *dst_map_tensor_ptr;
  dst_map_tensor.reserve( num_directions );
  
  double eta_dir = double(num_directions)/M_PI;
  double ang_step = 1.0/eta_dir;
  std::vector< std::vector<cv::Vec4f> > dir_segments;
  dir_segments.resize(num_directions);
  
  for( int i = 0; i < segments.size(); i++ )
  { 
    float direction = normals[i];

    direction += M_PI/2;
    if( direction >= M_PI/2 )
      direction -= M_PI;
    direction += M_PI/2;
    
    int i_dir = round(eta_dir*direction);
    i_dir %= num_directions;
    dir_segments[i_dir].push_back( segments[i] );
  }

  for( int i = 0; i < num_directions; i++ )
  {
    cv::Mat seg_img(src_img.size(), cv::DataType<uchar>::type, cv::Scalar(255));
    cv_ext::drawSegments( seg_img, dir_segments[i], cv::Scalar(0), 1);
    dst_map_tensor.push_back( cv::Mat(src_img.size(), cv::DataType<float>::type ) );
    cv::distanceTransform ( seg_img, dst_map_tensor[i], dist_type, mask_size );
    //cv_ext::showImage(dst_map_tensor[i], "Segments");
    
  }

  const float ang_penalty = lambda * ang_step;
  //std::cout<<"ang_penalty : "<<ang_penalty<<std::endl;
  
  // Forward recursion
  bool first_pass = true;
  for( int i = 0, prev_i = num_directions - 1; i < num_directions; i++, prev_i++ )
  {
//     int updated = 0;
    prev_i %= num_directions;
    for( int y = 0; y < h; y++)
    {
      float *prev_map_p = dst_map_tensor[prev_i].ptr<float>(y),
            *cur_map_p = dst_map_tensor[i].ptr<float>(y); 
      for( int x = 0; x < w; x++, prev_map_p++, cur_map_p++ )
      {
        float prev_dist = *prev_map_p + ang_penalty;
        if(*cur_map_p > prev_dist )
        {
          *cur_map_p = prev_dist;
//           updated++;
        }
      }
    }
//     std::cout<<"Updated : "<<updated<<" over : "<<w*h<<std::endl;
    if( i == num_directions - 1 && first_pass )
    {
      i = -1;
      first_pass = false;
    }
  }

  // Backward recursion
  first_pass = true;
  for( int i = num_directions - 1, next_i = 0; i >= 0; i--, next_i-- )
  {
    if( next_i < 0 )
      next_i = num_directions - 1;
//     int updated = 0;
    for( int y = 0; y < h; y++)
    {
      float *cur_map_p = dst_map_tensor[i].ptr<float>(y),
            *next_map_p = dst_map_tensor[next_i].ptr<float>(y);
      for( int x = 0; x < w; x++, cur_map_p++, next_map_p++ )
      {
        float next_dist = *next_map_p + ang_penalty;
        if(*cur_map_p > next_dist )
        {
          *cur_map_p = next_dist;
//           updated++;
        }
      }
    }
//     std::cout<<"Updated : "<<updated<<" over : "<<w*h<<std::endl;
    if( i == 0 && first_pass )
    {
      i = num_directions;
      first_pass = false;
    }
  }
  
  // Smoothing
  ImageTensorPtr smoothed_tensor_ptr = ImageTensorPtr( new std::vector<cv::Mat>() );
  smoothed_tensor_ptr->reserve( num_directions );
  
  for( int i = 0, prev_i = num_directions - 1, next_i = 1; i < num_directions; i++, prev_i++, next_i++ )
  {
    prev_i %= num_directions;
    next_i %= num_directions;
    
    smoothed_tensor_ptr->push_back( 0.25*dst_map_tensor_ptr->at(prev_i) +
                                    0.5*dst_map_tensor_ptr->at(i) + 
                                    0.25*dst_map_tensor_ptr->at(next_i));
  }
  dst_map_tensor_ptr = smoothed_tensor_ptr;
}

void computeDistanceMapTensor( const cv::Mat &src_img, 
                               ImageTensorPtr &dst_map_tensor_ptr, 
                               ImageTensorPtr &edgels_map_tensor_ptr,
                               double lambda, int num_directions )
{
  int w = src_img.cols, h = src_img.rows;
  const int dist_type = CV_DIST_L2, mask_size = 5;
  
  std::vector< cv::Vec4f > segments;
  std::vector< float > normals;
  std::vector<cv::Point2f> centers;
  extractEdgesPyr( src_img, segments );
  computeEdgesNormalsDirections( segments, normals, centers );
  
  dst_map_tensor_ptr = ImageTensorPtr( new std::vector<cv::Mat>() );
  edgels_map_tensor_ptr = ImageTensorPtr( new std::vector<cv::Mat>() );
  std::vector<cv::Mat> &dst_map_tensor = *dst_map_tensor_ptr,
                       &edgels_map_tensor = *edgels_map_tensor_ptr;
  
  dst_map_tensor.reserve( num_directions );
  edgels_map_tensor.reserve( num_directions );
  
  double eta_dir = double(num_directions)/M_PI;
  double ang_step = 1.0/eta_dir;
  std::vector< std::vector<cv::Vec4f> > dir_segments;
  dir_segments.resize(num_directions);
  
  for( int i = 0; i < segments.size(); i++ )
  { 
    float direction = normals[i];

    direction += M_PI/2;
    if( direction >= M_PI/2 )
      direction -= M_PI;
    direction += M_PI/2;
    
    int i_dir = round(eta_dir*direction);
    i_dir %= num_directions;
    dir_segments[i_dir].push_back( segments[i] );
  }

  std::vector<cv::Mat> seg_imgs;
  for( int i = 0; i < num_directions; i++ )
  {
    cv::Mat seg_img(src_img.size(), cv::DataType<uchar>::type, cv::Scalar(255));
    cv_ext::drawSegments( seg_img, dir_segments[i], cv::Scalar(0), 1);
    seg_imgs.push_back(seg_img);
    dst_map_tensor.push_back( cv::Mat(src_img.size(), cv::DataType<float>::type ) );
    edgels_map_tensor.push_back( cv::Mat(src_img.size(), cv::DataType<cv::Point2f>::type ) );
    
    cv::Mat labels_img(src_img.size(), cv::DataType<u_int32_t>::type );
    cv::distanceTransform(seg_img, dst_map_tensor[i], labels_img, dist_type, mask_size, cv::DIST_LABEL_PIXEL );

    int labels_size = seg_img.total() - cv::countNonZero(seg_img) + 1;
    std::vector< cv::Point2f> edgels(labels_size);
  
    for( int y = 0; y < labels_img.rows; y++)
    {
      const uchar *seg_row_p = seg_img.ptr<uchar>(y);
      const u_int32_t *label_row_p = labels_img.ptr<u_int32_t>(y);
      for( int x = 0; x < labels_img.cols; x++, label_row_p++, seg_row_p++ )
      {
        if( !(*seg_row_p) )
          edgels[*label_row_p] = cv::Point2f(x,y);
      }
    }
  
    for( int y = 0; y < labels_img.rows; y++)
    {
      cv::Point2f *points_row_p = edgels_map_tensor[i].ptr<cv::Point2f>(y);
      const u_int32_t *label_row_p = labels_img.ptr<u_int32_t>(y);
      for( int x = 0; x < labels_img.cols; x++, label_row_p++, points_row_p++ )
        *points_row_p = edgels[*label_row_p];
    }
  }

  const float ang_penalty = lambda * ang_step;
  //std::cout<<"ang_penalty : "<<ang_penalty<<std::endl;
  
  // Forward recursion
  bool first_pass = true;
  for( int i = 0, prev_i = num_directions - 1; i < num_directions; i++, prev_i++ )
  {
//     int updated = 0;
    prev_i %= num_directions;
    for( int y = 0; y < h; y++)
    {
      float *prev_map_p = dst_map_tensor[prev_i].ptr<float>(y),
            *cur_map_p = dst_map_tensor[i].ptr<float>(y);
      cv::Point2f *prev_points_map_p = edgels_map_tensor[prev_i].ptr<cv::Point2f>(y),
                  *cur_points_map_p = edgels_map_tensor[i].ptr<cv::Point2f>(y);
                  
      for( int x = 0; x < w; x++, prev_map_p++, cur_map_p++, prev_points_map_p++, cur_points_map_p++ )
      {
        float prev_dist = *prev_map_p + ang_penalty;
        if( *cur_map_p > prev_dist )
        {
          *cur_map_p = prev_dist;
          *cur_points_map_p = *prev_points_map_p;
//           updated++;
        }
      }
    }
//     std::cout<<"Updated : "<<updated<<" over : "<<w*h<<std::endl;
    if( i == num_directions - 1 && first_pass )
    {
      i = -1;
      first_pass = false;
    }
  }

  // Backward recursion
  first_pass = true;
  for( int i = num_directions - 1, next_i = 0; i >= 0; i--, next_i-- )
  {
    if( next_i < 0 )
      next_i = num_directions - 1;
//     int updated = 0;
    for( int y = 0; y < h; y++)
    {
      float *cur_map_p = dst_map_tensor[i].ptr<float>(y),
            *next_map_p = dst_map_tensor[next_i].ptr<float>(y);
      cv::Point2f *cur_points_map_p = edgels_map_tensor[i].ptr<cv::Point2f>(y),
                  *next_points_map_p = edgels_map_tensor[next_i].ptr<cv::Point2f>(y);
      for( int x = 0; x < w; x++, cur_map_p++, next_map_p++, cur_points_map_p++, next_points_map_p++ )
      {
        float next_dist = *next_map_p + ang_penalty;
        if(*cur_map_p > next_dist )
        {
          *cur_map_p = next_dist;
          *cur_points_map_p = *next_points_map_p;
//           updated++;
        }
      }
    }
//     std::cout<<"Updated : "<<updated<<" over : "<<w*h<<std::endl;
    if( i == 0 && first_pass )
    {
      i = num_directions;
      first_pass = false;
    }
  }
  
//   for( int i = 0; i < num_directions; i++ )
//   {
//     for( int y = 0; y < h; y++)
//     {
//       float *cur_map_p = dst_map_tensor[i].ptr<float>(y);
//       cv::Point2f *cur_points_map_p = edgels_map_tensor[i].ptr<cv::Point2f>(y);
//       for( int x = 0; x < w; x++, cur_map_p++, cur_points_map_p++ )
//       {
//         *cur_map_p = sqrt( (float(x) - (*cur_points_map_p).x)*(float(x) - (*cur_points_map_p).x) + 
//                            (float(y) - (*cur_points_map_p).y)*(float(y) - (*cur_points_map_p).y) );
//       }
//     }
//   }
  // Smoothing
  ImageTensorPtr smoothed_tensor_ptr = ImageTensorPtr( new std::vector<cv::Mat>() );
  smoothed_tensor_ptr->reserve( num_directions );
  
  for( int i = 0, prev_i = num_directions - 1, next_i = 1; i < num_directions; i++, prev_i++, next_i++ )
  {
    prev_i %= num_directions;
    next_i %= num_directions;
    
    smoothed_tensor_ptr->push_back( 0.25*dst_map_tensor_ptr->at(prev_i) +
                                    0.5*dst_map_tensor_ptr->at(i) + 
                                    0.25*dst_map_tensor_ptr->at(next_i));
//     cv_ext::showImage(seg_imgs[i],"seg_img", true, 10);
//     cv_ext::showImage(dst_map_tensor_ptr->at(i),"dst_map_tensor");
  }
  dst_map_tensor_ptr = smoothed_tensor_ptr;
}


void computeDistanceMapTensor( const cv::Mat &src_img, 
                               ImageTensorPtr &dst_map_tensor_ptr, 
                               ImageTensorPtr &x_dst_map_tensor_ptr, 
                               ImageTensorPtr &y_dst_map_tensor_ptr, 
                               ImageTensorPtr &edgels_map_tensor_ptr,
                               double lambda, int num_directions )
{
  int w = src_img.cols, h = src_img.rows;
  const int dist_type = CV_DIST_L2, mask_size = 5;
  
  std::vector< cv::Vec4f > segments;
  std::vector< float > normals;
  std::vector<cv::Point2f> centers;
  extractEdgesPyr( src_img, segments );
  computeEdgesNormalsDirections( segments, normals, centers );
  
  dst_map_tensor_ptr = ImageTensorPtr( new std::vector<cv::Mat>() );
  edgels_map_tensor_ptr = ImageTensorPtr( new std::vector<cv::Mat>() );
  std::vector<cv::Mat> &dst_map_tensor = *dst_map_tensor_ptr,
                       &edgels_map_tensor = *edgels_map_tensor_ptr;
  
  dst_map_tensor.reserve( num_directions );
  edgels_map_tensor.reserve( num_directions );
  
  double eta_dir = double(num_directions)/M_PI;
  double ang_step = 1.0/eta_dir;
  std::vector< std::vector<cv::Vec4f> > dir_segments;
  dir_segments.resize(num_directions);
  
  for( int i = 0; i < segments.size(); i++ )
  { 
    float direction = normals[i];

    direction += M_PI/2;
    if( direction >= M_PI/2 )
      direction -= M_PI;
    direction += M_PI/2;
    
    int i_dir = round(eta_dir*direction);
    i_dir %= num_directions;
    dir_segments[i_dir].push_back( segments[i] );
  }


  for( int i = 0; i < num_directions; i++ )
  {
    cv::Mat seg_img(src_img.size(), cv::DataType<uchar>::type, cv::Scalar(255));
    cv_ext::drawSegments( seg_img, dir_segments[i], cv::Scalar(0), 1);
    dst_map_tensor.push_back( cv::Mat( src_img.size(), cv::DataType<float>::type ) );
    edgels_map_tensor.push_back( cv::Mat(src_img.size(), cv::DataType<cv::Point2f>::type ) );
    
    cv::Mat labels_img(src_img.size(), cv::DataType<u_int32_t>::type );
    cv::distanceTransform(seg_img, dst_map_tensor[i], labels_img, dist_type, mask_size, cv::DIST_LABEL_PIXEL );

    int labels_size = seg_img.total() - cv::countNonZero(seg_img) + 1;
    std::vector< cv::Point2f> edgels(labels_size);
  
    for( int y = 0; y < labels_img.rows; y++)
    {
      const uchar *seg_row_p = seg_img.ptr<uchar>(y);
      const u_int32_t *label_row_p = labels_img.ptr<u_int32_t>(y);
      for( int x = 0; x < labels_img.cols; x++, label_row_p++, seg_row_p++ )
      {
        if( !(*seg_row_p) )
          edgels[*label_row_p] = cv::Point2f(x,y);
      }
    }
  
    for( int y = 0; y < labels_img.rows; y++)
    {
      cv::Point2f *points_row_p = edgels_map_tensor[i].ptr<cv::Point2f>(y);
      const u_int32_t *label_row_p = labels_img.ptr<u_int32_t>(y);
      for( int x = 0; x < labels_img.cols; x++, label_row_p++, points_row_p++ )
        *points_row_p = edgels[*label_row_p];
    }
  }

  const float ang_penalty = lambda * ang_step;
  //std::cout<<"ang_penalty : "<<ang_penalty<<std::endl;
  
  // Forward recursion
  bool first_pass = true;
  for( int i = 0, prev_i = num_directions - 1; i < num_directions; i++, prev_i++ )
  {
//     int updated = 0;
    prev_i %= num_directions;
    for( int y = 0; y < h; y++)
    {
      float *prev_map_p = dst_map_tensor[prev_i].ptr<float>(y),
            *cur_map_p = dst_map_tensor[i].ptr<float>(y);
      cv::Point2f *prev_points_map_p = edgels_map_tensor[prev_i].ptr<cv::Point2f>(y),
                  *cur_points_map_p = edgels_map_tensor[i].ptr<cv::Point2f>(y);
                  
      for( int x = 0; x < w; x++, prev_map_p++, cur_map_p++, prev_points_map_p++, cur_points_map_p++ )
      {
        float prev_dist = *prev_map_p + ang_penalty;
        if( *cur_map_p > prev_dist )
        {
          *cur_map_p = prev_dist;
          *cur_points_map_p = *prev_points_map_p;
//           updated++;
        }
      }
    }
//     std::cout<<"Updated : "<<updated<<" over : "<<w*h<<std::endl;
    if( i == num_directions - 1 && first_pass )
    {
      i = -1;
      first_pass = false;
    }
  }

  // Backward recursion
  first_pass = true;
  for( int i = num_directions - 1, next_i = 0; i >= 0; i--, next_i-- )
  {
    if( next_i < 0 )
      next_i = num_directions - 1;
//     int updated = 0;
    for( int y = 0; y < h; y++)
    {
      float *cur_map_p = dst_map_tensor[i].ptr<float>(y),
            *next_map_p = dst_map_tensor[next_i].ptr<float>(y);
      cv::Point2f *cur_points_map_p = edgels_map_tensor[i].ptr<cv::Point2f>(y),
                  *next_points_map_p = edgels_map_tensor[next_i].ptr<cv::Point2f>(y);
      for( int x = 0; x < w; x++, cur_map_p++, next_map_p++, cur_points_map_p++, next_points_map_p++ )
      {
        float next_dist = *next_map_p + ang_penalty;
        if(*cur_map_p > next_dist )
        {
          *cur_map_p = next_dist;
          *cur_points_map_p = *next_points_map_p;
//           updated++;
        }
      }
    }
//     std::cout<<"Updated : "<<updated<<" over : "<<w*h<<std::endl;
    if( i == 0 && first_pass )
    {
      i = num_directions;
      first_pass = false;
    }
  }
  
  x_dst_map_tensor_ptr = ImageTensorPtr( new std::vector<cv::Mat>() );
  y_dst_map_tensor_ptr = ImageTensorPtr( new std::vector<cv::Mat>() );

  std::vector<cv::Mat> &x_dst_map_tensor = *x_dst_map_tensor_ptr,
                       &y_dst_map_tensor = *y_dst_map_tensor_ptr;
  
  x_dst_map_tensor.reserve( num_directions );
  y_dst_map_tensor.reserve( num_directions );
  
  for( int i = 0 ; i < num_directions; i++ )
  {
    x_dst_map_tensor.push_back( cv::Mat(src_img.size(), cv::DataType<float>::type ) );
    y_dst_map_tensor.push_back( cv::Mat(src_img.size(), cv::DataType<float>::type ) );
    
    for( int y = 0; y < h; y++)
    {
      float *x_map_p = x_dst_map_tensor[i].ptr<float>(y),
            *y_map_p = y_dst_map_tensor[i].ptr<float>(y);
      cv::Point2f *points_map_p = edgels_map_tensor[i].ptr<cv::Point2f>(y);
      for( int x = 0; x < w; x++, x_map_p++, y_map_p++, points_map_p++ )
      {
        *x_map_p = (*points_map_p).x - float( x );
        *y_map_p = (*points_map_p).y - float( y );
      }
    }
    cv::GaussianBlur(x_dst_map_tensor[i], x_dst_map_tensor[i], cv::Size(0,0),5.0);
    cv::GaussianBlur(y_dst_map_tensor[i], y_dst_map_tensor[i], cv::Size(0,0),5.0);
//     cv_ext::showImage(x_dst_map_tensor[i], "x_dst_map_tensor[i]");
//     cv_ext::showImage(y_dst_map_tensor[i], "y_dst_map_tensor[i]");
  }
  
  // Smoothing
  ImageTensorPtr smoothed_tensor_ptr = ImageTensorPtr( new std::vector<cv::Mat>() );
  ImageTensorPtr x_smoothed_tensor_ptr = ImageTensorPtr( new std::vector<cv::Mat>() );
  ImageTensorPtr y_smoothed_tensor_ptr = ImageTensorPtr( new std::vector<cv::Mat>() );
  smoothed_tensor_ptr->reserve( num_directions );
  x_smoothed_tensor_ptr->reserve( num_directions );
  y_smoothed_tensor_ptr->reserve( num_directions );  
  
  for( int i = 0, prev_i = num_directions - 1, next_i = 1; i < num_directions; i++, prev_i++, next_i++ )
  {
    prev_i %= num_directions;
    next_i %= num_directions;

    smoothed_tensor_ptr->push_back( 0.25*dst_map_tensor_ptr->at(prev_i) +
                                    0.5*dst_map_tensor_ptr->at(i) + 
                                    0.25*dst_map_tensor_ptr->at(next_i));
    
    x_smoothed_tensor_ptr->push_back( 0.25*x_dst_map_tensor_ptr->at(prev_i) +
                                    0.5*x_dst_map_tensor_ptr->at(i) + 
                                    0.25*x_dst_map_tensor_ptr->at(next_i));
    
    y_smoothed_tensor_ptr->push_back( 0.25*y_dst_map_tensor_ptr->at(prev_i) +
                                    0.5*y_dst_map_tensor_ptr->at(i) + 
                                    0.25*y_dst_map_tensor_ptr->at(next_i));
  }
  
  dst_map_tensor_ptr = smoothed_tensor_ptr;  
  x_dst_map_tensor_ptr = x_smoothed_tensor_ptr;  
  y_dst_map_tensor_ptr = y_smoothed_tensor_ptr;  
}



class ChamferMatching::Optimizer
{
public:
  Optimizer(){};
  ~Optimizer(){};
  ceres::Problem problem;
};
 
struct ChamferResidual
{
  ChamferResidual ( const cv_ext::PinholeCameraModel &cam_model, const cv::Mat &distance_map,
                    const cv::Point3f &model_pt ) :
    cam_model_ ( cam_model ),
    dist_map_ ( distance_map )
  {
    model_pt_[0] = model_pt.x;
    model_pt_[1] = model_pt.y;
    model_pt_[2] = model_pt.z;
  }

  template <typename _T>
  bool operator() ( const _T* const pos, _T* residuals ) const
  {

    _T model_pt[3] = { _T ( model_pt_[0] ), _T ( model_pt_[1] ), _T ( model_pt_[2] ) };
    _T proj_pt[2];

    cam_model_.quatRTProject ( pos, pos + 4, model_pt, proj_pt );

    if ( proj_pt[0] < _T ( 1 ) ||  proj_pt[1] < _T ( 1 ) ||
         proj_pt[0] > _T ( cam_model_.imgWidth() - 2 ) ||
         proj_pt[1] > _T ( cam_model_.imgHeight() - 2 ) )
    {
      residuals[0] = _T ( 0 );
      return true;
    }

    //residuals[0] = cv_ext::bilinearInterp<float, _T> ( dist_map_, x, y );
    residuals[0] = cv_ext::getPix<float, _T> ( dist_map_, proj_pt[0], proj_pt[1] );
    return true;
  }

  static ceres::CostFunction* Create ( const cv_ext::PinholeCameraModel &cam_model,
                                       const cv::Mat &dist_map,
                                       const cv::Point3f &model_pt )
  {
    return ( new ceres::AutoDiffCostFunction<ChamferResidual, 1, 7 > (
               new ChamferResidual ( cam_model, dist_map, model_pt ) ) );
  }

  const cv_ext::PinholeCameraModel &cam_model_;
  const cv::Mat &dist_map_;
  double model_pt_[3];
};


ChamferMatching::ChamferMatching ( const cv_ext::PinholeCameraModel& cam_model, 
                                   const cv::Mat& dist_map, 
                                   const IntegralImageVectorPtr& int_dist_maps_ptr ) : 
  TemplateMatching ( cam_model )
{
  init(dist_map, int_dist_maps_ptr);
}

ChamferMatching::ChamferMatching ( const ChamferMatching& other ) : 
  TemplateMatching( other.cam_model_ )
{
  init(other.dist_map_, other.int_dist_maps_ptr_);
  if( other.model_ptr_ )
    this->setTemplateModel( other.model_ptr_ );
}

void ChamferMatching::init ( const cv::Mat& dist_map, 
                             const IntegralImageVectorPtr& int_dist_maps_ptr )
{
  dist_map_ =  dist_map;
  int_dist_maps_ptr_ = int_dist_maps_ptr;

  if( int_dist_maps_ptr_ )
  {
    num_directions_ = int_dist_maps_ptr_->size();
    eta_direction_ = double(num_directions_)/M_PI;
  }
  else
  {
    num_directions_ = eta_direction_ = 0;
  }
}

double ChamferMatching::optimize()
{
  if( !model_ptr_ || !optimizer_ptr_ )
    return -1;
  
  ceres::Solver::Options options;
  //options.linear_solver_type = ceres::DENSE_SCHUR;
  options.max_num_iterations = max_num_iterations_;
  options.linear_solver_type = ceres::DENSE_NORMAL_CHOLESKY;
  options.minimizer_progress_to_stdout = verbouse_mode_;

  ceres::Solver::Summary summary;
  ceres::Solve ( options, &optimizer_ptr_->problem, &summary );
  
  return summary.final_cost;
}

double ChamferMatching::avgDistance( int idx )
{
  if( !model_ptr_ )
    return 0;

  int n_pts = 0;
  double avg_dist = 0;
  
  // TODO
//   if( int_dist_maps_ptr_ )
//   {    
//     std::vector<cv_ext::IntegralImage<float> > &int_dist_maps = *int_dist_maps_ptr_;
//     std::vector<cv::Vec4f > proj_segs;
//     std::vector< float > normal_directions;
// 
//     model_ptr_->setModelView(r, t);
//     model_ptr_->projectRasterSegments(proj_segs, normal_directions );
//     int seg_len;
//     double sum;
//     for( int i = 0; i < proj_segs.size(); i++ )
//     {
//       const cv::Vec4f &seg = proj_segs[i];
//       if( seg[0] != -1 )
//       {
//         float direction = normal_directions[i] + M_PI/2;
//         if( direction >= M_PI/2 )
//           direction -= M_PI;
//         direction += M_PI/2;
//         
//         int i_dir = round(eta_direction_*direction);
//         i_dir %= num_directions_;
//         
//         sum = int_dist_maps[i_dir].getLineSum( seg, seg_len );
//         if( sum != std::numeric_limits<double>::max() )
//         {
//           avg_dist += sum;
//           n_pts += seg_len;
//         }
//       }
//     }
//     
//     if( n_pts )
//       avg_dist /= n_pts;
//     else
//       avg_dist = std::numeric_limits< float >::max();    
//   }
//   else
  {
    std::vector<cv::Point2f> proj_pts;

    if( idx < 0 )
      model_ptr_->projectRasterPoints( proj_pts );
    else
      model_ptr_->projectRasterPoints(idx, proj_pts );    

    for( int i = 0; i < proj_pts.size(); i++ )
    {
      const cv::Point2f &coord = proj_pts.at(i);
      if( coord.x >= 0)
      {
        n_pts++;
        int x = round(coord.x), y = round(coord.y);
        avg_dist += dist_map_.at<float>( y, x );
      }
    }

    if( n_pts )
      avg_dist /= n_pts;
    else
      avg_dist = std::numeric_limits< float >::max();
  }
  
  return avg_dist;
}

void ChamferMatching::updateOptimizer( int idx )
{
  if( !model_ptr_ ) return;
  std::vector<cv::Point3f> model_pts;
  
  if(model_ptr_->allVisiblePoints())
    model_ptr_->getPoints( model_pts );
  else
  {
    if( idx < 0 )
      model_ptr_->getVisiblePoints( model_pts );
    else
      model_ptr_->getVisiblePoints( idx, model_pts );
  }
  
  optimizer_ptr_ = boost::shared_ptr< Optimizer > ( new Optimizer () );
  
  for ( int i = 0; i < model_pts.size(); ++i )
  {
    ceres::CostFunction* cost_function =
      ChamferResidual::Create ( cam_model_, dist_map_, model_pts[i] );

    optimizer_ptr_->problem.AddResidualBlock ( cost_function, new ceres::HuberLoss(1.0), transf_.data() );
  }
}

struct ICPChamferResidual
{
  ICPChamferResidual ( const cv_ext::PinholeCameraModel &cam_model, 
                       const cv::Point3f &model_pt,
                       const cv::Point2f &img_pt ) :
    cam_model_ ( cam_model )
  {
    model_pt_[0] = model_pt.x;
    model_pt_[1] = model_pt.y;
    model_pt_[2] = model_pt.z;
    img_pt_[0] = img_pt.x;
    img_pt_[1] = img_pt.y;
  }

  template <typename _T>
  bool operator() ( const _T* const pos, _T* residuals ) const
  {

    _T model_pt[3] = { _T ( model_pt_[0] ), _T ( model_pt_[1] ), _T ( model_pt_[2] ) };
    _T img_pt[2] = {_T ( img_pt_[0] ), _T ( img_pt_[1] ) }, proj_pt[2];

    cam_model_.quatRTProject ( pos, pos + 4, model_pt, proj_pt );

    if ( proj_pt[0] < _T ( 1 ) ||  proj_pt[1] < _T ( 1 ) ||
         proj_pt[0] > _T ( cam_model_.imgWidth() - 2 ) ||
         proj_pt[1] > _T ( cam_model_.imgHeight() - 2 ) )
    {
      residuals[0] = _T ( 0 );
      residuals[1] = _T ( 0 );
      return true;
    }

    residuals[0] = img_pt[0] - proj_pt[0];
    residuals[1] = img_pt[1] - proj_pt[1];

    return true;
  }

  static ceres::CostFunction* Create ( const cv_ext::PinholeCameraModel &cam_model,
                                       const cv::Point3f &model_pt, const cv::Point2f &img_pt )
  {
    return ( new ceres::AutoDiffCostFunction<ICPChamferResidual, 2, 7 > (
               new ICPChamferResidual ( cam_model, model_pt, img_pt ) ) );
  }

  const cv_ext::PinholeCameraModel &cam_model_;
  double model_pt_[3], img_pt_[2];
};

class ICPChamferMatching::Optimizer
{
public:
  Optimizer(){};
  ~Optimizer(){};
  ceres::Problem problem;
};

ICPChamferMatching::ICPChamferMatching ( const cv_ext::PinholeCameraModel& cam_model, 
                                         const cv::Mat& closest_edgels_map ) : 
  TemplateMatching ( cam_model ),
  num_icp_iterations_(50),
  selected_idx_(-1)
{
  init(closest_edgels_map);
}

ICPChamferMatching::ICPChamferMatching ( const ICPChamferMatching& other ) : 
  TemplateMatching( other.cam_model_ )
{
  init(other.closest_edgels_map_ );
  if( other.model_ptr_ )
    this->setTemplateModel( other.model_ptr_ );
}

void ICPChamferMatching::init ( const cv::Mat& closest_edgels_map )
{
  closest_edgels_map_ =  closest_edgels_map;
}

double ICPChamferMatching::avgDistance ( int idx )
{
  if( !model_ptr_ )
    return 0;

  int n_pts = 0;
  double avg_dist = 0;
  
  std::vector<cv::Point2f> proj_pts;

  if( idx < 0 )
    model_ptr_->projectRasterPoints( proj_pts );
  else
    model_ptr_->projectRasterPoints(idx, proj_pts );    

  for( int i = 0; i < proj_pts.size(); i++ )
  {
    const cv::Point2f &coord = proj_pts.at(i);
    if( coord.x >= 0)
    {
      n_pts++;
      int x = round(coord.x), y = round(coord.y);
      const cv::Point2f &closest_edgel = closest_edgels_map_.at<cv::Point2f>( y, x );
      avg_dist += sqrt( (closest_edgel.x - coord.x)*(closest_edgel.x - coord.x) + 
                         (closest_edgel.y - coord.y)*(closest_edgel.y - coord.y) );
    }
  }

  if( n_pts )
    avg_dist /= n_pts;
  else
    avg_dist = std::numeric_limits< float >::max();
  
  return avg_dist;
}

void ICPChamferMatching::updateOptimizer ( int idx )
{
  if( !model_ptr_ ) return;
  
  selected_idx_ = idx;
  if(model_ptr_->allVisiblePoints())
    model_ptr_->getPoints( model_pts_ );
  else
  {
    if( idx < 0 )
      model_ptr_->getVisiblePoints( model_pts_ );
    else
      model_ptr_->getVisiblePoints( idx, model_pts_ );
  }
}

double ICPChamferMatching::optimize()
{
  if( !model_ptr_  )
    return -1;
  
  double final_cost = -1.0;
  for( int icp_iter = 0; icp_iter < num_icp_iterations_; icp_iter++)
  {
    std::vector<cv::Point2f> img_pts;
    if( selected_idx_ < 0 )
      model_ptr_->projectRasterPoints( transf_.data(), transf_.block<3,1>(4,0).data(), img_pts );
    else
      model_ptr_->projectRasterPoints( selected_idx_, transf_.data(), transf_.block<3,1>(4,0).data(), img_pts );
    
    optimizer_ptr_ = boost::shared_ptr< Optimizer > ( new Optimizer () );
    
    int res_size = 0;
    for ( int i = 0; i < model_pts_.size(); ++i )
    {
      if( img_pts[i].x >= 0 )
      {
        int x = round(img_pts[i].x), y = round(img_pts[i].y);
        const cv::Point2f &closest_edgel = closest_edgels_map_.at<cv::Point2f>( y, x );
        ceres::CostFunction* cost_function =
          ICPChamferResidual::Create ( cam_model_, model_pts_[i], closest_edgel );

        optimizer_ptr_->problem.AddResidualBlock ( cost_function, new ceres::HuberLoss(1.0), transf_.data() );
        res_size++;
      }
    }
    if( res_size >= 3 )
    {
      ceres::Solver::Options options;
      //options.linear_solver_type = ceres::DENSE_SCHUR;
      options.max_num_iterations = max_num_iterations_;
      options.linear_solver_type = ceres::DENSE_NORMAL_CHOLESKY;
      options.minimizer_progress_to_stdout = verbouse_mode_;

      ceres::Solver::Summary summary;
      ceres::Solve ( options, &optimizer_ptr_->problem, &summary );  
      final_cost = summary.final_cost;
    }
  }
  return final_cost;
}

class DirectionalChamferMatching::Optimizer
{
public:
  Optimizer(){};
  ~Optimizer(){};
  ceres::Problem problem;
};


struct DirectionalChamferResidual
{
  DirectionalChamferResidual ( const cv_ext::PinholeCameraModel &cam_model, 
                               const ImageTensorPtr &dist_map_tensor_ptr,
                               const cv::Point3f &model_pt, const cv::Point3f &model_dpt ) :
    cam_model_ ( cam_model ),
    dist_map_tensor_ptr_ ( dist_map_tensor_ptr ),
    dist_map_tensor_( *dist_map_tensor_ptr_ )
  {
    model_pt_[0] = model_pt.x;
    model_pt_[1] = model_pt.y;
    model_pt_[2] = model_pt.z;
    
    model_dpt_[0] = model_dpt.x;
    model_dpt_[1] = model_dpt.y;
    model_dpt_[2] = model_dpt.z;
    
    eta_direction_ = double(dist_map_tensor_.size())/M_PI;
    
    //cv::imshow("tensor[0]", dist_map_tensor_[23]);
    //cv::waitKey(0);
  }

  template <typename _T>
  bool operator() ( const _T* const pos, _T* residuals ) const
  {

    _T model_pt[3] = { _T ( model_pt_[0] ), _T ( model_pt_[1] ), _T ( model_pt_[2] ) };
    _T model_dpt[3] = { _T ( model_dpt_[0] ), _T ( model_dpt_[1] ), _T ( model_dpt_[2] ) };
    _T proj_pt[2], proj_dpt[2];

    cam_model_.quatRTProject ( pos, pos + 4, model_pt, proj_pt );
    cam_model_.quatRTProject ( pos, pos + 4, model_dpt, proj_dpt );

   if ( proj_pt[0] < _T ( 1 ) ||  proj_pt[1] < _T ( 1 ) ||
        proj_pt[0] > _T ( cam_model_.imgWidth() - 2 ) ||
        proj_pt[1] > _T ( cam_model_.imgHeight() - 2 ) ||
        proj_dpt[0] < _T ( 1 ) ||  proj_dpt[1] < _T ( 1 ) ||
        proj_dpt[0] > _T ( cam_model_.imgWidth() - 2 ) ||
        proj_dpt[1] > _T ( cam_model_.imgHeight() - 2 ) )
    {
      residuals[0] = _T ( 0 );
      return true;
    }
    
    _T diff[2] = { proj_dpt[0] - proj_pt[0], proj_dpt[1] - proj_pt[1] };
    _T direction;
    
    if( diff[0] != _T(0) )
      direction = atan( diff[1]/diff[0] );
    else
      direction = _T(-M_PI/2);
      
    _T z = _T(eta_direction_) * ( direction + _T(M_PI/2) );
    //residuals[0] = cv_ext::tensorGetPix<float, _T> ( dist_map_tensor_, proj_pt[0], proj_pt[1], z );
    residuals[0] = cv_ext::tensorbilinearInterp<float, _T> ( dist_map_tensor_, proj_pt[0], proj_pt[1], z );
    return true;
  }

  static ceres::CostFunction* Create ( const cv_ext::PinholeCameraModel &cam_model,
                                       const ImageTensorPtr &dist_map_tensor_ptr,
                                       const cv::Point3f &model_pt, const cv::Point3f &model_dpt)
  {
    return ( new ceres::AutoDiffCostFunction<DirectionalChamferResidual, 1, 7 > (
               new DirectionalChamferResidual( cam_model, dist_map_tensor_ptr, model_pt, model_dpt ) ) );
  }

  const cv_ext::PinholeCameraModel &cam_model_;
  const ImageTensorPtr dist_map_tensor_ptr_;
  const std::vector<cv::Mat> &dist_map_tensor_;
  double model_pt_[3], model_dpt_[3];
  double eta_direction_;
};


DirectionalChamferMatching::DirectionalChamferMatching ( const cv_ext::PinholeCameraModel& cam_model, 
                                                         const ImageTensorPtr& dist_map_tensor_ptr, 
                                                         const IntegralImageVectorPtr& int_dist_map_tensor_ptr ) : 
  TemplateMatching ( cam_model )
{
  init( dist_map_tensor_ptr, int_dist_map_tensor_ptr );
}

DirectionalChamferMatching::DirectionalChamferMatching ( const DirectionalChamferMatching& other ) : 
  TemplateMatching( other.cam_model_ )
{
  init( other.dist_map_tensor_ptr_, other.int_dist_map_tensor_ptr_ );
  if( other.model_ptr_ )
    this->setTemplateModel( other.model_ptr_ );
}

void DirectionalChamferMatching::init ( const ImageTensorPtr& dist_map_tensor_ptr, 
                                        const IntegralImageVectorPtr& int_dist_map_tensor_ptr )
{
  dist_map_tensor_ptr_ = dist_map_tensor_ptr;
  int_dist_map_tensor_ptr_ = int_dist_map_tensor_ptr;
  num_directions_ = dist_map_tensor_ptr_->size();
  eta_direction_ = double(num_directions_)/M_PI;
}

double DirectionalChamferMatching::avgDistance( int idx )
{
  if( !model_ptr_ )
    return - 1;

  int n_pts = 0;
  double avg_dist = 0;
  
  std::vector<cv::Point2f> proj_pts;
  std::vector<float> normal_directions;

  if( idx < 0 )
    model_ptr_->projectRasterPoints( proj_pts, normal_directions );
  else
    model_ptr_->projectRasterPoints( idx, proj_pts, normal_directions );

  std::vector<cv::Mat > &dist_map_tensor = *dist_map_tensor_ptr_;
  for( int i = 0; i < proj_pts.size(); i++ )
  {
    const cv::Point2f &coord = proj_pts.at(i);
    if( coord.x >= 0)
    {
      n_pts++;
      
      float direction = normal_directions[i] + M_PI/2;
      if( direction >= M_PI/2 )
        direction -= M_PI;
      direction += M_PI/2;
              
      int x = round(coord.x), y = round(coord.y);
      int z = round(eta_direction_*direction);
      z %= num_directions_;
      
      avg_dist += dist_map_tensor[z].at<float>( y, x );
    }
  }

  if( n_pts )
    avg_dist /= n_pts;
  else
    avg_dist = std::numeric_limits< float >::max();
  
  return avg_dist;
}


double DirectionalChamferMatching::optimize()
{
  if( !model_ptr_  || !optimizer_ptr_ )
    return -1;
  
  ceres::Solver::Options options;
  //options.linear_solver_type = ceres::DENSE_SCHUR;
  options.max_num_iterations = max_num_iterations_;
  options.linear_solver_type = ceres::DENSE_NORMAL_CHOLESKY;
  options.minimizer_progress_to_stdout = verbouse_mode_;

  ceres::Solver::Summary summary;
  ceres::Solve ( options, &optimizer_ptr_->problem, &summary );  
  
  return summary.final_cost;
}

void DirectionalChamferMatching::updateOptimizer( int idx )
{
  if( !model_ptr_ ) return;
  std::vector<cv::Point3f> model_pts, model_dpts;
    
  if(model_ptr_->allVisiblePoints())
    model_ptr_->getPoints( model_pts, model_dpts );
  else
  {
    if( idx < 0 )
      model_ptr_->getVisiblePoints( model_pts, model_dpts );
    else
      model_ptr_->getVisiblePoints( idx, model_pts, model_dpts );
  }
  
  optimizer_ptr_ = boost::shared_ptr< Optimizer > ( new Optimizer () );
  
  for ( int i = 0; i < model_pts.size(); ++i )
  {
    ceres::CostFunction* cost_function =
      DirectionalChamferResidual::Create ( cam_model_, dist_map_tensor_ptr_, model_pts[i], model_dpts[i] );

    optimizer_ptr_->problem.AddResidualBlock ( cost_function, new ceres::HuberLoss(1.0), transf_.data() );
  }
}


class BidirectionalChamferMatching::Optimizer
{
public:
  Optimizer(){};
  ~Optimizer(){};
  ceres::Problem problem;
};

struct BidirectionalChamferResidual
{
  BidirectionalChamferResidual ( const cv_ext::PinholeCameraModel &cam_model, 
                                 const ImageTensorPtr &x_dist_map_tensor_ptr,
                                 const ImageTensorPtr &y_dist_map_tensor_ptr,
                                 const cv::Point3f &model_pt, const cv::Point3f &model_dpt ) :
    cam_model_ ( cam_model ),
    x_dist_map_tensor_ptr_ ( x_dist_map_tensor_ptr ),
    y_dist_map_tensor_ptr_ ( y_dist_map_tensor_ptr ),
    x_dist_map_tensor_( *x_dist_map_tensor_ptr_ ),
    y_dist_map_tensor_( *y_dist_map_tensor_ptr_ )
  {
    model_pt_[0] = model_pt.x;
    model_pt_[1] = model_pt.y;
    model_pt_[2] = model_pt.z;
    
    model_dpt_[0] = model_dpt.x;
    model_dpt_[1] = model_dpt.y;
    model_dpt_[2] = model_dpt.z;
    
    eta_direction_ = double(x_dist_map_tensor_.size())/M_PI;
  }

  template <typename _T>
  bool operator() ( const _T* const pos, _T* residuals ) const
  {

    _T model_pt[3] = { _T ( model_pt_[0] ), _T ( model_pt_[1] ), _T ( model_pt_[2] ) };
    _T model_dpt[3] = { _T ( model_dpt_[0] ), _T ( model_dpt_[1] ), _T ( model_dpt_[2] ) };
    _T proj_pt[2], proj_dpt[2];

    cam_model_.quatRTProject ( pos, pos + 4, model_pt, proj_pt );
    cam_model_.quatRTProject ( pos, pos + 4, model_dpt, proj_dpt );

   if ( proj_pt[0] < _T ( 1 ) ||  proj_pt[1] < _T ( 1 ) ||
        proj_pt[0] > _T ( cam_model_.imgWidth() - 2 ) ||
        proj_pt[1] > _T ( cam_model_.imgHeight() - 2 ) ||
        proj_dpt[0] < _T ( 1 ) ||  proj_dpt[1] < _T ( 1 ) ||
        proj_dpt[0] > _T ( cam_model_.imgWidth() - 2 ) ||
        proj_dpt[1] > _T ( cam_model_.imgHeight() - 2 ) )
    {
      residuals[0] = _T ( 0 );
      residuals[1] = _T ( 0 );
      return true;
    }
    
    _T diff[2] = { proj_dpt[0] - proj_pt[0], proj_dpt[1] - proj_pt[1] };
    _T direction;
    
    if( diff[0] != _T(0) )
      direction = atan( diff[1]/diff[0] );
    else
      direction = _T(-M_PI/2);
      
    _T z = _T(eta_direction_) * ( direction + _T(M_PI/2) );
    //residuals[0] = cv_ext::tensorGetPix<float, _T> ( dist_map_tensor_, proj_pt[0], proj_pt[1], z );
    residuals[0] = cv_ext::tensorbilinearInterp<float, _T> ( x_dist_map_tensor_, proj_pt[0], proj_pt[1], z );
    residuals[1] = cv_ext::tensorbilinearInterp<float, _T> ( y_dist_map_tensor_, proj_pt[0], proj_pt[1], z );
    
    return true;
  }

  static ceres::CostFunction* Create ( const cv_ext::PinholeCameraModel &cam_model,
                                       const ImageTensorPtr &x_dist_map_tensor_ptr,
                                       const ImageTensorPtr &y_dist_map_tensor_ptr,
                                       const cv::Point3f &model_pt, const cv::Point3f &model_dpt)
  {
    return ( new ceres::AutoDiffCostFunction<BidirectionalChamferResidual, 2, 7 > (
               new BidirectionalChamferResidual( cam_model, x_dist_map_tensor_ptr, y_dist_map_tensor_ptr, 
                                                 model_pt, model_dpt ) ) );
  }

  const cv_ext::PinholeCameraModel &cam_model_;
  const ImageTensorPtr x_dist_map_tensor_ptr_, y_dist_map_tensor_ptr_;
  const std::vector<cv::Mat> &x_dist_map_tensor_, y_dist_map_tensor_;
  double model_pt_[3], model_dpt_[3];
  double eta_direction_;
};


BidirectionalChamferMatching::BidirectionalChamferMatching ( const cv_ext::PinholeCameraModel& cam_model, 
                                                             const ImageTensorPtr& x_dist_map_tensor_ptr, 
                                                             const ImageTensorPtr& y_dist_map_tensor_ptr, 
                                                             const IntegralImageVectorPtr& int_dist_map_tensor_ptr ) : 
  TemplateMatching ( cam_model )
{
  init( x_dist_map_tensor_ptr, y_dist_map_tensor_ptr, int_dist_map_tensor_ptr );
}

BidirectionalChamferMatching ::BidirectionalChamferMatching  ( const BidirectionalChamferMatching & other ) : 
  TemplateMatching( other.cam_model_ )
{
  init( other.x_dist_map_tensor_ptr_, other.y_dist_map_tensor_ptr_, other.int_dist_map_tensor_ptr_ );
  if( other.model_ptr_ )
    this->setTemplateModel( other.model_ptr_ );
}

void BidirectionalChamferMatching ::init ( const ImageTensorPtr& x_dist_map_tensor_ptr, 
                                           const ImageTensorPtr& y_dist_map_tensor_ptr, 
                                           const IntegralImageVectorPtr& int_dist_map_tensor_ptr )
{
  x_dist_map_tensor_ptr_ = x_dist_map_tensor_ptr;
  y_dist_map_tensor_ptr_ = y_dist_map_tensor_ptr;
    
  int_dist_map_tensor_ptr_ = int_dist_map_tensor_ptr;
  num_directions_ = x_dist_map_tensor_ptr_->size();
  eta_direction_ = double(num_directions_)/M_PI;
}

double BidirectionalChamferMatching ::avgDistance( int idx )
{
  if( !model_ptr_ )
    return - 1;

//   int n_pts = 0;
//   double avg_dist = 0;
//   
//   std::vector<cv::Point2f> proj_pts;
//   std::vector<float> normal_directions;
// 
//   if( idx < 0 )
//     model_ptr_->projectRasterPoints( proj_pts, normal_directions );
//   else
//     model_ptr_->projectRasterPoints( idx, proj_pts, normal_directions );
// 
//   std::vector<cv::Mat > &dist_map_tensor = *dist_map_tensor_ptr_;
//   for( int i = 0; i < proj_pts.size(); i++ )
//   {
//     const cv::Point2f &coord = proj_pts.at(i);
//     if( coord.x >= 0)
//     {
//       n_pts++;
//       
//       float direction = normal_directions[i] + M_PI/2;
//       if( direction >= M_PI/2 )
//         direction -= M_PI;
//       direction += M_PI/2;
//               
//       int x = round(coord.x), y = round(coord.y);
//       int z = round(eta_direction_*direction);
//       z %= num_directions_;
//       
//       avg_dist += dist_map_tensor[z].at<float>( y, x );
//     }
//   }
// 
//   if( n_pts )
//     avg_dist /= n_pts;
//   else
//     avg_dist = std::numeric_limits< float >::max();
//   
//   return avg_dist;
}


double BidirectionalChamferMatching ::optimize()
{
  if( !model_ptr_  || !optimizer_ptr_ )
    return -1;
  
  ceres::Solver::Options options;
  //options.linear_solver_type = ceres::DENSE_SCHUR;
  options.max_num_iterations = max_num_iterations_;
  options.linear_solver_type = ceres::DENSE_NORMAL_CHOLESKY;
  options.minimizer_progress_to_stdout = verbouse_mode_;

  ceres::Solver::Summary summary;
  ceres::Solve ( options, &optimizer_ptr_->problem, &summary );  
  
  return summary.final_cost;
}

void BidirectionalChamferMatching ::updateOptimizer( int idx )
{
  if( !model_ptr_ ) return;
  std::vector<cv::Point3f> model_pts, model_dpts;
    
  if(model_ptr_->allVisiblePoints())
    model_ptr_->getPoints( model_pts, model_dpts );
  else
  {
    if( idx < 0 )
      model_ptr_->getVisiblePoints( model_pts, model_dpts );
    else
      model_ptr_->getVisiblePoints( idx, model_pts, model_dpts );
  }
  
  optimizer_ptr_ = boost::shared_ptr< Optimizer > ( new Optimizer () );
  
  for ( int i = 0; i < model_pts.size(); ++i )
  {
    ceres::CostFunction* cost_function =
      BidirectionalChamferResidual::Create ( cam_model_, x_dist_map_tensor_ptr_, y_dist_map_tensor_ptr_,
                                             model_pts[i], model_dpts[i] );

    optimizer_ptr_->problem.AddResidualBlock ( cost_function, new ceres::HuberLoss(1.0), transf_.data() );
  }
}

class ICPDirectionalChamferMatching::Optimizer
{
public:
  Optimizer(){};
  ~Optimizer(){};
  ceres::Problem problem;
};

ICPDirectionalChamferMatching::ICPDirectionalChamferMatching ( const cv_ext::PinholeCameraModel& cam_model, 
                                                               const ImageTensorPtr& edgels_map_tensor_ptr ) : 
  TemplateMatching ( cam_model ),
  num_icp_iterations_(50),
  selected_idx_(-1)
{
  init(edgels_map_tensor_ptr);
}

ICPDirectionalChamferMatching::ICPDirectionalChamferMatching ( const ICPDirectionalChamferMatching& other ) : 
  TemplateMatching( other.cam_model_ )
{
  init( other.edgels_map_tensor_ptr_ );
  if( other.model_ptr_ )
    this->setTemplateModel( other.model_ptr_ );
}

void ICPDirectionalChamferMatching::init ( const ImageTensorPtr& edgels_map_tensor_ptr )
{
  edgels_map_tensor_ptr_ = edgels_map_tensor_ptr;
  num_directions_ = edgels_map_tensor_ptr_->size();
  eta_direction_ = double(num_directions_)/M_PI;
}

double ICPDirectionalChamferMatching::avgDistance ( int idx )
{
  if( !model_ptr_ )
    return - 1;

  int n_pts = 0;
  double avg_dist = 0;
  
  std::vector<cv::Point2f> proj_pts;
  std::vector<float> normal_directions;

  if( idx < 0 )
    model_ptr_->projectRasterPoints( proj_pts, normal_directions );
  else
    model_ptr_->projectRasterPoints( idx, proj_pts, normal_directions );

  std::vector<cv::Mat > &edgels_map_tensor = *edgels_map_tensor_ptr_;
  for( int i = 0; i < proj_pts.size(); i++ )
  {
    const cv::Point2f &coord = proj_pts.at(i);
    if( coord.x >= 0)
    {
      n_pts++;
      
      float direction = normal_directions[i] + M_PI/2;
      if( direction >= M_PI/2 )
        direction -= M_PI;
      direction += M_PI/2;
              
      int x = round(coord.x), y = round(coord.y);
      int z = round(eta_direction_*direction);
      z %= num_directions_;
      
      const cv::Point2f &closest_edgel = edgels_map_tensor[z].at<cv::Point2f>( y, x );
      avg_dist += sqrt( (closest_edgel.x - coord.x)*(closest_edgel.x - coord.x) + 
                        (closest_edgel.y - coord.y)*(closest_edgel.y - coord.y) );
      
    }
  }

  if( n_pts )
    avg_dist /= n_pts;
  else
    avg_dist = std::numeric_limits< float >::max();
  
  return avg_dist;
}

void ICPDirectionalChamferMatching::updateOptimizer ( int idx )
{
  if( !model_ptr_ ) return;
  
  selected_idx_ = idx;
  if(model_ptr_->allVisiblePoints())
    model_ptr_->getPoints( model_pts_ );
  else
  {
    if( idx < 0 )
      model_ptr_->getVisiblePoints( model_pts_ );
    else
      model_ptr_->getVisiblePoints( idx, model_pts_ );
  }
}

double ICPDirectionalChamferMatching::optimize()
{
  if( !model_ptr_ )
    return -1;
  
  double final_cost = -1.0;
  std::vector<cv::Mat > &edgels_map_tensor = *edgels_map_tensor_ptr_;
  
  for( int icp_iter = 0; icp_iter < num_icp_iterations_; icp_iter++)
  {
    
    std::vector<cv::Point2f> img_pts;
    std::vector<float> normal_directions;
    if( selected_idx_ < 0 )
      model_ptr_->projectRasterPoints( transf_.data(), transf_.block<3,1>(4,0).data(), 
                                       img_pts, normal_directions );
    else
      model_ptr_->projectRasterPoints( selected_idx_, transf_.data(), transf_.block<3,1>(4,0).data(), 
                                       img_pts, normal_directions );
    
    optimizer_ptr_ = boost::shared_ptr< Optimizer > ( new Optimizer () );
    
    int res_size = 0;
    for ( int i = 0; i < model_pts_.size(); ++i )
    {
      if( img_pts[i].x >= 0 )
      {
      
        float direction = normal_directions[i] + M_PI/2;
        if( direction >= M_PI/2 )
          direction -= M_PI;
        direction += M_PI/2;
                
        int x = round(img_pts[i].x), y = round(img_pts[i].y);
        int z = round(eta_direction_*direction);
        z %= num_directions_;
      
        const cv::Point2f &closest_edgel = edgels_map_tensor[z].at<cv::Point2f>( y, x );
        
        ceres::CostFunction* cost_function =
          ICPChamferResidual::Create ( cam_model_, model_pts_[i], closest_edgel );

        optimizer_ptr_->problem.AddResidualBlock ( cost_function, new ceres::HuberLoss(1.0), transf_.data() );
        res_size++;
      }
    }
    if( res_size >= 3 )
    {
      ceres::Solver::Options options;
      //options.linear_solver_type = ceres::DENSE_SCHUR;
      options.max_num_iterations = max_num_iterations_;
      options.linear_solver_type = ceres::DENSE_NORMAL_CHOLESKY;
      options.minimizer_progress_to_stdout = verbouse_mode_;

      ceres::Solver::Summary summary;
      ceres::Solve ( options, &optimizer_ptr_->problem, &summary );  
      final_cost = summary.final_cost;
    }
  }
  return final_cost;
}

class MultiViewsDirectionalChamferMatching::Optimizer
{
public:
  Optimizer(){};
  ~Optimizer(){};
  ceres::Problem problem;
};

/*struct MultiViewsDirectionalChamferResidual
{
  MultiViewsDirectionalChamferResidual ( const cv_ext::PinholeCameraModel &cam_model, 
                               const ImageTensorPtr &dist_map_tensor_ptr,
                               const cv::Point3f &model_pt, const cv::Point3f &model_dpt,
                               const Eigen::Quaterniond &view_q, const Eigen::Vector3d &view_t ) :
    cam_model_ ( cam_model ),
    dist_map_tensor_ptr_ ( dist_map_tensor_ptr ),
    dist_map_tensor_( *dist_map_tensor_ptr_ )
  {
    model_pt_[0] = model_pt.x;
    model_pt_[1] = model_pt.y;
    model_pt_[2] = model_pt.z;
    
    model_dpt_[0] = model_dpt.x;
    model_dpt_[1] = model_dpt.y;
    model_dpt_[2] = model_dpt.z;
    
    eta_direction_ = double(dist_map_tensor_.size())/M_PI;
    
    view_[0]=view_q.x(); view_[1]=view_q.y(); view_[2]=view_q.z(); view_[3]=view_q.w();
    view_[4]=view_t(0); view_[5]=view_t(1); view_[6]=view_t(2);
    
   // cv::imshow("tensor[0]", dist_map_tensor_[23]);
   // cv::waitKey(0);
  }

  template <typename _T>
  bool operator() ( const _T* const pos, _T* residuals ) const
  {

    _T model_pt[3] = { _T ( model_pt_[0] ), _T ( model_pt_[1] ), _T ( model_pt_[2] ) };
    _T model_dpt[3] = { _T ( model_dpt_[0] ), _T ( model_dpt_[1] ), _T ( model_dpt_[2] ) };
    _T proj_pt[2], proj_dpt[2];
    
    _T view[7]={_T(view_[0]), _T(view_[1]), _T(view_[2]), _T(view_[3]), _T(view_[4]), _T(view_[5]), _T(view_[6])};

   // cam_model_.quatRTProjectWithOffset ( pos, pos + 4, view, view+4,model_pt, proj_pt );
   // cam_model_.quatRTProjectWithOffset ( pos, pos + 4, view, view+4,model_pt, proj_dpt );
    
    cam_model_.quatRTProject ( pos, pos + 4, model_pt, proj_pt );
    cam_model_.quatRTProject ( pos, pos + 4, model_pt, proj_dpt );
    
   if ( proj_pt[0] < _T ( 1 ) ||  proj_pt[1] < _T ( 1 ) ||
        proj_pt[0] > _T ( cam_model_.imgWidth() - 2 ) ||
        proj_pt[1] > _T ( cam_model_.imgHeight() - 2 ) ||
        proj_dpt[0] < _T ( 1 ) ||  proj_dpt[1] < _T ( 1 ) ||
        proj_dpt[0] > _T ( cam_model_.imgWidth() - 2 ) ||
        proj_dpt[1] > _T ( cam_model_.imgHeight() - 2 ) )
    {
      residuals[0] = _T ( 0 );
      return true;
    }
    
    _T diff[2] = { proj_dpt[0] - proj_pt[0], proj_dpt[1] - proj_pt[1] };
    _T direction;
    
    if( diff[0] != _T(0) )
      direction = atan( diff[1]/diff[0] );
    else
      direction = _T(-M_PI/2);
      
    _T z = _T(eta_direction_) * ( direction + _T(M_PI/2) );
    //residuals[0] = cv_ext::tensorGetPix<float, _T> ( dist_map_tensor_, proj_pt[0], proj_pt[1], z );
    residuals[0] = cv_ext::tensorbilinearInterp<float, _T> ( dist_map_tensor_, proj_pt[0], proj_pt[1], z );
    return true;
  }

  static ceres::CostFunction* Create ( const cv_ext::PinholeCameraModel &cam_model,
                                       const ImageTensorPtr &dist_map_tensor_ptr,
                                       const cv::Point3f &model_pt, const cv::Point3f &model_dpt,
                                       const Eigen::Quaterniond &view_q, const Eigen::Vector3d &view_t)
  {
    return ( new ceres::AutoDiffCostFunction<MultiViewsDirectionalChamferResidual, 1, 7 > (
               new MultiViewsDirectionalChamferResidual( cam_model, dist_map_tensor_ptr, model_pt, model_dpt, view_q, view_t ) ) );
  }

  const cv_ext::PinholeCameraModel &cam_model_;
  const ImageTensorPtr dist_map_tensor_ptr_;
  const std::vector<cv::Mat> &dist_map_tensor_;
  double model_pt_[3], model_dpt_[3];
  double eta_direction_;
  double view_[7];
  
};
*/
struct MultiViewsDirectionalChamferResidual
{
  MultiViewsDirectionalChamferResidual ( const cv_ext::PinholeCameraModel &cam_model, 
                               const ImageTensorPtr &dist_map_tensor_ptr,
                               const cv::Point3f &model_pt, const cv::Point3f &model_dpt,
                               const Eigen::Quaterniond &view_q, const Eigen::Vector3d &view_t ) :
    cam_model_ ( cam_model ),
    dist_map_tensor_ptr_ ( dist_map_tensor_ptr ),
    dist_map_tensor_( *dist_map_tensor_ptr_ )
  {
    model_pt_[0] = model_pt.x;
    model_pt_[1] = model_pt.y;
    model_pt_[2] = model_pt.z;
    
    model_dpt_[0] = model_dpt.x;
    model_dpt_[1] = model_dpt.y;
    model_dpt_[2] = model_dpt.z;
    
    eta_direction_ = double(dist_map_tensor_.size())/M_PI;
    
   // view_[0]=view_q.x(); view_[1]=view_q.y(); view_[2]=view_q.z(); view_[3]=view_q.w();
    view_[0]=view_q.w(); view_[1]=view_q.x(); view_[2]=view_q.y(); view_[3]=view_q.z();
    view_[4]=view_t(0); view_[5]=view_t(1); view_[6]=view_t(2);
    
    //cv::imshow("tensor[0]", dist_map_tensor_[23]);
    //cv::waitKey(0);
  }

  template <typename _T>
  bool operator() ( const _T* const pos, _T* residuals ) const
  {

    _T model_pt[3] = { _T ( model_pt_[0] ), _T ( model_pt_[1] ), _T ( model_pt_[2] ) };
    _T model_dpt[3] = { _T ( model_dpt_[0] ), _T ( model_dpt_[1] ), _T ( model_dpt_[2] ) };
    _T proj_pt[2], proj_dpt[2];
    
    _T view[7]={_T(view_[0]), _T(view_[1]), _T(view_[2]), _T(view_[3]), _T(view_[4]), _T(view_[5]), _T(view_[6])};
    
    
    Eigen::Quaternion<_T> pos_q; cv_ext::quat2EigenQuat(pos, pos_q); pos_q.normalize();
    Eigen::Matrix<_T,3,1> pos_t; pos_t<<pos[4], pos[5], pos[6];
    Eigen::Quaternion<_T> view_q; cv_ext::quat2EigenQuat(view, view_q); //view_q.normalize();
    Eigen::Matrix<_T,3,1> view_t; view_t<<view[4], view[5], view[6];
    
    Eigen::Transform< _T,3, Eigen::Affine > pos_T, T, view_T;
    pos_T.linear()=Eigen::Matrix<_T, 3,3>(pos_q); pos_T.translation()=pos_t;
    view_T.linear()=Eigen::Matrix<_T, 3,3>(view_q); view_T.translation()=view_t;

    T=(view_T*pos_T);
    Eigen::Matrix<_T,3,1> transf_p; transf_p<<model_pt[0], model_pt[1], model_pt[2]; transf_p=T*transf_p;
    Eigen::Matrix<_T,3,1> transf_dp; transf_dp<<model_dpt[0], model_dpt[1], model_dpt[2]; transf_dp=T*transf_dp;
    
    
    _T transf_pt[3] = { transf_p(0) ,transf_p(1) ,transf_p(2)  };
    _T transf_dpt[3] = { transf_dp(0) , transf_dp(1) , transf_dp(2)  };
    cam_model_.project(transf_pt, proj_pt);
    cam_model_.project(transf_dpt, proj_dpt);


   if ( proj_pt[0] < _T ( 1 ) ||  proj_pt[1] < _T ( 1 ) ||
        proj_pt[0] > _T ( cam_model_.imgWidth() - 2 ) ||
        proj_pt[1] > _T ( cam_model_.imgHeight() - 2 ) ||
        proj_dpt[0] < _T ( 1 ) ||  proj_dpt[1] < _T ( 1 ) ||
        proj_dpt[0] > _T ( cam_model_.imgWidth() - 2 ) ||
        proj_dpt[1] > _T ( cam_model_.imgHeight() - 2 ) )
    {
      residuals[0] = _T ( 0 );
      return true;
    }
    
    _T diff[2] = { proj_dpt[0] - proj_pt[0], proj_dpt[1] - proj_pt[1] };
    _T direction;
    
    if( diff[0] != _T(0) )
      direction = atan( diff[1]/diff[0] );
    else
      direction = _T(-M_PI/2);
      
    _T z = _T(eta_direction_) * ( direction + _T(M_PI/2) );
    //residuals[0] = cv_ext::tensorGetPix<float, _T> ( dist_map_tensor_, proj_pt[0], proj_pt[1], z );
    residuals[0] = cv_ext::tensorbilinearInterp<float, _T> ( dist_map_tensor_, proj_pt[0], proj_pt[1], z );

    
    return true;
  }

  static ceres::CostFunction* Create ( const cv_ext::PinholeCameraModel &cam_model,
                                       const ImageTensorPtr &dist_map_tensor_ptr,
                                       const cv::Point3f &model_pt, const cv::Point3f &model_dpt,
                                       const Eigen::Quaterniond &view_q, const Eigen::Vector3d &view_t)
  {
    return ( new ceres::AutoDiffCostFunction<MultiViewsDirectionalChamferResidual, 1, 7 > (
               new MultiViewsDirectionalChamferResidual( cam_model, dist_map_tensor_ptr, model_pt, model_dpt, view_q, view_t ) ) );
  }

  const cv_ext::PinholeCameraModel &cam_model_;
  const ImageTensorPtr dist_map_tensor_ptr_;
  const std::vector<cv::Mat> &dist_map_tensor_;
  double model_pt_[3], model_dpt_[3];
  double eta_direction_;
  double view_[7];
 // Eigen::Affine3d view_;
};


MultiViewsDirectionalChamferMatching::MultiViewsDirectionalChamferMatching ( const cv_ext::PinholeCameraModel& cam_model, 
                                                             const std::vector<ImageTensorPtr> &dist_map_tensor_ptr_vec, 
                                                             const ImageTensorPtr &dist_map_tensor_ptr, 
                                                             const IntegralImageVectorPtr& int_dist_map_tensor_ptr ) : 
  TemplateMatching ( cam_model )
{
  init( dist_map_tensor_ptr_vec, dist_map_tensor_ptr , int_dist_map_tensor_ptr );
}

MultiViewsDirectionalChamferMatching::MultiViewsDirectionalChamferMatching ( const MultiViewsDirectionalChamferMatching & other ) : 
  TemplateMatching( other.cam_model_ )
{
  init( other.dist_map_tensor_ptr_vec_, other.dist_map_tensor_ptr_,  other.int_dist_map_tensor_ptr_ );
  if( other.model_ptr_ )
    this->setTemplateModel( other.model_ptr_ );
}

void MultiViewsDirectionalChamferMatching ::init ( const std::vector<ImageTensorPtr> &dist_map_tensor_ptr_vec, 
                                                  const ImageTensorPtr &dist_map_tensor_ptr, 
                                           const IntegralImageVectorPtr& int_dist_map_tensor_ptr )
{
  dist_map_tensor_ptr_vec_=dist_map_tensor_ptr_vec;
  dist_map_tensor_ptr_ = dist_map_tensor_ptr;
  
  int_dist_map_tensor_ptr_ = int_dist_map_tensor_ptr;
  num_directions_ = dist_map_tensor_ptr_->size();
  eta_direction_ = double(num_directions_)/M_PI;
  
}

double MultiViewsDirectionalChamferMatching::avgDistance( int idx )
{
  if( !model_ptr_ )
    return - 1;

//   int n_pts = 0;
//   double avg_dist = 0;
//   
//   std::vector<cv::Point2f> proj_pts;
//   std::vector<float> normal_directions;
// 
//   if( idx < 0 )
//     model_ptr_->projectRasterPoints( proj_pts, normal_directions );
//   else
//     model_ptr_->projectRasterPoints( idx, proj_pts, normal_directions );
// 
//   std::vector<cv::Mat > &dist_map_tensor = *dist_map_tensor_ptr_;
//   for( int i = 0; i < proj_pts.size(); i++ )
//   {
//     const cv::Point2f &coord = proj_pts.at(i);
//     if( coord.x >= 0)
//     {
//       n_pts++;
//       
//       float direction = normal_directions[i] + M_PI/2;
//       if( direction >= M_PI/2 )
//         direction -= M_PI;
//       direction += M_PI/2;
//               
//       int x = round(coord.x), y = round(coord.y);
//       int z = round(eta_direction_*direction);
//       z %= num_directions_;
//       
//       avg_dist += dist_map_tensor[z].at<float>( y, x );
//     }
//   }
// 
//   if( n_pts )
//     avg_dist /= n_pts;
//   else
//     avg_dist = std::numeric_limits< float >::max();
//   
//   return avg_dist;
}


double MultiViewsDirectionalChamferMatching::optimize()
{
  if( !model_ptr_vec_[0]  || !optimizer_ptr_ )
    return -1;
  
  ceres::Solver::Options options;
  //options.linear_solver_type = ceres::DENSE_SCHUR;
  options.max_num_iterations = max_num_iterations_;
  options.linear_solver_type = ceres::DENSE_NORMAL_CHOLESKY;
  options.minimizer_progress_to_stdout = verbouse_mode_;

  ceres::Solver::Summary summary;
  ceres::Solve ( options, &optimizer_ptr_->problem, &summary );  
  
  return summary.final_cost;
}

void MultiViewsDirectionalChamferMatching::updateOptimizer( int idx )
{
 
  optimizer_ptr_ = boost::shared_ptr< Optimizer > ( new Optimizer () );
  for(int j=0; j<views_.size(); j++)
  {
     if( !model_ptr_vec_[j] ) return;
    std::vector<cv::Point3f> model_pts, model_dpts;

    model_ptr_vec_[j]->getVisiblePoints( model_pts, model_dpts );
    //model_ptr_->getPoints( model_pts, model_dpts );

  //std::cout<<"pts.size: "<<model_pts.size()<<" model_ptr_vec_size: "<<model_ptr_vec_.size()<<std::endl;

    Eigen::Affine3d view_inv=views_[j];
    Eigen::Quaterniond q(view_inv.linear()); q.normalize();
    for ( int i = 0; i < model_pts.size(); ++i )
    {
      
      ceres::CostFunction* cost_function =
        MultiViewsDirectionalChamferResidual::Create ( cam_model_, dist_map_tensor_ptr_vec_[j],
                                               model_pts[i], model_dpts[i], q, view_inv.translation() );

      optimizer_ptr_->problem.AddResidualBlock ( cost_function, new ceres::HuberLoss(1.0), transf_.data() );
    }
  }
  
 /* if( !model_ptr_ ) return;
  std::vector<cv::Point3f> model_pts, model_dpts;
    
  if(model_ptr_->allVisiblePoints())
    model_ptr_->getPoints( model_pts, model_dpts );
  else
  {
    if( idx < 0 )
      model_ptr_->getVisiblePoints( model_pts, model_dpts );
    else
      model_ptr_->getVisiblePoints( idx, model_pts, model_dpts );
  }
  
  optimizer_ptr_ = boost::shared_ptr< Optimizer > ( new Optimizer () );

  for ( int i = 0; i < model_pts.size(); ++i )
  {
    ceres::CostFunction* cost_function =
      MultiViewsDirectionalChamferResidual::Create ( cam_model_, dist_map_tensor_ptr_, model_pts[i], model_dpts[i] );

    optimizer_ptr_->problem.AddResidualBlock ( cost_function, new ceres::HuberLoss(1.0), transf_.data() );
  }*/
}



class MultiViewsCalibrationDirectionalChamferMatching::Optimizer
{
public:
  Optimizer(){};
  ~Optimizer(){};
  ceres::Problem problem;
};

struct MultiViewsCalibrationDirectionalChamferResidual
{
  MultiViewsCalibrationDirectionalChamferResidual ( const cv_ext::PinholeCameraModel &cam_model, 
                               const ImageTensorPtr &dist_map_tensor_ptr,
                               const cv::Point3f &model_pt, const cv::Point3f &model_dpt,
                               const Eigen::Quaterniond &camera_q, const Eigen::Vector3d &camera_t,
                               const Eigen::Quaterniond &model_q, const Eigen::Vector3d &model_t) :
    cam_model_ ( cam_model ),
    dist_map_tensor_ptr_ ( dist_map_tensor_ptr ),
    dist_map_tensor_( *dist_map_tensor_ptr_ )
  {
    model_pt_[0] = model_pt.x;
    model_pt_[1] = model_pt.y;
    model_pt_[2] = model_pt.z;
    
    model_dpt_[0] = model_dpt.x;
    model_dpt_[1] = model_dpt.y;
    model_dpt_[2] = model_dpt.z;
    
    eta_direction_ = double(dist_map_tensor_.size())/M_PI;

    camera_[0]=camera_q.w(); camera_[1]=camera_q.x(); camera_[2]=camera_q.y(); camera_[3]=camera_q.z();
    camera_[4]=camera_t(0); camera_[5]=camera_t(1); camera_[6]=camera_t(2);
    
    model_[0]=model_q.w(); model_[1]=model_q.x(); model_[2]=model_q.y(); model_[3]=model_q.z();
    model_[4]=model_t(0); model_[5]=model_t(1); model_[6]=model_t(2);
    

  }

  template <typename _T>
  bool operator() ( const _T* const offset, _T* residuals ) const
  {

    _T model_pt[3] = { _T ( model_pt_[0] ), _T ( model_pt_[1] ), _T ( model_pt_[2] ) };
    _T model_dpt[3] = { _T ( model_dpt_[0] ), _T ( model_dpt_[1] ), _T ( model_dpt_[2] ) };
    _T proj_pt[2], proj_dpt[2];
    
    _T camera[7]={_T(camera_[0]), _T(camera_[1]), _T(camera_[2]), _T(camera_[3]), _T(camera_[4]), _T(camera_[5]), _T(camera_[6])};
    _T model[7]={_T(model_[0]), _T(model_[1]), _T(model_[2]), _T(model_[3]), _T(model_[4]), _T(model_[5]), _T(model_[6])};
    
    
    Eigen::Quaternion<_T> offset_q; cv_ext::quat2EigenQuat(offset, offset_q); offset_q.normalize();
    Eigen::Matrix<_T,3,1> offset_t; offset_t<<offset[4], offset[5], offset[6];
    Eigen::Quaternion<_T> camera_q; cv_ext::quat2EigenQuat(camera, camera_q);
    Eigen::Matrix<_T,3,1> camera_t; camera_t<<camera[4], camera[5], camera[6];
    Eigen::Quaternion<_T> model_q; cv_ext::quat2EigenQuat(model, model_q);
    Eigen::Matrix<_T,3,1> model_t; model_t<<model[4], model[5], model[6];
    
    Eigen::Transform< _T,3, Eigen::Affine > offset_T, T, camera_T, model_T;
    offset_T.linear()=Eigen::Matrix<_T, 3,3>(offset_q); offset_T.translation()=offset_t;
    camera_T.linear()=Eigen::Matrix<_T, 3,3>(camera_q); camera_T.translation()=camera_t;
    model_T.linear()=Eigen::Matrix<_T, 3,3>(model_q); model_T.translation()=model_t;

    T=(camera_T*offset_T).inverse()*model_T;
    Eigen::Matrix<_T,3,1> transf_p; transf_p<<model_pt[0], model_pt[1], model_pt[2]; transf_p=T*transf_p;
    Eigen::Matrix<_T,3,1> transf_dp; transf_dp<<model_dpt[0], model_dpt[1], model_dpt[2]; transf_dp=T*transf_dp;
    
    
    _T transf_pt[3] = { transf_p(0) ,transf_p(1) ,transf_p(2)  };
    _T transf_dpt[3] = { transf_dp(0) , transf_dp(1) , transf_dp(2)  };
    cam_model_.project(transf_pt, proj_pt);
    cam_model_.project(transf_dpt, proj_dpt);


   if ( proj_pt[0] < _T ( 1 ) ||  proj_pt[1] < _T ( 1 ) ||
        proj_pt[0] > _T ( cam_model_.imgWidth() - 2 ) ||
        proj_pt[1] > _T ( cam_model_.imgHeight() - 2 ) ||
        proj_dpt[0] < _T ( 1 ) ||  proj_dpt[1] < _T ( 1 ) ||
        proj_dpt[0] > _T ( cam_model_.imgWidth() - 2 ) ||
        proj_dpt[1] > _T ( cam_model_.imgHeight() - 2 ) )
    {
      residuals[0] = _T ( 0 );
      return true;
    }
    
    _T diff[2] = { proj_dpt[0] - proj_pt[0], proj_dpt[1] - proj_pt[1] };
    _T direction;
    
    if( diff[0] != _T(0) )
      direction = atan( diff[1]/diff[0] );
    else
      direction = _T(-M_PI/2);
      
    _T z = _T(eta_direction_) * ( direction + _T(M_PI/2) );
    //residuals[0] = cv_ext::tensorGetPix<float, _T> ( dist_map_tensor_, proj_pt[0], proj_pt[1], z );
    residuals[0] = cv_ext::tensorbilinearInterp<float, _T> ( dist_map_tensor_, proj_pt[0], proj_pt[1], z );

    
    return true;
  }

  static ceres::CostFunction* Create ( const cv_ext::PinholeCameraModel &cam_model,
                                       const ImageTensorPtr &dist_map_tensor_ptr,
                                       const cv::Point3f &model_pt, const cv::Point3f &model_dpt,
                                       const Eigen::Quaterniond &camera_q, const Eigen::Vector3d &camera_t,
                                       const Eigen::Quaterniond &model_q, const Eigen::Vector3d &model_t)
  {
    return ( new ceres::AutoDiffCostFunction<MultiViewsCalibrationDirectionalChamferResidual, 1, 7 > (
               new MultiViewsCalibrationDirectionalChamferResidual( cam_model, dist_map_tensor_ptr, model_pt, model_dpt, camera_q, camera_t, model_q, model_t ) ) );
  }

  const cv_ext::PinholeCameraModel &cam_model_;
  const ImageTensorPtr dist_map_tensor_ptr_;
  const std::vector<cv::Mat> &dist_map_tensor_;
  double model_pt_[3], model_dpt_[3];
  double eta_direction_;
  double camera_[7];
  double model_[7];
 // Eigen::Affine3d view_;
};


MultiViewsCalibrationDirectionalChamferMatching::MultiViewsCalibrationDirectionalChamferMatching ( const cv_ext::PinholeCameraModel& cam_model, 
                                                             const std::vector<ImageTensorPtr> &dist_map_tensor_ptr_vec, 
                                                             const ImageTensorPtr &dist_map_tensor_ptr, 
                                                             const IntegralImageVectorPtr& int_dist_map_tensor_ptr ) : 
  TemplateMatching ( cam_model )
{
  init( dist_map_tensor_ptr_vec, dist_map_tensor_ptr , int_dist_map_tensor_ptr );
}

MultiViewsCalibrationDirectionalChamferMatching::MultiViewsCalibrationDirectionalChamferMatching ( const MultiViewsCalibrationDirectionalChamferMatching & other ) : 
  TemplateMatching( other.cam_model_ )
{
  init( other.dist_map_tensor_ptr_vec_, other.dist_map_tensor_ptr_,  other.int_dist_map_tensor_ptr_ );
  if( other.model_ptr_ )
    this->setTemplateModel( other.model_ptr_ );
}

void MultiViewsCalibrationDirectionalChamferMatching ::init ( const std::vector<ImageTensorPtr> &dist_map_tensor_ptr_vec, 
                                                  const ImageTensorPtr &dist_map_tensor_ptr, 
                                           const IntegralImageVectorPtr& int_dist_map_tensor_ptr )
{
  dist_map_tensor_ptr_vec_=dist_map_tensor_ptr_vec;
  dist_map_tensor_ptr_ = dist_map_tensor_ptr;
  
  int_dist_map_tensor_ptr_ = int_dist_map_tensor_ptr;
  num_directions_ = dist_map_tensor_ptr_->size();
  eta_direction_ = double(num_directions_)/M_PI;
  
}

double MultiViewsCalibrationDirectionalChamferMatching::avgDistance( int idx )
{
  if( !model_ptr_ )
    return - 1;

}


double MultiViewsCalibrationDirectionalChamferMatching::optimize()
{
  if( !model_ptr_vec_[0]  || !optimizer_ptr_ )
    return -1;
  
  ceres::Solver::Options options;
  //options.linear_solver_type = ceres::DENSE_SCHUR;
  options.max_num_iterations = max_num_iterations_;
  options.linear_solver_type = ceres::DENSE_NORMAL_CHOLESKY;
  options.minimizer_progress_to_stdout = verbouse_mode_;

  ceres::Solver::Summary summary;
  ceres::Solve ( options, &optimizer_ptr_->problem, &summary );  
  
  return summary.final_cost;
}

void MultiViewsCalibrationDirectionalChamferMatching::updateOptimizer( int idx )
{
 
  optimizer_ptr_ = boost::shared_ptr< Optimizer > ( new Optimizer () );
  for(int j=0; j<camera_poses_.size(); j++)
  {
     if( !model_ptr_vec_[j] ) return;
    std::vector<cv::Point3f> model_pts, model_dpts;

    model_ptr_vec_[j]->getVisiblePoints( model_pts, model_dpts );
    //model_ptr_->getPoints( model_pts, model_dpts );

  //std::cout<<"pts.size: "<<model_pts.size()<<" model_ptr_vec_size: "<<model_ptr_vec_.size()<<std::endl;

    Eigen::Affine3d camera_T=camera_poses_[j];
    Eigen::Quaterniond camera_q(camera_T.linear()); camera_q.normalize();
    Eigen::Quaterniond model_q(model_pose_.linear()); model_q.normalize();
    for ( int i = 0; i < model_pts.size(); ++i )
    {
      
      ceres::CostFunction* cost_function =
        MultiViewsCalibrationDirectionalChamferResidual::Create ( cam_model_, dist_map_tensor_ptr_vec_[j],
                                               model_pts[i], model_dpts[i], camera_q, camera_T.translation(),model_q, model_pose_.translation() );

      optimizer_ptr_->problem.AddResidualBlock ( cost_function, new ceres::HuberLoss(1.0), transf_.data() );
    }
  }
  
}



