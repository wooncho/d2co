#include "template_matching.h"

void extractEdgesPyr( const cv::Mat &src_img, std::vector< cv::Vec4f > &segments )
{

  segments.clear();
  cv::Mat scaled_img;
  for (double scale = 1; scale<4 ; scale+=.5)
  { 
    cv::resize(src_img, scaled_img, cv::Size(0,0), 1/scale, 1/scale);
    double *dbl_img = new double[scaled_img.cols*scaled_img.rows];

    for ( int y = 0; y < scaled_img.rows; y++ )
      for ( int x=0; x < scaled_img.cols; x++ )
        dbl_img[ x + y * scaled_img.cols ] = ( double ) scaled_img.at<uchar> ( y,x );

    int n_segments;

   //  double *dbl_segments = lsd(&n_segments, dbl_img, scaled_img.cols, scaled_img.rows);

    double *dbl_segments = LineSegmentDetection ( &n_segments, dbl_img, scaled_img.cols,scaled_img.rows,
                                                  1,//1.0 /*scale*/,
                                                  1,//1.0 /*sigma_scale*/,
                                                 .7,//0.95 /* quant*/, //original 0.5
                                                  22.5,//22.5 /* ang_th */,
                                                  0.0 /*log_eps */,
                                                  0.5 /* density_th */,
                                                  1024,//1024 /* n_bins */,
                                                  NULL, NULL, NULL );

    delete [] dbl_img;

    for ( int i = 0; i < n_segments; i++ )
    {
      cv::Vec4f s;
      s[0] = dbl_segments[7*i]*scale;
      s[1] = dbl_segments[7*i + 1]*scale;
      s[2] = dbl_segments[7*i + 2]*scale;
      s[3] = dbl_segments[7*i + 3]*scale;
      segments.push_back(s);
    }
    delete dbl_segments;
  }
}

void extractEdges( const cv::Mat &src_img, std::vector< cv::Vec4f > &segments )
{
  double *dbl_img = new double[src_img.cols*src_img.rows];

  for ( int y = 0; y < src_img.rows; y++ )
    for ( int x=0; x < src_img.cols; x++ )
      dbl_img[ x + y * src_img.cols ] = ( double ) src_img.at<uchar> ( y,x );

  int n_segments;

   //double *dbl_segments = lsd(&n_segments, dbl_img, src_img.cols, src_img.rows);

  double *dbl_segments = LineSegmentDetection ( &n_segments, dbl_img, src_img.cols, src_img.rows,
                                                1,//1.0 /*scale*/,
                                                1,//1.0 /*sigma_scale*/,
                                               .5,//0.95 /* quant*/, //original 0.5
                                                22.5,//22.5 /* ang_th */,
                                                0.0 /*log_eps */,
                                                0.5 /* density_th */,
                                                1024,//1024 /* n_bins */,
                                                NULL, NULL, NULL );

  delete [] dbl_img;
  segments.resize( n_segments );

  for ( int i = 0; i < n_segments; i++ )
  {
    cv::Vec4f &s = segments[i];
    s[0] = dbl_segments[7*i];
    s[1] = dbl_segments[7*i + 1];
    s[2] = dbl_segments[7*i + 2];
    s[3] = dbl_segments[7*i + 3];
  }
  delete dbl_segments;
}

void computeEdgesNormalsDirections( const std::vector< cv::Vec4f > &segments, 
                                    std::vector<float> &normals,
                                    std::vector<cv::Point2f> &centers )
{
  double m, xc, yc;
  int seg_size = segments.size();
  centers.resize(seg_size);
  normals.resize(seg_size);
  
  for(int i = 0; i < seg_size; i++)
  {
    
    const float &x1 = segments[i][0], &y1 = segments[i][1],
                &x2 = segments[i][2], &y2 = segments[i][3];
    
    // center of the line
    centers[i] = cv::Point2f((x1+x2)/2, (y1+y2)/2);

    // Normal direction
    double cos_ang = x2-x1, sin_ang = y2-y1;
    if( sin_ang )
      m = -atan(cos_ang/sin_ang);
    else
      m = -M_PI/2;

    normals[i] = m;
  }
}

TemplateMatching :: TemplateMatching ( const cv_ext::PinholeCameraModel &cam_model ) : 
  cam_model_(cam_model),
  max_num_iterations_(50),
  verbouse_mode_(false)
{
  transf_ << 1.0, 0, 0, 0, 0, 0, 0, 0;
}

void TemplateMatching::setTemplateModel ( const RasterObjectModelPtr& model_ptr )
{
  model_ptr_ = model_ptr;
  updateOptimizer(-1);
}

void TemplateMatching::setTemplateModelVec (  const std::vector<RasterObjectModel3DPtr>& model_ptr_vec )
{
  model_ptr_vec_ = model_ptr_vec;
  updateOptimizer(-1);
}

double TemplateMatching::performOptimization( double r_quat[4], double t_vec[3] )
{
  setPos( r_quat, t_vec );
  if(!model_ptr_->allVisiblePoints())
  {
    model_ptr_->setModelView(transf_.data(), transf_.block<3,1>(4,0).data()); 
    updateOptimizer(-1);
  }
  double res = optimize();
  getPos( r_quat, t_vec );
  
  return res;
}

double TemplateMatching::performOptimization ( Eigen::Quaterniond& r_quat, 
                                               Eigen::Vector3d& t_vec )
{
  setPos( r_quat, t_vec );
  if(!model_ptr_->allVisiblePoints())
  {
    model_ptr_->setModelView(transf_.data(), transf_.block<3,1>(4,0).data()); 
    updateOptimizer(-1);
  }
  double res = optimize();
  getPos( r_quat, t_vec );
  
  return res;
}

double TemplateMatching::performOptimization ( Eigen::Quaterniond& r_quat, 
                                               Eigen::Vector3d& t_vec, 
                                               const std::vector<Eigen::Affine3d> &views )
{
  views_.clear();
  views_=views;
  for (int i=0; i<views_.size(); i++)
  {
    Eigen::Matrix3d R(r_quat);
    Eigen::Affine3d RT; RT.linear()=R; RT.translation()=t_vec;
    RT=views_[i]*RT;
    Eigen::Quaterniond r(RT.linear());
    Eigen::Vector3d t=RT.translation();
    setPos( r, t );

      model_ptr_vec_[i]->setModelView(transf_.data(), transf_.block<3,1>(4,0).data());

  }
  setPos( r_quat, t_vec );
  updateOptimizer(-1);
  double res = optimize();
  getPos( r_quat, t_vec );
  Eigen::Matrix3d m(r_quat);
  return res;
}

double TemplateMatching::performOptimization ( Eigen::Quaterniond& r_quat, 
                                               Eigen::Vector3d& t_vec, 
                                               const std::vector<Eigen::Affine3d> &camera_poses,
                                               const Eigen::Affine3d &model_pose )
{
  camera_poses_.clear();
  camera_poses_=camera_poses;
  model_pose_=model_pose;
  for (int i=0; i<camera_poses_.size(); i++)
  {
    Eigen::Matrix3d R(r_quat);
    Eigen::Affine3d RT; RT.linear()=R; RT.translation()=t_vec;
    Eigen::Affine3d T=(camera_poses_[i]*RT).inverse()*model_pose;
    Eigen::Quaterniond r(T.linear());
    Eigen::Vector3d t=T.translation();
    setPos( r, t );

      model_ptr_vec_[i]->setModelView(transf_.data(), transf_.block<3,1>(4,0).data());

  }
  setPos( r_quat, t_vec );
  updateOptimizer(-1);
  double res = optimize();
  getPos( r_quat, t_vec );
  Eigen::Matrix3d m(r_quat);
  std::cout<<"res= "<<res<<" t_vec= "<<t_vec.transpose()<<" \nR: "<<m<<std::endl;
  return res;
}


double TemplateMatching::performOptimization( cv::Mat_<double> &r_vec, 
                                              cv::Mat_<double> &t_vec )
{
  setPos( r_vec, t_vec );
  if(!model_ptr_->allVisiblePoints())
  {
    model_ptr_->setModelView(transf_.data(), transf_.block<3,1>(4,0).data()); 
    updateOptimizer(-1);
  }
  double res = optimize();
  getPos( r_vec, t_vec );
  
  return res;
}

double TemplateMatching::performOptimization ( int idx, double r_quat[4], double t_vec[3] )
{
  Eigen::Quaterniond quat;
  Eigen::Vector3d t;
  model_ptr_->modelView(idx, quat, t);
  setPos ( quat, t );
  
  if(!model_ptr_->allVisiblePoints())
    updateOptimizer(idx);
  
  double res = optimize();
  getPos (r_quat, t_vec );
  
  return res;
}

double TemplateMatching::performOptimization ( int idx, Eigen::Quaterniond& r_quat, 
                                               Eigen::Vector3d& t_vec  )
{
  Eigen::Quaterniond quat;
  Eigen::Vector3d t;
  model_ptr_->modelView(idx, quat, t);
  setPos ( quat, t );
  
  if(!model_ptr_->allVisiblePoints())
    updateOptimizer(idx);
  
  double res = optimize();
  getPos (r_quat, t_vec );
  
  return res;
}

double TemplateMatching::performOptimization ( int idx, cv::Mat_< double >& r_vec, 
                                               cv::Mat_< double >& t_vec )
{
  Eigen::Quaterniond quat;
  Eigen::Vector3d t;
  model_ptr_->modelView(idx, quat, t);
  setPos ( quat, t );
  
  if(!model_ptr_->allVisiblePoints())
    updateOptimizer(idx);
  
  double res = optimize();
  getPos (r_vec, t_vec );
  
  return res;
}

double TemplateMatching::getAvgDistance( const double r_quat[4], const double t_vec[3] )
{
  setPos( r_quat, t_vec );
  if(!model_ptr_->allVisiblePoints())
    model_ptr_->setModelView(transf_.data(), transf_.block<3,1>(4,0).data()); 
    
  return avgDistance(-1);
}

double TemplateMatching::getAvgDistance( const Eigen::Quaterniond& r_quat, 
                                         const Eigen::Vector3d& t_vec )
{
  setPos( r_quat, t_vec );
  if(!model_ptr_->allVisiblePoints())
    model_ptr_->setModelView(transf_.data(), transf_.block<3,1>(4,0).data()); 
  
  return avgDistance(-1);
}


double TemplateMatching::getAvgDistance( const cv::Mat_< double >& r_vec, 
                                         const cv::Mat_< double >& t_vec )
{
  setPos( r_vec, t_vec );
  if(!model_ptr_->allVisiblePoints())
    model_ptr_->setModelView(transf_.data(), transf_.block<3,1>(4,0).data()); 
  
  return avgDistance(-1);
}

double TemplateMatching::getAvgDistance( int idx )
{
  Eigen::Quaterniond quat;
  Eigen::Vector3d t;
  model_ptr_->modelView(idx, quat, t);
  setPos ( quat, t );
  
  return avgDistance(idx);
}

void TemplateMatching::setPos ( const double r_quat[4], const double t_vec[3] )
{
  transf_(0,0) = r_quat[0];
  transf_(1,0) = r_quat[1];
  transf_(2,0) = r_quat[2];
  transf_(3,0) = r_quat[3];

  transf_(4,0) = t_vec[0];
  transf_(5,0) = t_vec[1];
  transf_(6,0) = t_vec[2];
}

void TemplateMatching::setPos ( const Eigen::Quaterniond& r_quat, 
                                const Eigen::Vector3d& t_vec )
{
  cv_ext::eigenQuat2Quat( r_quat, transf_.data() );
  transf_(4,0) = t_vec[0];
  transf_(5,0) = t_vec[1];
  transf_(6,0) = t_vec[2];
}

void TemplateMatching::setPos ( const cv::Mat_< double >& r_vec, 
                                const cv::Mat_< double >& t_vec )
{
  double angle_axis[3] = { r_vec ( 0,0 ) , r_vec ( 1,0 ) , r_vec ( 2,0 ) };
  ceres::AngleAxisToQuaternion<double> ( angle_axis, transf_.data() );

  transf_(4,0) = t_vec ( 0,0 );
  transf_(5,0) = t_vec ( 1,0 );
  transf_(6,0) = t_vec ( 2,0 );
}

void TemplateMatching::getPos ( double r_quat[4], double t_vec[3] ) const
{
  r_quat[0] = transf_(0,0);
  r_quat[1] = transf_(1,0);
  r_quat[2] = transf_(2,0);
  r_quat[3] = transf_(3,0);

  t_vec[0] = transf_(4,0);
  t_vec[1] = transf_(5,0);
  t_vec[2] = transf_(6,0);
}

void TemplateMatching::getPos ( Eigen::Quaterniond& r_quat, 
                                Eigen::Vector3d& t_vec ) const
{
  cv_ext::quat2EigenQuat(transf_.data(), r_quat );
  t_vec( 0,0 ) = transf_(4,0);
  t_vec( 1,0 ) = transf_(5,0);
  t_vec( 2,0 ) = transf_(6,0);
}


void TemplateMatching::getPos ( cv::Mat_< double >& r_vec, 
                                cv::Mat_< double >& t_vec ) const
{
  double angle_axis[3];
  ceres::QuaternionToAngleAxis<double> ( transf_.data(), angle_axis );
  
  r_vec ( 0,0 ) = angle_axis[0];
  r_vec ( 1,0 ) = angle_axis[1];
  r_vec ( 2,0 ) = angle_axis[2];

  t_vec ( 0,0 ) = transf_(4,0);
  t_vec ( 1,0 ) = transf_(5,0);
  t_vec ( 2,0 ) = transf_(6,0);
}
