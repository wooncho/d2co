#include <iostream>
#include <iterator>
#include <boost/concept_check.hpp>
#include <boost/thread/locks.hpp>
#include <sstream>
#include <algorithm>

#include "raster_object_model.h"

RasterObjectModel::RasterObjectModel() :
  unit_meas_( MILLIMETER ),
  centroid_orig_offset_(USER_DEFINED_ORIG_OFFSET),
  orig_offset_(0.0f, 0.0f, 0.0f),
  step_(0.001),
  epsilon_(1e-4),
  min_seg_len_( std::numeric_limits< double >::min() )
{ 
  rq_view_.setIdentity();
  t_view_.setZero();
}

void RasterObjectModel::setUnitOfMeasure( UoM val ) 
{ 
//   boost::lock_guard<boost::mutex> lock(mutex_);
  unit_meas_ = val; 
};

void RasterObjectModel::setCentroidOrigOffset()
{
  centroid_orig_offset_ = CENTROID_ORIG_OFFSET;
}

void RasterObjectModel::setBBCenterOrigOffset()
{
  centroid_orig_offset_ = BOUNDING_BOX_CENTER_ORIG_OFFSET;
}

void RasterObjectModel::setOrigOffset( cv::Point3f offset )
{ 
//   boost::lock_guard<boost::mutex> lock(mutex_);
  orig_offset_ = offset;
  centroid_orig_offset_ = USER_DEFINED_ORIG_OFFSET;
}

void RasterObjectModel::setStepMeters( double s )
{ 
//   boost::lock_guard<boost::mutex> lock(mutex_);
  step_ = s; 
};

void RasterObjectModel::setMinSegmentsLen ( double len )
{
//   boost::lock_guard<boost::mutex> lock(mutex_);
  min_seg_len_ = len;
}

void RasterObjectModel::setCamModel ( const cv_ext::PinholeCameraModel& cam_model )
{
//   boost::lock_guard<boost::mutex> lock(mutex_);
  cam_model_ = cam_model;
}

void RasterObjectModel::setModelView ( const double r_quat[4], const double t_vec[3] )
{
//   boost::lock_guard<boost::mutex> lock(mutex_);
  
  cv_ext::quat2EigenQuat(r_quat, rq_view_ );
  t_view_ = Eigen::Map<const Eigen::Vector3d>(t_vec);
  
  updateRaster();
}

void RasterObjectModel::setModelView ( const Eigen::Quaterniond& r_quat, const Eigen::Vector3d& t_vec )
{
//   boost::lock_guard<boost::mutex> lock(mutex_);
  
  rq_view_ = r_quat;
  t_view_ = t_vec;
  
  updateRaster();
}

void RasterObjectModel::setModelView ( const cv::Mat_< double >& r_vec, const cv::Mat_< double >& t_vec )
{
//   boost::lock_guard<boost::mutex> lock(mutex_);
  
  double r_quat[4];
  ceres::AngleAxisToQuaternion( (const double*)(r_vec.data), r_quat) ;
  rq_view_ = Eigen::Quaterniond( r_quat[0], r_quat[1], r_quat[2], r_quat[3] );
  t_view_ = Eigen::Map< const Eigen::Vector3d>( (const double*)(t_vec.data) );
  
  updateRaster();
}

void RasterObjectModel::storeModelView()
{
  precomputed_rq_view_.push_back(rq_view_);
  precomputed_t_view_.push_back(t_view_);
  storeModel();
}

void RasterObjectModel::setModelView ( int idx )
{
  rq_view_ = precomputed_rq_view_[idx];
  t_view_ = precomputed_t_view_[idx];
  retreiveModel( idx );
}

void RasterObjectModel::modelView ( double r_quat[4], double t_vec[3] ) const
{
  r_quat[0] = rq_view_.w();
  r_quat[1] = rq_view_.x();
  r_quat[2] = rq_view_.y();
  r_quat[3] = rq_view_.z();

  t_vec[0] = t_view_(0);
  t_vec[1] = t_view_(1);
  t_vec[2] = t_view_(2);
}

void RasterObjectModel::modelView ( Eigen::Quaterniond& r_quat, Eigen::Vector3d& t_vec ) const
{
  r_quat = rq_view_;
  t_vec = t_view_;
}

void RasterObjectModel::modelView ( int idx, Eigen::Quaterniond& r_quat, Eigen::Vector3d& t_vec ) const
{
  r_quat = precomputed_rq_view_[idx];
  t_vec = precomputed_t_view_[idx];
}

void RasterObjectModel::modelView ( cv::Mat_< double >& r_vec, cv::Mat_< double >& t_vec ) const
{
  r_vec = cv::Mat_< double >(3,1);
  t_vec = cv::Mat_< double >(3,1);
  
  double r_quat[4];
  r_quat[0] = rq_view_.w();
  r_quat[1] = rq_view_.x();
  r_quat[2] = rq_view_.y();
  r_quat[3] = rq_view_.z();

  ceres::QuaternionToAngleAxis<double> ( r_quat, ( double* )r_vec.data );
  
  t_vec(0) = t_view_(0);
  t_vec(1) = t_view_(1);
  t_vec(2) = t_view_(2);
}


void RasterObjectModel::projectRasterPoints( std::vector<cv::Point2f> &proj_pts ) const
{
  projectRasterPoints( rq_view_, t_view_, proj_pts );
}
                                             
void RasterObjectModel::projectRasterPoints( const double r_quat[4], const double t_vec[3], 
                                             std::vector<cv::Point2f> &proj_pts )  const
{
//   boost::lock_guard<boost::mutex> lock(mutex_);

  std::vector<cv::Point3f> pts;
  if( allVisiblePoints() )
    getPoints( pts );
  else
    getVisiblePoints( pts );
    
  cv_ext::PinholeSceneProjector proj( cam_model_ );
  proj.setTransformation( r_quat, t_vec );
  proj.projectPoints( pts, proj_pts );
}

void RasterObjectModel::projectRasterPoints( const Eigen::Quaterniond &r_quat, const Eigen::Vector3d &t_vec, 
                                             std::vector<cv::Point2f> &proj_pts )  const
{
//   boost::lock_guard<boost::mutex> lock(mutex_);

  std::vector<cv::Point3f> pts;
  if( allVisiblePoints() )
    getPoints( pts );
  else
    getVisiblePoints( pts );
    
  cv_ext::PinholeSceneProjector proj( cam_model_ );
  proj.setTransformation( r_quat, t_vec );
  proj.projectPoints( pts, proj_pts );
}

void RasterObjectModel::projectRasterPoints( const cv::Mat_<double> &r_vec, const cv::Mat_<double> &t_vec,
                                             std::vector<cv::Point2f> &proj_pts )  const
{
//   boost::lock_guard<boost::mutex> lock(mutex_);

  std::vector<cv::Point3f> pts;
  if( allVisiblePoints() )
    getPoints( pts );
  else
    getVisiblePoints( pts );
    
  cv_ext::PinholeSceneProjector proj( cam_model_ );
  proj.setTransformation( r_vec, t_vec );
  proj.projectPoints( pts, proj_pts );
}

void RasterObjectModel::projectRasterPoints ( int idx, std::vector< cv::Point2f >& proj_pts ) const
{
  projectRasterPoints ( idx, precomputed_rq_view_[idx], precomputed_t_view_[idx], proj_pts );
}

void RasterObjectModel::projectRasterPoints ( int idx, const double r_quat[4], const double t_vec[3],
                                              std::vector< cv::Point2f >& proj_pts ) const
{
  std::vector<cv::Point3f> pts;
  getVisiblePoints( idx, pts );
  cv_ext::PinholeSceneProjector proj( cam_model_ );
  proj.setTransformation( r_quat, t_vec );
  proj.projectPoints( pts, proj_pts );
}

void RasterObjectModel::projectRasterPoints ( int idx, const Eigen::Quaterniond &r_quat, const Eigen::Vector3d &t_vec,
                                              std::vector< cv::Point2f >& proj_pts ) const
{
  std::vector<cv::Point3f> pts;
  getVisiblePoints( idx, pts );
  cv_ext::PinholeSceneProjector proj( cam_model_ );
  proj.setTransformation( r_quat, t_vec );
  proj.projectPoints( pts, proj_pts );
}

void RasterObjectModel::projectRasterPoints ( int idx, const cv::Mat_<double> &r_vec, const cv::Mat_<double> &t_vec,
                                              std::vector< cv::Point2f >& proj_pts ) const
{
  std::vector<cv::Point3f> pts;
  getVisiblePoints( idx, pts );
  cv_ext::PinholeSceneProjector proj( cam_model_ );
  proj.setTransformation( r_vec, t_vec );
  proj.projectPoints( pts, proj_pts );
}
 
void RasterObjectModel::projectRasterPoints( std::vector<cv::Point2f> &proj_pts,
                                             std::vector<float> &normal_directions )  const
{  
  projectRasterPoints( rq_view_, t_view_, proj_pts, normal_directions );
}


void RasterObjectModel::projectRasterPoints( const double r_quat[4], const double t_vec[3],
                                             std::vector<cv::Point2f> &proj_pts,
                                             std::vector<float> &normal_directions )  const
{  
  std::vector<cv::Point3f> pts, d_pts;
  std::vector<cv::Point2f> d_proj_pts;
  if( allVisiblePoints() )
    getPoints( pts, d_pts );
  else
    getVisiblePoints( pts, d_pts );
  
  cv_ext::PinholeSceneProjector proj( cam_model_ );

  proj.setTransformation( r_quat, t_vec );
  proj.projectPoints( pts, proj_pts );
  proj.projectPoints( d_pts, d_proj_pts );
  
  int pts_size = pts.size();
  normal_directions.resize(pts_size);
  
  for( int i = 0; i < pts_size; i++)
  {
    // Check if the point has been correclty projected inside the image
    if( proj_pts[i].x != -1 && d_proj_pts[i].x != -1)
    {
      cv::Point2f diff(d_proj_pts[i] - proj_pts[i]);
      if( diff.y )
        normal_directions[i] = -atan( diff.x/diff.y );
      else
        normal_directions[i] = -M_PI/2;
    }
    else
    {
      proj_pts[i] = cv::Point2f(-1,-1);
      normal_directions[i] = std::numeric_limits< float >::max();
    }
  }
}

void RasterObjectModel::projectRasterPoints( const Eigen::Quaterniond &r_quat, const Eigen::Vector3d &t_vec,
                                             std::vector<cv::Point2f> &proj_pts,
                                             std::vector<float> &normal_directions )  const
{  
  std::vector<cv::Point3f> pts, d_pts;
  std::vector<cv::Point2f> d_proj_pts;
  if( allVisiblePoints() )
    getPoints( pts, d_pts );
  else
    getVisiblePoints( pts, d_pts );
  
  cv_ext::PinholeSceneProjector proj( cam_model_ );

  proj.setTransformation( r_quat, t_vec );
  proj.projectPoints( pts, proj_pts );
  proj.projectPoints( d_pts, d_proj_pts );
  
  int pts_size = pts.size();
  normal_directions.resize(pts_size);
  
  for( int i = 0; i < pts_size; i++)
  {
    // Check if the point has been correclty projected inside the image
    if( proj_pts[i].x != -1 && d_proj_pts[i].x != -1)
    {
      cv::Point2f diff(d_proj_pts[i] - proj_pts[i]);
      if( diff.y )
        normal_directions[i] = -atan( diff.x/diff.y );
      else
        normal_directions[i] = -M_PI/2;
    }
    else
    {
      proj_pts[i] = cv::Point2f(-1,-1);
      normal_directions[i] = std::numeric_limits< float >::max();
    }
  }
}

void RasterObjectModel::projectRasterPoints( const cv::Mat_<double> &r_vec, const cv::Mat_<double> &t_vec,
                                             std::vector<cv::Point2f> &proj_pts,
                                             std::vector<float> &normal_directions )  const
{  
  std::vector<cv::Point3f> pts, d_pts;
  std::vector<cv::Point2f> d_proj_pts;
  if( allVisiblePoints() )
    getPoints( pts, d_pts );
  else
    getVisiblePoints( pts, d_pts );
  
  cv_ext::PinholeSceneProjector proj( cam_model_ );

  proj.setTransformation( r_vec, t_vec );
  proj.projectPoints( pts, proj_pts );
  proj.projectPoints( d_pts, d_proj_pts );
  
  int pts_size = pts.size();
  normal_directions.resize(pts_size);
  
  for( int i = 0; i < pts_size; i++)
  {
    // Check if the point has been correclty projected inside the image
    if( proj_pts[i].x != -1 && d_proj_pts[i].x != -1)
    {
      cv::Point2f diff(d_proj_pts[i] - proj_pts[i]);
      if( diff.y )
        normal_directions[i] = -atan( diff.x/diff.y );
      else
        normal_directions[i] = -M_PI/2;
    }
    else
    {
      proj_pts[i] = cv::Point2f(-1,-1);
      normal_directions[i] = std::numeric_limits< float >::max();
    }
  }
}


void RasterObjectModel::projectRasterPoints ( int idx, std::vector< cv::Point2f >& proj_pts, 
                                              std::vector< float >& normal_directions ) const
{
  projectRasterPoints( idx, precomputed_rq_view_[idx], precomputed_t_view_[idx], 
                       proj_pts, normal_directions );
}

void RasterObjectModel::projectRasterPoints ( int idx, const double r_quat[4], const double t_vec[3],
                                              std::vector< cv::Point2f >& proj_pts, 
                                              std::vector< float >& normal_directions ) const
{
  std::vector<cv::Point3f> pts, d_pts;
  std::vector<cv::Point2f> d_proj_pts;
  
  cv_ext::PinholeSceneProjector proj( cam_model_ );

  getVisiblePoints( idx, pts, d_pts );
  
  proj.setTransformation( r_quat, t_vec );
  proj.projectPoints( pts, proj_pts );
  proj.projectPoints( d_pts, d_proj_pts );
  
  int pts_size = pts.size();
  normal_directions.resize(pts_size);
  
  for( int i = 0; i < pts_size; i++)
  {
    // Check if the point has been correclty projected inside the image
    if( proj_pts[i].x != -1 && d_proj_pts[i].x != -1)
    {
      cv::Point2f diff(d_proj_pts[i] - proj_pts[i]);
      if( diff.y )
        normal_directions[i] = -atan( diff.x/diff.y );
      else
        normal_directions[i] = -M_PI/2;
    }
    else
    {
      proj_pts[i] = cv::Point2f(-1,-1);
      normal_directions[i] = std::numeric_limits< float >::max();
    }
  }
}

void RasterObjectModel::projectRasterPoints ( int idx, const Eigen::Quaterniond &r_quat, const Eigen::Vector3d &t_vec,
                                              std::vector< cv::Point2f >& proj_pts, 
                                              std::vector< float >& normal_directions ) const
{
  std::vector<cv::Point3f> pts, d_pts;
  std::vector<cv::Point2f> d_proj_pts;
  
  cv_ext::PinholeSceneProjector proj( cam_model_ );

  getVisiblePoints( idx, pts, d_pts );
  
  proj.setTransformation( r_quat, t_vec );
  proj.projectPoints( pts, proj_pts );
  proj.projectPoints( d_pts, d_proj_pts );
  
  int pts_size = pts.size();
  normal_directions.resize(pts_size);
  
  for( int i = 0; i < pts_size; i++)
  {
    // Check if the point has been correclty projected inside the image
    if( proj_pts[i].x != -1 && d_proj_pts[i].x != -1)
    {
      cv::Point2f diff(d_proj_pts[i] - proj_pts[i]);
      if( diff.y )
        normal_directions[i] = -atan( diff.x/diff.y );
      else
        normal_directions[i] = -M_PI/2;
    }
    else
    {
      proj_pts[i] = cv::Point2f(-1,-1);
      normal_directions[i] = std::numeric_limits< float >::max();
    }
  }
}

void RasterObjectModel::projectRasterPoints ( int idx, const cv::Mat_<double> &r_vec, const cv::Mat_<double> &t_vec,
                                              std::vector< cv::Point2f >& proj_pts, 
                                              std::vector< float >& normal_directions ) const
{
  std::vector<cv::Point3f> pts, d_pts;
  std::vector<cv::Point2f> d_proj_pts;
  
  cv_ext::PinholeSceneProjector proj( cam_model_ );

  getVisiblePoints( idx, pts, d_pts );
  
  proj.setTransformation( r_vec, t_vec );
  proj.projectPoints( pts, proj_pts );
  proj.projectPoints( d_pts, d_proj_pts );
  
  int pts_size = pts.size();
  normal_directions.resize(pts_size);
  
  for( int i = 0; i < pts_size; i++)
  {
    // Check if the point has been correclty projected inside the image
    if( proj_pts[i].x != -1 && d_proj_pts[i].x != -1)
    {
      cv::Point2f diff(d_proj_pts[i] - proj_pts[i]);
      if( diff.y )
        normal_directions[i] = -atan( diff.x/diff.y );
      else
        normal_directions[i] = -M_PI/2;
    }
    else
    {
      proj_pts[i] = cv::Point2f(-1,-1);
      normal_directions[i] = std::numeric_limits< float >::max();
    }
  }
}


void RasterObjectModel::projectRasterSegments ( std::vector< cv::Vec4f >& proj_segs ) const
{
//   boost::lock_guard<boost::mutex> lock(mutex_);
  
  std::vector<cv::Vec6f> segs;
  if( allVisiblePoints() )
    getSegments( segs );
  else
    getVisibleSegments( segs );
  
  cv_ext::PinholeSceneProjector proj( cam_model_ );
  proj.setTransformation( rq_view_, t_view_ );
  proj.projectSegments( segs, proj_segs );
}

void RasterObjectModel::projectRasterSegments ( std::vector< cv::Vec4f >& proj_segs, 
                                                std::vector< float >& normal_directions ) const
{
//   boost::lock_guard<boost::mutex> lock(mutex_);
  
  std::vector<cv::Vec6f> segs;
  std::vector<cv::Point3f> d_segs;
  std::vector<cv::Point2f> d_proj_segs;
  
  if( allVisiblePoints() )
    getSegments( segs, d_segs );
  else
    getVisibleSegments( segs, d_segs );
    
  cv_ext::PinholeSceneProjector proj( cam_model_ );
  proj.setTransformation( rq_view_, t_view_ );
  
  proj.projectSegments( segs, proj_segs );
  proj.projectPoints( d_segs, d_proj_segs );

  int seg_size = segs.size();
  normal_directions.resize(seg_size);
  
  for( int i = 0; i < seg_size; i++)
  {
    cv::Vec4f &raster_seg = proj_segs[i];
    // Check if the point has been correclty projected inside the image
    if( raster_seg[0] != -1 && d_proj_segs[i].x != -1)
    {
      cv::Point2f p0( raster_seg[0], raster_seg[1] );
      cv::Point2f diff(d_proj_segs[i] - p0);
      if( diff.y )
        normal_directions[i] = -atan( diff.x/diff.y );
      else
        normal_directions[i] = -M_PI/2;
    }
    else
    {
      proj_segs[i] = cv::Vec4f(-1,-1,-1,-1);
      normal_directions[i] = std::numeric_limits< float >::max();
    }
  }  
}

void RasterObjectModel::loadPrecomputedModelsViews ( std::string base_name )
{
  precomputed_rq_view_.clear();
  precomputed_t_view_.clear();
 
  std::stringstream sname;
  sname<<base_name<<"_model_views.yml";
  cv::FileStorage fs(sname.str().data(), cv::FileStorage::READ);
  
  cv::Mat model_views;
  fs["model_views"] >> model_views;
  
  for( int i = 0; i< model_views.rows; i++ )
  {
    Eigen::Quaterniond quat(model_views.at<double>(i,0),
                            model_views.at<double>(i,1),
                            model_views.at<double>(i,2),
                            model_views.at<double>(i,3));
    Eigen::Vector3d t(model_views.at<double>(i,4),
                      model_views.at<double>(i,5),
                      model_views.at<double>(i,6));
    
    precomputed_rq_view_.push_back(quat);
    precomputed_t_view_.push_back(t);
  }
  
  loadPrecomputedModels(fs);
  
  fs.release();
}

void RasterObjectModel::savePrecomputedModelsViews ( std::string base_name ) const
{
  cv::Mat model_views(precomputed_rq_view_.size(), 7, cv::DataType<double>::type );
  for( int i = 0; i < precomputed_rq_view_.size(); i++ )
  {
    model_views.at<double>(i,0) = precomputed_rq_view_[i].w();
    model_views.at<double>(i,1) = precomputed_rq_view_[i].x();
    model_views.at<double>(i,2) = precomputed_rq_view_[i].y();
    model_views.at<double>(i,3) = precomputed_rq_view_[i].z();
    model_views.at<double>(i,4) = precomputed_t_view_[i](0);
    model_views.at<double>(i,5) = precomputed_t_view_[i](1);
    model_views.at<double>(i,6) = precomputed_t_view_[i](2);
  }
  
  std::stringstream sname;
  sname<<base_name<<"_model_views.yml";
  cv::FileStorage fs(sname.str().data(), cv::FileStorage::WRITE);
  
  fs << "model_views" << model_views;
  
  savePrecomputedModels(fs);
  
  fs.release();
}
