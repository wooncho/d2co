#include <iostream>
#include <fstream>
#include "cv_ext/cv_ext.h"
#include "raster_object_model3D.h"
#include "chamfer_matching.h"
#include "direct_matching.h"


static const double OUTER_POINT_SCORE = 0.6;


double evaluateScore ( cv_ext::ImageStatisticsPtr &img_stats_p,
                       std::vector<cv::Point2f> &raster_pts,
                       const std::vector<float> &normal_directions )
{
  boost::shared_ptr< std::vector<float> > g_dir_p =  img_stats_p->getGradientDirections ( raster_pts );
  boost::shared_ptr< std::vector<float> > g_mag_p =  img_stats_p->getGradientMagnitudes ( raster_pts );
  
  std::vector<float> &g_dir = *g_dir_p;
  std::vector<float> &g_mag = *g_mag_p;

  if ( !g_dir.size() || !g_mag_p->size() )
    return 0;

  double score = 0;
  for ( int i = 0; i < g_dir.size(); i++ )
  {
    float &direction = g_dir[i], magnitude = g_mag[i];
    if ( img_stats_p->outOfImage ( direction ) )
      score += OUTER_POINT_SCORE;
    else
      score += ((magnitude > 0.01)?1.0:magnitude ) * std::abs ( cos ( double ( direction ) - normal_directions[i] ) );
  }

  return score/g_dir.size();
}


static void help()
{
  std::cout << "Use the keyboard to move the object model in a reasonable initial position and perform the registration using one of the available algorithms (D2CO, DC-ICP, Simple Chamfer Matching, C-ICP, Direct):" << std::endl;
  std::cout << "[j-l-k-i-u-o] translate the model along the X-Y-Z axis " << std::endl;
  std::cout << "[a-s-q-e-z-w] handle the rotation through axis-angle notation " << std::endl;
  std::cout << "[1] D2CO in the position indicated by the user" << std::endl;
  std::cout << "[2] DC-ICP optimization in the position indicated by the user" << std::endl;
  std::cout << "[3] Simple Chamfer Matching optimization in the position indicated by the user" << std::endl;
  std::cout << "[4] C-ICP optimization in the position indicated by the user" << std::endl;
  std::cout << "[5] Direct optimization in the position indicated by the user" << std::endl;
  std::cout << "[g] evaluate score of the model in the position indicated by the user" << std::endl;
  std::cout << "[SPACE] undo" << std::endl;
  std::cout << "[ESC] exit" << std::endl<<std::endl;
}



int main ( int argc, char** argv )
{
  if ( argc < 3 )
    fprintf ( stderr,"Provide a STL file and an input image\n" );

  help();

  ///// Initial pose /////
  cv::Mat_<double> r_vec = ( cv::Mat_<double> ( 3,1 ) << -1.9, 1.87, 0.49 ),
                   t_vec = ( cv::Mat_<double> ( 3,1 ) << 0,0,0.4 ),
                   prev_r_vec, prev_t_vec;

  ///// CAMERA MODEL /////
  cv::Mat camera_matrix=( cv::Mat_<double> ( 3,3 ) << 1457.845746, 0, 979.39066, 0, 1371.01823, 633.79483, 0, 0, 1);
  cv::Mat dist_coeff=( cv::Mat_<double> ( 5,1 ) << -0.154272, 0.09142, -0.009540999999999999, -0.002034, 0);

  
  int image_width=1920, image_height=1200;
  cv_ext::PinholeCameraModel cam_model ( camera_matrix, image_width, image_height, 1.4, dist_coeff );
  ///////////////////////
  
  std::string stl_file ( argv[1] );
//   std::string stl_file2 ( argv[2] );
//   std::string stl_file3 ( argv[3] );
//   std::vector<std::string> stl_files;stl_files.push_back(stl_file); stl_files.push_back(stl_file2); stl_files.push_back(stl_file3);
//   std::cout<<"file_3 "<<stl_files[2]<<std::endl;

  cv_ext::BasicTimer timer;
    
  ///// Load the 3D object model from file /////
  RasterObjectModel3DPtr obj_model_ptr( new RasterObjectModel3D() );
  obj_model_ptr->setCamModel( cam_model );
  obj_model_ptr->setStepMeters ( 0.001 ); //meters
  obj_model_ptr->setBBCenterOrigOffset();
  if ( !obj_model_ptr->setStlFile ( stl_file ) )
    return -1;
  obj_model_ptr->computeRaster();
  
//   std::vector<RasterObjectModel3DPtr> obj_model_ptr_vec;  
//     obj_model_ptr_vec.resize(stl_files.size());
//     for(int i=0; i<obj_model_ptr_vec.size(); i++)
//     {
//       obj_model_ptr_vec[i]=boost::shared_ptr< RasterObjectModel3D >( new RasterObjectModel3D() );
//       obj_model_ptr_vec[i]->setCamModel( cam_model );
//       obj_model_ptr_vec[i]->setStepMeters ( 0.001 ); //meters
//       //obj_model_ptr->setCentroidOrigOffset();
//       obj_model_ptr_vec[i]->setBBCenterOrigOffset();
//       if ( !obj_model_ptr_vec[i]->setStlFile ( stl_files[i] ) )
//       {
//         return -1;
//       }
//       obj_model_ptr_vec[i]->enableDepthBufferStoring(true);
//       //obj_model_ptr->setMinSegmentsLen(0.01);
//       obj_model_ptr_vec[i]->computeRaster();
//     }
 
  
  std::vector<cv::Point2f> proj_pts;
  std::vector<float> normals;
      
  cv::Mat src_img = imread ( argv[2], cv::IMREAD_GRAYSCALE ), scaled_img, scaled_img_color;
  cv::resize(src_img , src_img , cv::Size(), 1/cam_model.sizeScaleFactor(), 1/cam_model.sizeScaleFactor());
  // Scale image (2.0)
  //cv::pyrDown ( src_img, scaled_img );
  scaled_img = src_img;
  
  ///// Image statistics used in the evaluateScore function /////
  cv_ext::ImageStatisticsPtr img_stats_p =
  cv_ext::ImageStatistics::createImageStatistics ( scaled_img, true );

  
  bool exit_now = false, optimize_c = false, optimize_dc = false, 
       optimize_direct = false, local_search = false, optimize_icp_c = false,
       optimize_icp_dc = false;
  bool draw=false;

  cv::Mat dbg_img;
  cv::cvtColor ( scaled_img,  scaled_img_color, cv::COLOR_GRAY2BGR );
  dbg_img=scaled_img_color.clone();
      
  ImageTensorPtr dist_map_tensor_ptr, /*x_dist_map_tensor_ptr, y_dist_map_tensor_ptr,*/ edgels_map_tensor_ptr;
  timer.reset();
  cv::Mat dist_map, closest_edgels_map;
  computeDistanceMap( scaled_img, dist_map, closest_edgels_map );
  ChamferMatching c_matching(cam_model, dist_map );
  c_matching.setTemplateModel( obj_model_ptr );
  c_matching.enableVerbouseMode ( false);
  
  ICPChamferMatching icp_c_matching(cam_model, closest_edgels_map );
  icp_c_matching.setTemplateModel( obj_model_ptr );
  icp_c_matching.enableVerbouseMode ( false);
  
  computeDistanceMapTensor ( scaled_img, dist_map_tensor_ptr, edgels_map_tensor_ptr, 50.0 );
  std::cout<<"\ncomputeDistanceMapTensor : "<<timer.elapsedTimeMs() <<" ms"<<std::endl;
  
  DirectionalChamferMatching dc_matching(cam_model, dist_map_tensor_ptr);
  dc_matching.setTemplateModel( obj_model_ptr );
  dc_matching.enableVerbouseMode ( false);

  ICPDirectionalChamferMatching icp_dc_matching(cam_model, edgels_map_tensor_ptr );
  icp_dc_matching.setTemplateModel( obj_model_ptr );
  icp_dc_matching.enableVerbouseMode ( false);
  
  ScaledImagesListPtr g_mag_pyr_ptr;
  computeGradientMagnitudePyr(scaled_img, g_mag_pyr_ptr, 1 );
  DirectMatching direct_matching(cam_model, g_mag_pyr_ptr );
  direct_matching.setTemplateModel( obj_model_ptr );
  direct_matching.enableVerbouseMode ( false);

//   int model_idx=0;
  while ( !exit_now )
  {
//     obj_model_ptr=obj_model_ptr_vec[model_idx];
    prev_r_vec = r_vec.clone();
    prev_t_vec = t_vec.clone();

    if ( optimize_c )
    {
      timer.reset();
      c_matching.performOptimization ( r_vec, t_vec );
      std::cout<<std::endl<<"Simple Chamfer Optimization (elapsed time): "<<timer.elapsedTimeMs() <<" ms"<<std::endl;
      obj_model_ptr->setModelView(r_vec, t_vec);
      obj_model_ptr->projectRasterPoints ( proj_pts, normals );
      std::cout<<"Score : "<<evaluateScore ( img_stats_p, proj_pts, normals ) <<std::endl;
      std::cout<<"Object Pose (in camera reference frame): "<<std::endl;
      std::cout<<" - Rotation (axis-angle): "<<r_vec<<std::endl;
      std::cout<<" - Translation (meters): "<<t_vec<<std::endl;
      optimize_c = false;
    }
    else if ( optimize_dc )
    {
      timer.reset();
      dc_matching.performOptimization ( r_vec, t_vec );
      std::cout<<std::endl<<"D2CO (elapsed time): "<<timer.elapsedTimeMs() <<" ms"<<std::endl;
      obj_model_ptr->setModelView(r_vec, t_vec);
      obj_model_ptr->projectRasterPoints ( proj_pts, normals );
      std::cout<<"Score : "<<evaluateScore ( img_stats_p, proj_pts, normals ) <<std::endl;
      std::cout<<"Object Pose (in camera reference frame): "<<std::endl;
      std::cout<<" - Rotation (axis-angle): "<<r_vec<<std::endl;
      std::cout<<" - Translation (meters): "<<t_vec<<std::endl;
      optimize_dc = false;
    }
    else if( optimize_direct )
    {
      timer.reset();
      direct_matching.performOptimization ( r_vec, t_vec );
      std::cout<<std::endl<<"Direct (elapsed time): "<<timer.elapsedTimeMs() <<" ms"<<std::endl;
      obj_model_ptr->setModelView(r_vec, t_vec);
      obj_model_ptr->projectRasterPoints ( proj_pts, normals );
      std::cout<<"Score : "<<evaluateScore ( img_stats_p, proj_pts, normals ) <<std::endl;
      std::cout<<"Object Pose (in camera reference frame): "<<std::endl;
      std::cout<<" - Rotation (axis-angle): "<<r_vec<<std::endl;
      std::cout<<" - Translation (meters): "<<t_vec<<std::endl;
      optimize_direct = false;      
    }
    else if( optimize_icp_c )
    {
      timer.reset();
      icp_c_matching.performOptimization ( r_vec, t_vec );
      std::cout<<std::endl<<"C-ICP (elapsed time): "<<timer.elapsedTimeMs() <<" ms"<<std::endl;
      obj_model_ptr->setModelView(r_vec, t_vec);
      obj_model_ptr->projectRasterPoints ( proj_pts, normals );
      std::cout<<"Score : "<<evaluateScore ( img_stats_p, proj_pts, normals ) <<std::endl;
      std::cout<<"Object Pose (in camera reference frame): "<<std::endl;
      std::cout<<" - Rotation (axis-angle): "<<r_vec<<std::endl;
      std::cout<<" - Translation (meters): "<<t_vec<<std::endl;
      optimize_icp_c = false;
    }
    else if( optimize_icp_dc )
    {
      timer.reset();
      icp_dc_matching.performOptimization ( r_vec, t_vec );
      std::cout<<std::endl<<"DC-ICP (elapsed time): "<<timer.elapsedTimeMs() <<" ms"<<std::endl;
      obj_model_ptr->setModelView(r_vec, t_vec);
      obj_model_ptr->projectRasterPoints ( proj_pts, normals );
      std::cout<<"Score : "<<evaluateScore ( img_stats_p, proj_pts, normals ) <<std::endl;
      std::cout<<"Object Pose (in camera reference frame): "<<std::endl;
      std::cout<<" - Rotation (axis-angle): "<<r_vec<<std::endl;
      std::cout<<" - Translation (meters): "<<t_vec<<std::endl;
      optimize_icp_dc = false;     
    }  
    if ( !draw )
    {
      dbg_img=scaled_img_color.clone();
      obj_model_ptr->setModelView(r_vec, t_vec);
      obj_model_ptr->projectRasterPoints ( proj_pts, normals );
      cv_ext::drawPoints ( dbg_img, proj_pts,cv::Scalar ( 0,0,255 ) );
    }
    cv::imshow ( "test_localization", dbg_img );
    draw=false;

    int key = cv::waitKey();
    switch ( key )
    {
    case 'i':
    case 'I':
      t_vec.at<double> ( 1,0 ) -= 0.01;
      break;
    case 'k':
    case 'K':
      t_vec.at<double> ( 1,0 ) += 0.01;
      break;
    case 'j':
    case 'J':
      t_vec.at<double> ( 0,0 ) -= 0.01;
      break;
    case 'l':
    case 'L':
      t_vec.at<double> ( 0,0 ) += 0.01;
      break;
    case 'u':
    case 'U':
      t_vec.at<double> ( 2,0 ) -= 0.01;
      break;
    case 'o':
    case 'O':
      t_vec.at<double> ( 2,0 ) += 0.01;
      break;
    case 'a':
    case 'A':
      r_vec.at<double> ( 1,0 ) += 0.01;
      break;
    case 's':
    case 'S':
      r_vec.at<double> ( 1,0 ) -= 0.01;
      break;
    case 'w':
    case 'W':
      r_vec.at<double> ( 0,0 ) += 0.01;
      break;
    case 'z':
    case 'Z':
      r_vec.at<double> ( 0,0 ) -= 0.01;
      break;
    case 'q':
    case 'Q':
      r_vec.at<double> ( 2,0 ) += 0.01;
      break;
    case 'e':
    case 'E':
      r_vec.at<double> ( 2,0 ) -= 0.01;
      break;
    case 'g' : //evaluate Score
    case 'G' :
      std::cout<<"Score : "<<evaluateScore ( img_stats_p, proj_pts, normals ) <<std::endl;
      break;
    case '3':
//       std::cout<<"Direct Chamfer"<<std::endl;
      optimize_c = true;
      break;
    case '1':
//       std::cout<<"D2CO"<<std::endl;
      optimize_dc = true;
      break;
    case '5':
//       std::cout<<"Gradient magnitude"<<std::endl;
      optimize_direct = true;
      break;
    case '4':
//       std::cout<<"ICP Chamfer"<<std::endl;
      optimize_icp_c = true;
      break;
    case '2':
//       std::cout<<"Directional ICP Chamfer"<<std::endl;
      optimize_icp_dc = true;
      break;
    case ' ':
      std::cout<<"Undo"<<std::endl;
      r_vec = prev_r_vec.clone();
      t_vec = prev_t_vec.clone();
      break;
//     case 'b':
//       cv::imwrite("clean_img.jpg", dbg_img);
//       scaled_img_color=dbg_img.clone();
//       break;
// //     case 'n':
// //       model_idx++;
// // //       obj_model_ptr=boost::shared_ptr< RasterObjectModel3D >( new RasterObjectModel3D() );
// // //       obj_model_ptr->setCamModel( cam_model );
// // //       obj_model_ptr->setStepMeters ( 0.001 ); //meters
// // //       obj_model_ptr->setBBCenterOrigOffset();
// // //       if ( !obj_model_ptr->setStlFile ( stl_files[model_idx] ) )
// // //         return -1;
// // //       obj_model_ptr->computeRaster();
// //       break;
    case 27: // ESC
      exit_now=true;
      break;
    }
  }

  return 0;
}
