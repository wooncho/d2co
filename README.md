# D2CO: Direct Directional Chamfer Optimization #

This software implements the D2CO (Direct Directional Chamfer Optimization) algorithm, a fast edge-based registration technique for accurate 3D object pose estimation.

## References ##

Paper Describing the Approach:

M. Imperoli and A. Pretto D2CO: Fast and Robust Registration of 3D Textureless Objects Using the Directional Chamfer Distance In Proceedings of the 10th International Conference on Computer Vision Systems (ICVS 2015), July 6-9 , 2015 Copenhagen, Denmark, pages: 316-328 ([PDF](http://www.dis.uniroma1.it/~pretto/papers/miap_icvs2015.pdf))


```
#!bibtex

@inproceedings{miap_icvs2015,
  author={Imperoli, M. and Pretto, A.},
  title={{D\textsuperscript{2}CO}: Fast and Robust Registration of {3D}
         Textureless Objects Using the {Directional 
         Chamfer Distance}},
  booktitle={Proc. of 10th International Conference on 
             Computer Vision Systems (ICVS 2015)},
  year={2015},
  pages={316--328}
}
```

## Requirements ##

The code is tested on Ubuntu 14.04. D2CO requires different tools and libraries. To install them on Ubuntu, use the terminal command:


```
#!bash

sudo apt-get install build-essential cmake libeigen3-dev libdime-dev libdime1 libglew-dev libglew1.10 libglm-dev
```
- Follow this [guide](http://ceres-solver.org/building.html) to install Ceres Solver.
- Click [here](http://www.openmesh.org/media/Releases/3.3/OpenMesh-3.3.tar.gz) to download OpenMesh.
- Click [here](http://sourceforge.net/projects/glfw/files/glfw/3.1/glfw-3.1.zip/download) to download glfw.

## Building ##

To build D2CO on Ubuntu, type in a terminal the following command sequence.

```
#!bash

cd d2co
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make

```
Test the library with the **test_localization** app (binary in /bin, source code in apps/test_localization.cpp): given the 3D CAD model of the object and the image of the scene, **test_localization** can perform different registration methods including D2CO and ICP-based algorithms:

```
#!bash

./test_localization 3D_models/AX-01b_bearing_box.stl test_images/2.png

```

## Contact information ##

- Alberto Pretto [pretto@dis.uniroma1.it](mailto:pretto@dis.uniroma1.it)
- Marco Imperoli [imperoli@dis.uniroma1.it](mailto:imperoli@dis.uniroma1.it)